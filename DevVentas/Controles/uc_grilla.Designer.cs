﻿namespace Capa_Cliente.Controles
{
    partial class uc_grilla
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_grilla));
            this.dgv_mostrar = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.item_exportar = new System.Windows.Forms.ToolStripMenuItem();
            this.item_multiselect = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_mostrar)).BeginInit();
            this.ContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_mostrar
            // 
            this.dgv_mostrar.AllowUserToAddRows = false;
            this.dgv_mostrar.AllowUserToDeleteRows = false;
            this.dgv_mostrar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_mostrar.ContextMenuStrip = this.ContextMenuStrip;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_mostrar.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_mostrar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_mostrar.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgv_mostrar.Location = new System.Drawing.Point(0, 0);
            this.dgv_mostrar.MultiSelect = false;
            this.dgv_mostrar.Name = "dgv_mostrar";
            this.dgv_mostrar.ReadOnly = true;
            this.dgv_mostrar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_mostrar.Size = new System.Drawing.Size(635, 206);
            this.dgv_mostrar.TabIndex = 0;
            this.dgv_mostrar.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_mostrar_CellDoubleClick);
            this.dgv_mostrar.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_mostrar_RowEnter);
            this.dgv_mostrar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgv_mostrar_KeyDown);
            this.dgv_mostrar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgv_mostrar_MouseDown);
            // 
            // ContextMenuStrip
            // 
            this.ContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item_exportar,
            this.item_multiselect});
            this.ContextMenuStrip.Name = "ContextMenuStrip";
            this.ContextMenuStrip.Size = new System.Drawing.Size(153, 70);
            // 
            // item_exportar
            // 
            this.item_exportar.Image = ((System.Drawing.Image)(resources.GetObject("item_exportar.Image")));
            this.item_exportar.Name = "item_exportar";
            this.item_exportar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.item_exportar.Size = new System.Drawing.Size(152, 22);
            this.item_exportar.Text = "Exportar";
            this.item_exportar.Click += new System.EventHandler(this.item_exportar_Click);
            // 
            // item_multiselect
            // 
            this.item_multiselect.Image = ((System.Drawing.Image)(resources.GetObject("item_multiselect.Image")));
            this.item_multiselect.Name = "item_multiselect";
            this.item_multiselect.Size = new System.Drawing.Size(152, 22);
            this.item_multiselect.Text = "MultiSelect";
            this.item_multiselect.Visible = false;
            this.item_multiselect.Click += new System.EventHandler(this.item_multiselect_Click);
            // 
            // uc_grilla
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgv_mostrar);
            this.Name = "uc_grilla";
            this.Size = new System.Drawing.Size(635, 206);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_mostrar)).EndInit();
            this.ContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public DevComponents.DotNetBar.Controls.DataGridViewX dgv_mostrar;
        private System.Windows.Forms.ToolStripMenuItem item_exportar;
        private System.Windows.Forms.ContextMenuStrip ContextMenuStrip;
        public System.Windows.Forms.ToolStripMenuItem item_multiselect;

    }
}
