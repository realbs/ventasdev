﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using Capa_Cliente.Basico;
using System.IO;

namespace Capa_Cliente.Controles
{
    public partial class uc_grilla : UserControl
    {
        int IniMenu = 0;
        int Posicionfila = 0;
        int Posicioncolumna = 0;
        string TextoBuscar = string.Empty;
        public DataGridViewRow _filaactiva;
        public event EventHandler Grilla_CellDoubleClick;
        public event EventHandler Grilla_RowEnter;

        private void CellDoubleClick(DataGridViewCellEventArgs e)
        {
            if (Grilla_CellDoubleClick != null) Grilla_CellDoubleClick(this, e);
        }

        private void RowEnter(DataGridViewCellEventArgs e)
        {
            if (Grilla_RowEnter != null) Grilla_RowEnter(this, e);
        }

        public uc_grilla()
        {
            InitializeComponent();
        }

        public void ExportToExcel()
        {
            if (this.dgv_mostrar.Rows.Count == 0)
            {
                MessageBox.Show("No hay registros.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                string fileName = Guid.NewGuid().ToString() + ".xml";

                String srtRutaArchivo = clsFunciones.ObtenerCarpetaTemporal();

                fileName = srtRutaArchivo + fileName;

                System.IO.StreamWriter excelDoc;
                excelDoc = new System.IO.StreamWriter(fileName);

                //const string startExcelXML = "<xml version>\r\n<Workbook " +
                //      "xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"\r\n" +
                //      " xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\n " +
                //      "xmlns:x=\"urn:schemas-    microsoft-com:office:" +
                //      "excel\"\r\n xmlns:ss=\"urn:schemas-microsoft-com:" +
                //      "office:spreadsheet\">\r\n <Styles>\r\n " +
                //    //para cambiar la forma en la que se muestra los datos
                //          "<Style ss:ID=\"Default\" ss:Name=\"Normal\">\r\n <Alignment ss:Vertical=\"Bottom\"/>\r\n <Borders/>\r\n <Font/>\r\n <Interior/>\r\n <NumberFormat/>\r\n <Protection/>\r\n </Style>\r\n " +
                //          "<Style ss:ID=\"BoldColumn\"> <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/> \r\n <Font x:Family=\"Swiss\" ss:Color=\"#FFFFFF\" ss:Bold=\"1\"/> <Interior ss:Color=\"#5A5A5A\" ss:Pattern=\"Solid\"/>\r\n </Style>\r\n " +

                //          "<Style ss:ID=\"StringLiteral\">   \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"@\"/>             <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>\r\n </Style>\r\n " +
                //          "<Style ss:ID=\"Decimal1\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.0\"/>       <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>\r\n </Style>\r\n " +
                //          "<Style ss:ID=\"Decimal2\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.00\"/>      <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>\r\n </Style>\r\n " +
                //          "<Style ss:ID=\"Decimal3\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.000\"/>     <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>\r\n </Style>\r\n " +
                //          "<Style ss:ID=\"Decimal4\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.0000\"/>    <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>\r\n </Style>\r\n " +
                //          "<Style ss:ID=\"Decimal5\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.00000\"/>   <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>\r\n </Style>\r\n " +
                //          "<Style ss:ID=\"Decimal6\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.000000\"/>  <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>\r\n </Style>\r\n " +
                //          "<Style ss:ID=\"DecimalD\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.0000000\"/> <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>\r\n </Style>\r\n " +
                //          "<Style ss:ID=\"Integer\">         \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0\"/>         <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>\r\n </Style>\r\n " +
                //          "<Style ss:ID=\"IntegerSF\">       \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"0\"/>             <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>\r\n </Style>\r\n " +
                //          "<Style ss:ID=\"DateLiteral\">     \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"dd/MM/yyyy HH:mm:ss;@\"/>  <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>\r\n </Style>\r\n " +
                //      "</Styles>\r\n ";

                const string startExcelXML = "<xml version>\r\n<Workbook " +
                     "xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"\r\n" +
                     " xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\n " +
                     "xmlns:x=\"urn:schemas-    microsoft-com:office:" +
                     "excel\"\r\n xmlns:ss=\"urn:schemas-microsoft-com:" +
                     "office:spreadsheet\">\r\n <Styles>\r\n " +
                    //para cambiar la forma en la que se muestra los datos
                         "<Style ss:ID=\"Default\" ss:Name=\"Normal\">\r\n <Alignment ss:Vertical=\"Bottom\"/>\r\n <Borders/>\r\n <Font/>\r\n <Interior/>\r\n <NumberFormat/>\r\n <Protection/>\r\n </Style>\r\n " +
                         "<Style ss:ID=\"BoldColumn\"> <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/> \r\n <Font x:Family=\"Swiss\" ss:Color=\"#FFFFFF\" ss:Bold=\"1\"/> <Interior ss:Color=\"#4267B2\" ss:Pattern=\"Solid\"/>\r\n </Style>\r\n " +

                         "<Style ss:ID=\"StringLiteral\">   \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"@\"/>            \r\n </Style>\r\n " +
                         "<Style ss:ID=\"Decimal1\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.0\"/>       \r\n </Style>\r\n " +
                         "<Style ss:ID=\"Decimal2\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.00\"/>      \r\n </Style>\r\n " +
                         "<Style ss:ID=\"Decimal3\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.000\"/>     \r\n </Style>\r\n " +
                         "<Style ss:ID=\"Decimal4\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.0000\"/>    \r\n </Style>\r\n " +
                         "<Style ss:ID=\"Decimal5\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.00000\"/>   \r\n </Style>\r\n " +
                         "<Style ss:ID=\"Decimal6\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.000000\"/>  \r\n </Style>\r\n " +
                         "<Style ss:ID=\"DecimalD\">        \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0.0000000\"/> \r\n </Style>\r\n " +
                         "<Style ss:ID=\"Integer\">         \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"#,##0\"/>         \r\n </Style>\r\n " +
                         "<Style ss:ID=\"IntegerSF\">       \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"0\"/>             \r\n </Style>\r\n " +
                         "<Style ss:ID=\"DateLiteral\">     \r\n <Font x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"0\"/><NumberFormat ss:Format=\"dd/MM/yyyy HH:mm:ss;@\"/>  \r\n </Style>\r\n " +
                     "</Styles>\r\n ";

                int rowCount = 0;
                int sheetCount = 1;

                excelDoc.Write(startExcelXML);
                excelDoc.Write("<Worksheet ss:Name=\"Sheet" + sheetCount + "\">");


                excelDoc.Write("<Table>");
                //Cabecera en caso se necesite ingresar una cabecera a la hoja()
                //excelDoc.Write("<Row>");
                //excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\"><Data ss:Type=\"String\">");
                //excelDoc.Write("Creado Por: Jorge Plasencia");
                //excelDoc.Write("</Data></Cell>");
                //excelDoc.Write("</Row>");
                //excelDoc.Write("<Row>");
                //excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\"><Data ss:Type=\"String\">");
                //excelDoc.Write("Fecha:");
                //excelDoc.Write("</Data></Cell>");
                //excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\"><Data ss:Type=\"String\">");
                //excelDoc.Write("=AHORA()");
                //excelDoc.Write("</Data></Cell>");
                //excelDoc.Write("</Row>");
                //excelDoc.Write("<Row/>");

                // Setear el ancho de las celdas - EVT.
                for (int x = 0; x < this.dgv_mostrar.Columns.Count; x++)
                {
                    excelDoc.Write("<Column ss:Width=\"" + this.dgv_mostrar.Columns[x].Width.ToString() + "\"/>");
                }

                excelDoc.Write("<Row>");
                for (int x = 0; x < this.dgv_mostrar.Columns.Count; x++)
                {
                    if (this.dgv_mostrar.Columns[x].Visible &&
                        (this.dgv_mostrar.Columns[x].CellType.ToString() == "System.Windows.Forms.DataGridViewTextBoxCell" ||
                         this.dgv_mostrar.Columns[x].CellType.ToString() == "System.Windows.Forms.DataGridViewComboBoxCell" ||
                         this.dgv_mostrar.Columns[x].CellType.ToString() == "System.Windows.Forms.DataGridViewLinkCell" ||
                         this.dgv_mostrar.Columns[x].CellType.ToString() == "System.Windows.Forms.DataGridViewCheckBoxCell" ||
                         this.dgv_mostrar.Columns[x].CellType.ToString() == "OptimusNG.Windows.Controles.CeldaFechaHoraNG"))
                    {
                        excelDoc.Write("<Cell ss:StyleID=\"BoldColumn\"><Data ss:Type=\"String\">");
                        excelDoc.Write(this.dgv_mostrar.Columns[x].HeaderText);
                        excelDoc.Write("</Data></Cell>");
                    }
                }
                excelDoc.Write("</Row>");

                foreach (DataGridViewRow x in this.dgv_mostrar.Rows)
                //foreach (DataRow x in grilla.Rows)
                {
                    rowCount++;
                    //if the number of rows is > 64000 create a new page to continue output
                    if (rowCount == 64000)
                    {
                        rowCount = 0;
                        sheetCount++;
                        excelDoc.Write("</Table>");
                        excelDoc.Write(" </Worksheet>");
                        excelDoc.Write("<Worksheet ss:Name=\"Sheet" + sheetCount + "\">");
                        excelDoc.Write("<Table>");
                    }

                    excelDoc.Write("<Row>"); //ID=" + rowCount + "
                    for (int y = 0; y < this.dgv_mostrar.Columns.Count; y++)
                    {
                        if (this.dgv_mostrar.Columns[y].Visible &&
                            (this.dgv_mostrar.Columns[y].CellType.ToString() == "System.Windows.Forms.DataGridViewTextBoxCell" ||
                             this.dgv_mostrar.Columns[y].CellType.ToString() == "System.Windows.Forms.DataGridViewComboBoxCell" ||
                             this.dgv_mostrar.Columns[y].CellType.ToString() == "System.Windows.Forms.DataGridViewLinkCell" ||
                             this.dgv_mostrar.Columns[y].CellType.ToString() == "System.Windows.Forms.DataGridViewCheckBoxCell" ||
                             this.dgv_mostrar.Columns[y].CellType.ToString() == "OptimusNG.Windows.Controles.CeldaFechaHoraNG"))
                        {
                            String valor;
                            object Valor1;
                            int NDec = 0;
                            bool Formato = false;
                            System.Type rowType;
                            rowType = x.Cells[y].GetType();
                            valor = x.Cells[y].FormattedValue.ToString();
                            Formato = (valor.IndexOf(",") >= 0);
                            valor = valor.Replace(",", "");
                            /*if (this.Columns[y].CellType.ToString() == "System.Windows.Forms.DataGridViewComboBoxCell")
                            {
                                valor = x.Cells[y].FormattedValue.ToString();
                            }*/

                            if (x.Cells[y].FormattedValueType.ToString() == "System.Boolean")
                            {
                                valor = x.Cells[y].Value == null ? "" : x.Cells[y].Value.ToString();
                            }

                            if (IsNumeric(valor) == true)
                            {
                                if ((valor.IndexOf("(") >= 0) && (valor.IndexOf(")") >= 0))
                                {
                                    Decimal Nreg = 0;
                                    valor = valor.Replace("(", "");
                                    valor = valor.Replace(")", "");
                                    Nreg = Decimal.Parse(valor);
                                    Nreg = Nreg * -1;
                                    valor = Nreg.ToString();
                                }
                                if (valor.IndexOf(".") >= 0)
                                {
                                    Valor1 = Decimal.Parse(valor);
                                    NDec = valor.Length - valor.IndexOf(".") - 1;
                                    valor = valor.Replace(",", ".");
                                }
                                else
                                {
                                    Valor1 = long.Parse(valor);
                                }

                            }
                            else
                            {
                                /*if (this.Columns[y].CellType.ToString() == "System.Windows.Forms.DataGridViewComboBoxCell")
                                {
                                    valor = x.Cells[y].FormattedValue.ToString();
                                    Valor1 = x.Cells[y].FormattedValue.ToString();
                                }
                                else
                                {*/
                                valor = x.Cells[y].FormattedValue.ToString();
                                Valor1 = x.Cells[y].FormattedValue.ToString();
                                //}
                            }
                            rowType = Valor1.GetType();
                            if (valor == "System.Drawing.Bitmap")
                            {
                                excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\"><Data ss:Type=\"String\">");
                                excelDoc.Write("");
                                excelDoc.Write("</Data></Cell>");
                            }
                            else
                            {
                                switch (rowType.ToString())
                                {
                                    case "System.String":
                                        string XMLstring = valor;
                                        XMLstring = XMLstring.Trim();
                                        XMLstring = XMLstring.Replace("&", "&");
                                        XMLstring = XMLstring.Replace(">", ">");
                                        XMLstring = XMLstring.Replace("<", "<");
                                        excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\"><Data ss:Type=\"String\">");
                                        excelDoc.Write(XMLstring);
                                        excelDoc.Write("</Data></Cell>");
                                        break;
                                    case "System.DateTime":
                                        //Excel has a specific Date Format of YYYY-MM-DD followed by  
                                        //the letter 'T' then hh:mm:sss.lll Example 2005-01-31T24:01:21.000
                                        //The Following Code puts the date stored in XMLDate 
                                        //to the format above
                                        DateTime XMLDate = (DateTime)x.Cells[y].FormattedValue;
                                        XMLDate.ToString("dd/MM/yyyy HH:mm:ss");
                                        string XMLDatetoString = ""; //Excel Converted Date
                                        XMLDatetoString = XMLDate.Year.ToString() +
                                             "-" +
                                             (XMLDate.Month < 10 ? "0" +
                                             XMLDate.Month.ToString() : XMLDate.Month.ToString()) +
                                             "-" +
                                             (XMLDate.Day < 10 ? "0" +
                                             XMLDate.Day.ToString() : XMLDate.Day.ToString()) +
                                             "T" +
                                             (XMLDate.Hour < 10 ? "0" +
                                             XMLDate.Hour.ToString() : XMLDate.Hour.ToString()) +
                                             ":" +
                                             (XMLDate.Minute < 10 ? "0" +
                                             XMLDate.Minute.ToString() : XMLDate.Minute.ToString()) +
                                             ":" +
                                             (XMLDate.Second < 10 ? "0" +
                                             XMLDate.Second.ToString() : XMLDate.Second.ToString()) +
                                             ".000";
                                        excelDoc.Write("<Cell ss:StyleID=\"DateLiteral\"><Data ss:Type=\"DateTime\">");
                                        excelDoc.Write(XMLDatetoString);
                                        excelDoc.Write("</Data></Cell>");
                                        break;
                                    case "System.Boolean":
                                        excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\"><Data ss:Type=\"String\">");
                                        excelDoc.Write(valor);
                                        excelDoc.Write("</Data></Cell>");
                                        break;
                                    case "System.Int16":
                                    case "System.Int32":
                                    case "System.Int64":
                                        if (!Formato)//significa que no tiene formato de separador de miles
                                        {
                                            excelDoc.Write("<Cell ss:StyleID=\"IntegerSF\"><Data ss:Type=\"Number\">");
                                            excelDoc.Write(valor.Trim());
                                            excelDoc.Write("</Data></Cell>");
                                        }
                                        else
                                        {
                                            excelDoc.Write("<Cell ss:StyleID=\"Integer\"><Data ss:Type=\"Number\">");
                                            excelDoc.Write(valor.Trim());
                                            excelDoc.Write("</Data></Cell>");
                                        }
                                        break;
                                    case "System.Byte":
                                        if (!Formato)//significa que no tiene formato de separador de miles
                                        {
                                            excelDoc.Write("<Cell ss:StyleID=\"IntegerSF\"><Data ss:Type=\"Number\">");
                                            excelDoc.Write(valor.Trim());
                                            excelDoc.Write("</Data></Cell>");
                                        }
                                        else
                                        {
                                            excelDoc.Write("<Cell ss:StyleID=\"Integer\"><Data ss:Type=\"Number\">");
                                            excelDoc.Write(valor.Trim());
                                            excelDoc.Write("</Data></Cell>");
                                        }
                                        break;
                                    case "System.Decimal":
                                    case "System.Double":
                                        switch (NDec)
                                        {
                                            case 1:
                                                excelDoc.Write("<Cell ss:StyleID=\"Decimal1\"><Data ss:Type=\"Number\">");
                                                break;
                                            case 2:
                                                excelDoc.Write("<Cell ss:StyleID=\"Decimal2\"><Data ss:Type=\"Number\">");
                                                break;
                                            case 3:
                                                excelDoc.Write("<Cell ss:StyleID=\"Decimal3\"><Data ss:Type=\"Number\">");
                                                break;
                                            case 4:
                                                excelDoc.Write("<Cell ss:StyleID=\"Decimal4\"><Data ss:Type=\"Number\">");
                                                break;
                                            case 5:
                                                excelDoc.Write("<Cell ss:StyleID=\"Decimal5\"><Data ss:Type=\"Number\">");
                                                break;
                                            case 6:
                                                excelDoc.Write("<Cell ss:StyleID=\"Decimal6\"><Data ss:Type=\"Number\">");
                                                break;
                                            default:
                                                excelDoc.Write("<Cell ss:StyleID=\"DecimalD\"><Data ss:Type=\"Number\">");
                                                break;
                                        }
                                        excelDoc.Write(valor.Trim());
                                        excelDoc.Write("</Data></Cell>");
                                        break;
                                    case "System.DBNull":
                                        excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\"><Data ss:Type=\"String\">");
                                        excelDoc.Write("");
                                        excelDoc.Write("</Data></Cell>");
                                        break;
                                    default:
                                        throw (new Exception(rowType.ToString() + " not handled."));
                                }
                            }
                        }
                    }
                    excelDoc.Write("</Row>");
                }
                excelDoc.Write("</Table>");


                excelDoc.Write("<WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">");
                //excelDoc.Write("<Zoom>80</Zoom><Selected/>");
                excelDoc.Write("<ProtectObjects>False</ProtectObjects>");
                excelDoc.Write("<ProtectScenarios>False</ProtectScenarios>");
                excelDoc.Write("</WorksheetOptions>");
                //--------------------------------------------------------------------

                excelDoc.Write(" </Worksheet>");
                excelDoc.Write("</Workbook>");
                excelDoc.Close();
                string exe = "EXCEL.exe";
                try
                {
                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    p.EnableRaisingEvents = false;
                    Process.Start(exe, fileName);
                }
                catch (System.Exception ex)
                {
                    MessageBox.Show(ex.Message + " '" + exe + "'", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private static bool IsNumeric(object Expression)
        {
            bool isNum = false;
            try
            {
                double retNum;
                isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
                return isNum;
            }
            catch (System.Exception ex)
            {
                return isNum;
            }
        }

        private void dgv_mostrar_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                    //this.ExportToExcel();
                    break;
                case Keys.F2:
                    //this.Buscar(true);
                    break;
                case Keys.F3:
                    //this.Buscar(false, true);
                    break;
                default:
                    break;
            }
        }

        private void dgv_mostrar_MouseDown(object sender, MouseEventArgs e)
        {
            //if (IniMenu == 0)
            //{
            //    if (this.ContextMenuStrip != null)
            //    {
            //        //ClsExportToExcelToolStripMenuItem a = new ClsExportToExcelToolStripMenuItem(this);
            //        //this.ContextMenuStrip.Items.Add("-");
            //        //this.ContextMenuStrip.Items.Add(a);
            //        //a = new ClsExportToExcelToolStripMenuItem(this, "buscarToolStripMenuItem", "Buscar");
            //        //this.ContextMenuStrip.Items.Add(a);

            //    }
            //    else
            //    {
            //        //ClsExportToExcelContextMenu Menu = new ClsExportToExcelContextMenu(this);
            //        //this.ContextMenuStrip = Menu;
            //    }
            //    IniMenu++;
            //}
        }

        private void item_exportar_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable table = clsFunciones.Clonar((DataTable)dgv_mostrar.DataSource);

                foreach (DataGridViewColumn item in dgv_mostrar.Columns)
                {
                    if (item.Visible == false)
                    {
                        table.Columns.Remove(item.DataPropertyName.ToString());
                    }

                }

                string rutadestino = clsFunciones.ObtenerCarpetaTemporal();

                DateTime fecha = clsFunciones.ObtenerFechaServidor();

                string strfecha = fecha.Year.ToString()
                                + fecha.Month.ToString()
                                + fecha.Day.ToString()
                                + fecha.Hour.ToString()
                                + fecha.Minute.ToString()
                                + fecha.Second.ToString();

                string nombrearchivo = strfecha + ".xlsx";

                rutadestino = Path.Combine(rutadestino, Path.GetFileName(nombrearchivo));

                clsFunciones.ExportarExcel2007(table, "", rutadestino, clsFunciones.Usuario.NombreUsuario, false, "Green");

                Process.Start(rutadestino);

                //ExportToExcel();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void dgv_mostrar_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            _filaactiva = dgv_mostrar.Rows[e.RowIndex];
            RowEnter(e);
        }

        public void ColumnaVisible(string header, bool visible)
        {
            dgv_mostrar.Columns[header].Visible = visible;
        }

        public void ColumnaReadOnly(string header, bool read)
        {
            dgv_mostrar.Columns[header].ReadOnly = read;
        }

        private void dgv_mostrar_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            CellDoubleClick(e);
        }

        private void item_multiselect_Click(object sender, EventArgs e)
        {
            this.dgv_mostrar.MultiSelect = true;
        }
    }
}
