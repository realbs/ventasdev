﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Capa_Entidad;
using System.Collections;
using Capa_Cliente.Basico;

namespace Capa_Cliente.Controles
{
    public partial class uc_empresa : UserControl
    {
        public event EventHandler DatosCambiados;

        private void OnDatosCambiados(EventArgs e)
        {
            if (DatosCambiados != null) DatosCambiados(this, e);
        }

        public uc_empresa()
        {
            InitializeComponent();
        }

        public void Inicializar(clsUsuario objusuario,Boolean todos)
        {  
            Hashtable parametros = new Hashtable();
            parametros.Add("@p_idempresa", objusuario.IdEmpresa);
            parametros.Add("@p_idusuario", objusuario.IdUsuario);
            clsFunciones.Instancia.CargarCombo(this.cbo_empresa, "Empresa", "AyudaLista", parametros, todos);

            if (objusuario.IdEmpresa > 0)
            {
                this.cbo_empresa.SelectedValue = objusuario.IdEmpresa;
                this.cbo_empresa.Enabled = false;             
            }
        }

        private void cbo_empresa_SelectionChangeCommitted(object sender, EventArgs e)
        {
            OnDatosCambiados(e);
        }
    }
}
