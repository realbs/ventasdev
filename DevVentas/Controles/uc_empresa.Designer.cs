﻿namespace Capa_Cliente.Controles
{
    partial class uc_empresa
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbo_empresa = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lbl_empresa = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // cbo_empresa
            // 
            this.cbo_empresa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbo_empresa.DisplayMember = "Text";
            this.cbo_empresa.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_empresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_empresa.FormattingEnabled = true;
            this.cbo_empresa.ItemHeight = 14;
            this.cbo_empresa.Location = new System.Drawing.Point(53, 2);
            this.cbo_empresa.Name = "cbo_empresa";
            this.cbo_empresa.Size = new System.Drawing.Size(280, 20);
            this.cbo_empresa.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_empresa.TabIndex = 45;
            this.cbo_empresa.SelectionChangeCommitted += new System.EventHandler(this.cbo_empresa_SelectionChangeCommitted);
            // 
            // lbl_empresa
            // 
            this.lbl_empresa.AutoSize = true;
            // 
            // 
            // 
            this.lbl_empresa.BackgroundStyle.Class = "";
            this.lbl_empresa.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_empresa.Location = new System.Drawing.Point(0, 5);
            this.lbl_empresa.Name = "lbl_empresa";
            this.lbl_empresa.Size = new System.Drawing.Size(47, 15);
            this.lbl_empresa.TabIndex = 46;
            this.lbl_empresa.Text = "Empresa";
            // 
            // uc_empresa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.lbl_empresa);
            this.Controls.Add(this.cbo_empresa);
            this.Name = "uc_empresa";
            this.Size = new System.Drawing.Size(333, 25);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevComponents.DotNetBar.Controls.ComboBoxEx cbo_empresa;
        private DevComponents.DotNetBar.LabelX lbl_empresa;

    }
}
