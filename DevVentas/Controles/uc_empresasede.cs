﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Capa_Entidad;
using System.Collections;
using Capa_Cliente.Basico;

namespace Capa_Cliente.Controles
{
    public partial class uc_empresasede : UserControl
    {
        PosicionControl _posicion;
        clsUsuario _objusuario = new clsUsuario();
        Boolean _todos;

        public event EventHandler DatosCambiados;

        private void OnDatosCambiados(EventArgs e)
        {
            if (DatosCambiados != null) DatosCambiados(this, e);
        }


        public uc_empresasede()
        {
            InitializeComponent();
        }

        public void Inicializar(clsUsuario objusuario, Boolean todos)
        {
            _objusuario = objusuario;
            _todos = todos;

            cbo_empresa.Enabled = cbo_sede.Enabled = true;

            Hashtable parametros = new Hashtable();
            parametros.Add("@p_idempresa", _objusuario.IdEmpresa);
            parametros.Add("@p_idusuario", _objusuario.IdUsuario);
            clsFunciones.Instancia.CargarCombo(this.cbo_empresa, "Empresa", "AyudaLista", parametros, _todos);

            if (_objusuario.IdEmpresa > 0)
            {
                this.cbo_empresa.SelectedValue = _objusuario.IdEmpresa;
                this.cbo_empresa.Enabled = false;
            }

            cbo_empresa_SelectionChangeCommitted(null, null);
        }

        public void cbo_empresa_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cbo_sede.DataSource = null;

            if (cbo_empresa.SelectedValue != null)
            {             
                Hashtable parametros = new Hashtable();
                parametros.Add("@p_idempresa", cbo_empresa.SelectedValue);
                clsFunciones.Instancia.CargarCombo(cbo_sede, "Sede", "AyudaLista", parametros, _todos);

                if (_objusuario.IdSede > 0)
                {
                    cbo_sede.SelectedValue = _objusuario.IdSede;
                    cbo_sede.Enabled = false;
                }

                OnDatosCambiados(e);
            }

        }

        public PosicionControl PosicionControl
        {
            get { return _posicion; }
            set
            {
                _posicion = value;

                switch (_posicion)
                {
                    case PosicionControl.EnLinea:

                        lbl_empresa.Location = new Point(3, 7);
                        lbl_sede.Location = new Point(22, 33);

                        cbo_empresa.Location = new Point(56, 4);
                        cbo_sede.Location = new Point(56, 30);

                        this.Size = new Size(180, 54);

                        break;
                    case PosicionControl.EnParalelo:

                        lbl_empresa.Location = new Point(1, 3);
                        lbl_sede.Location = new Point(1, 45);

                        cbo_empresa.Location = new Point(4, 21);
                        cbo_sede.Location = new Point(4, 61);

                        this.Size = new Size(128, 87);
                        break;
                    case PosicionControl.Horizontal:
                        lbl_empresa.Location = new Point(1, 7);
                        lbl_sede.Location = new Point(191, 7);

                        cbo_empresa.Location = new Point(56, 4);
                        cbo_sede.Location = new Point(225, 4);

                        this.Size = new Size(352, 27);
                        break;
                    default:
                        break;
                }
                cbo_empresa.Size = new Size(124, 20);
                cbo_sede.Size = new Size(124, 20);
            }
        }

        private void cbo_sede_SelectionChangeCommitted(object sender, EventArgs e)
        {
            OnDatosCambiados(e);
        }
    }
}
