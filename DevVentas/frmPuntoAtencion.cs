﻿using Capa_Cliente.Basico;
using Capa_Entidad;
using Capa_Negocio;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Cliente
{
    public partial class frmPuntoAtencion : frmBasico
    {
        Boolean _nuevo = true;
        DataTableG _dataResultado = null;
        clsPuntoAtencion _objentidad = new clsPuntoAtencion();
        Int16 _idempresa = 0;
        Int16 _idsede = 0;
        Int16 _idusuario = 0;
        clsUsuario _objusuario = new clsUsuario();

        public frmPuntoAtencion()
        {
            InitializeComponent();
        }

        private void frmPuntoAtencion_Load(object sender, EventArgs e)
        {
            //_objusuario = ((frmMDIPrincipal)this.MdiParent)._objusuario;
            _objusuario.IdUsuario=1;
            clsFunciones.Usuario = _objusuario;
            uc_empresasedefiltro.Inicializar(_objusuario, true);
            uc_empresasedealcance.Inicializar(_objusuario, true);
            Hashtable parametros = new Hashtable();
            clsFunciones.Instancia.CargarCombo(cbo_tipoatencion, "TipoAtencion", "AyudaLista", parametros, false);
        }

        private void btn_nuevo_Click(object sender, EventArgs e)
        {
            _nuevo = true;
            uc_empresasedealcance.Inicializar(_objusuario, true);
            cbo_tipoatencion.Enabled = _nuevo;
            this.txt_descripcion.Clear();
            txt_descripcion.Focus();
        }

        private void btn_grabar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ActivarGuardar())
                {
                    Grabar();
                }
                else
                {
                    MessageBox.Show("Favor Ingresar Datos a Modificar", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void Grabar()
        {
            Boolean resultado = true;
            clsPuntoAtencion objentidad = new clsPuntoAtencion();

            objentidad.IdTipoAtencion = (Int16)cbo_tipoatencion.SelectedValue;
            objentidad.IdUsuario = (Int32)cbo_usuario.SelectedValue;
            objentidad.Descripcion = txt_descripcion.Text;
            objentidad.IdEstado = (Int16)Estado.ACTIVO;
            objentidad.IdEmpresa = Convert.ToInt16(uc_empresasede.cbo_empresa.SelectedValue);
            objentidad.IdSede = Convert.ToInt16(uc_empresasede.cbo_sede.SelectedValue);

            if (_nuevo)
            {
                resultado = clsGestionarMaestro.Instancia.InsertarPuntoAtencion(objentidad);
                if (resultado)
                {
                    MessageBox.Show("Datos Registrados", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.tab_padre.SelectedTab = tab_mostrar;
                    this.btn_actualizar_Click(null, null);

                    //ActivarBotones(false);
                }

                else

                    MessageBox.Show("Datos no Registrados", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (MessageBox.Show("¿ Esta Seguro de Actualizar los Datos ?", "Informacion", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    objentidad.IdPuntoAtencion = _objentidad.IdPuntoAtencion;

                    resultado = clsGestionarMaestro.Instancia.ActualizarPuntoAtencion(objentidad);
                    if (resultado)
                    {
                        //MessageBox.Show("Datos Actualizados", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        tab_padre.SelectedTab = tab_mostrar;
                        this.btn_actualizar_Click(null, null);
                        //ActivarBotones(false);
                    }

                    else

                        MessageBox.Show("Datos no Actualizados", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }

        }

        private Boolean ActivarGuardar()
        {
            return this.btn_grabar.Enabled = this.txt_descripcion.TextLength > 0 && cbo_usuario.SelectedValue!=null

                 && (
                    (_objentidad == null ? true : (Int16)cbo_tipoatencion.SelectedValue != _objentidad.IdTipoAtencion)
                 || (_objentidad == null ? true : (Int32)cbo_usuario.SelectedValue != _objentidad.IdUsuario)
                 || (_objentidad == null ? true : (Int16)uc_empresasede.cbo_empresa.SelectedValue != _objentidad.IdEmpresa)
                 || (_objentidad == null ? true : (Int16)uc_empresasede.cbo_sede.SelectedValue != _objentidad.IdSede)
                 || (_objentidad == null ? true : txt_descripcion.Text != _objentidad.Descripcion)
                     );
        }

        private void btn_actualizar_Click(object sender, EventArgs e)
        {
            if (!bg_work.IsBusy) CargarDatos();
        }

        private void CargarDatos()
        {
            try
            {
                this.btn_actualizar.Enabled = false;
                _idempresa = Convert.ToInt16(uc_empresasedefiltro.cbo_empresa.SelectedValue);
                _idsede = Convert.ToInt16(uc_empresasedefiltro.cbo_sede.SelectedValue);
                _idusuario = Convert.ToInt16(cbo_usuariofiltro.SelectedValue);
                //EstablecerMensajeProceso("Obteniendo información...");
                bg_work.RunWorkerAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void bg_work_DoWork(object sender, DoWorkEventArgs e)
        {
            clsPuntoAtencion objentidad = new clsPuntoAtencion();
            objentidad.IdEmpresa = _idempresa;
            objentidad.IdSede = _idsede;
            objentidad.IdUsuario = _idusuario;
            objentidad.IdUsuarioPrincipal = clsFunciones.Usuario.IdUsuario; ;

            try
            {
                _dataResultado = clsGestionarMaestro.Instancia.ListarPuntoAtencion(objentidad);
                e.Result = _dataResultado;
            }
            catch (Exception ex)
            {

                //AvisosNG.Error(ex.Message);
            }
        }

        private void bg_work_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                this.btn_actualizar.Enabled = true;

                if (e.Result != null)
                {
                    uc_grilla.dgv_mostrar.DataSource = null;
                    uc_grilla.dgv_mostrar.Columns.Clear();
                    uc_grilla.dgv_mostrar.DataSource = ((DataTableG)e.Result).ConvertToDataTable();
                    uc_grilla.dgv_mostrar.Refresh();
                    uc_grilla.dgv_mostrar.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
                    uc_grilla.ColumnaVisible("IdTipoAtencion", false);
                    uc_grilla.ColumnaVisible("IdUsuario", false);
                    uc_grilla.ColumnaVisible("IdEstado", false);
                    uc_grilla.ColumnaVisible("IdEmpresa", false);
                    uc_grilla.ColumnaVisible("IdSede", false);

                }

                ActivarBotones();

            }
            catch (Exception ex)
            {

            }
        }

        public void ActivarBotones()
        {
            btn_nuevo.Enabled = true;
            btn_editar.Enabled = uc_grilla.dgv_mostrar.Rows.Count > 0;
            btn_eliminar.Enabled = uc_grilla.dgv_mostrar.Rows.Count > 0;
            btn_grabar.Enabled = tab_padre.SelectedTab != tab_mostrar;
            btn_cancelar.Enabled = tab_padre.SelectedTab != tab_mostrar;
        }

        private void txt_descripcion_TextChanged(object sender, EventArgs e)
        {
            ActivarGuardar();
        }

        private void cbo_tipoatencion_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ActivarGuardar();
        }

        private void cbo_usuario_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cbo_usuario.SelectedItem != null)
            {
                Hashtable parametros = new Hashtable();
                clsUsuario objentidad = new clsUsuario();

                DataRowView dr = ((DataRowView)cbo_usuario.SelectedItem);
                objentidad.IdUsuario = Convert.ToInt16(dr.Row.ItemArray[0]);
                objentidad.IdEmpresa = Convert.ToInt16(dr.Row.ItemArray[2]);
                objentidad.IdSede = Convert.ToInt16(dr.Row.ItemArray[3]);
               

                uc_empresasede.Inicializar(objentidad, false);
            }
            ActivarGuardar();
        }

        private void btn_editar_Click(object sender, EventArgs e)
        {
            _nuevo = false;           
            cbo_tipoatencion.Enabled = _nuevo;

            this.txt_descripcion.Focus();
            
            _objentidad.IdPuntoAtencion = Convert.ToInt16(uc_grilla._filaactiva.Cells["IdPuntoAtencion"].Value);
            _objentidad.Descripcion = uc_grilla._filaactiva.Cells["Descripcion"].Value.ToString();
            _objentidad.IdTipoAtencion = Convert.ToInt16(uc_grilla._filaactiva.Cells["IdTipoAtencion"].Value);
            _objentidad.IdUsuario = Convert.ToInt16(uc_grilla._filaactiva.Cells["IdUsuario"].Value);
            _objentidad.IdEmpresa = Convert.ToInt16(uc_grilla._filaactiva.Cells["IdEmpresa"].Value);
            _objentidad.IdSede = Convert.ToInt16(uc_grilla._filaactiva.Cells["IdSede"].Value);

            uc_empresasedealcance.Inicializar(_objusuario, true);
            cbo_tipoatencion.SelectedValue = _objentidad.IdTipoAtencion;
            cbo_usuario.SelectedValue = _objentidad.IdUsuario;
            txt_descripcion.Text = _objentidad.Descripcion;
            uc_empresasede.cbo_empresa.SelectedValue = _objentidad.IdEmpresa;
            uc_empresasede.cbo_empresa_SelectionChangeCommitted(null, null);
            uc_empresasede.cbo_sede.SelectedValue = _objentidad.IdSede;
            this.tab_padre.SelectedTab = tab_registro;
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            ActivarBotones();
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿ Esta Seguro de Eliminar el Registro ?", "Informacion", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                try
                {
                    //_filaactiva = dgv_mostrar.CurrentRow;
                    _objentidad.IdPuntoAtencion = Convert.ToInt16(uc_grilla._filaactiva.Cells["IdPuntoAtencion"].Value);

                    clsGestionarMaestro.Instancia.EliminarPuntoAtencion(_objentidad);
                    this.btn_actualizar_Click(null, null);
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        private void uc_empresasede_DatosCambiados(object sender, EventArgs e)
        {
            ActivarGuardar();
        }

        private void uc_empresasedealcance_DatosCambiados(object sender, EventArgs e)
        {
            Hashtable parametros = new Hashtable();
            parametros.Add("@p_idempresa", uc_empresasedealcance.cbo_empresa.SelectedValue);
            parametros.Add("@p_idsede", uc_empresasedealcance.cbo_sede.SelectedValue);
            clsFunciones.Instancia.CargarCombo(cbo_usuario, "Usuario", "AyudaLista", parametros, false);
            cbo_usuario_SelectionChangeCommitted(null, null);
        }

        private void uc_empresasedefiltro_DatosCambiados(object sender, EventArgs e)
        {
            Hashtable parametros = new Hashtable();
            parametros.Add("@p_idempresa", uc_empresasedefiltro.cbo_empresa.SelectedValue);
            parametros.Add("@p_idsede", uc_empresasedefiltro.cbo_sede.SelectedValue);
            clsFunciones.Instancia.CargarCombo(cbo_usuariofiltro, "Usuario", "AyudaLista", parametros, true);
        }

        private void frmPuntoAtencion_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == clsFunciones.Teclas("Mostrar"))
            {
                if (btn_actualizar.Enabled)
                {
                    btn_actualizar_Click(null, null);
                }
            }
            if (e.KeyData == clsFunciones.Teclas("Nuevo"))
            {
                if (btn_nuevo.Enabled)
                {
                    base.btn_nuevo_Click(null, null);
                    this.btn_nuevo_Click(null, null);
                }
            }
            if (e.KeyData == clsFunciones.Teclas("Grabar"))
            {
                if (btn_grabar.Enabled)
                {
                    btn_grabar_Click(null, null);
                }
            }
            if (e.KeyData == clsFunciones.Teclas("Editar"))
            {
                if (btn_editar.Enabled)
                {
                    base.btn_editar_Click(null, null);
                    btn_editar_Click(null, null);
                }
            }
            if (e.KeyData == clsFunciones.Teclas("Cancelar"))
            {
                if (btn_cancelar.Enabled)
                {
                    base.btn_cancelar_Click(null, null);
                    btn_cancelar_Click(null, null);
                }
            }
            if (e.KeyData == clsFunciones.Teclas("Eliminar"))
            {
                if (btn_eliminar.Enabled)
                {
                    btn_eliminar_Click(null, null);
                }
            }
            if (e.KeyData == clsFunciones.Teclas("Salir"))
            {
                if (btn_salir.Enabled)
                {
                    btn_salir_Click(null, null);
                }
            }
        }
    }
}
