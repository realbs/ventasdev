﻿namespace Capa_Cliente
{
    partial class frmPuntoAtencion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uc_grilla = new Capa_Cliente.Controles.uc_grilla();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.uc_empresasede = new Capa_Cliente.Controles.uc_empresasede();
            this.grupo_alcance = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.uc_empresasedealcance = new Capa_Cliente.Controles.uc_empresasede();
            this.lbl_usuario = new DevComponents.DotNetBar.LabelX();
            this.cbo_usuario = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.cbo_tipoatencion = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lbl_descripcion = new DevComponents.DotNetBar.LabelX();
            this.txt_descripcion = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.bg_work = new System.ComponentModel.BackgroundWorker();
            this.uc_empresasedefiltro = new Capa_Cliente.Controles.uc_empresasede();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.cbo_usuariofiltro = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tab_padre)).BeginInit();
            this.tab_padre.SuspendLayout();
            this.tab_registro1.SuspendLayout();
            this.tab_mostrar1.SuspendLayout();
            this.panelEx1.SuspendLayout();
            this.grupo_alcance.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_titulo
            // 
            // 
            // 
            // 
            this.lbl_titulo.BackgroundStyle.Class = "";
            this.lbl_titulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_titulo.Size = new System.Drawing.Size(745, 35);
            this.lbl_titulo.Text = "Gestionar Punto de Atención";
            // 
            // bar1
            // 
            this.bar1.Size = new System.Drawing.Size(745, 33);
            // 
            // btn_actualizar
            // 
            this.btn_actualizar.Click += new System.EventHandler(this.btn_actualizar_Click);
            // 
            // btn_nuevo
            // 
            this.btn_nuevo.Click += new System.EventHandler(this.btn_nuevo_Click);
            // 
            // btn_grabar
            // 
            this.btn_grabar.Click += new System.EventHandler(this.btn_grabar_Click);
            // 
            // btn_editar
            // 
            this.btn_editar.Click += new System.EventHandler(this.btn_editar_Click);
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Enabled = false;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // btn_eliminar
            // 
            this.btn_eliminar.Click += new System.EventHandler(this.btn_eliminar_Click);
            // 
            // tab_padre
            // 
            this.tab_padre.SelectedTabIndex = 1;
            this.tab_padre.Size = new System.Drawing.Size(745, 396);
            this.tab_padre.Controls.SetChildIndex(this.tab_registro1, 0);
            this.tab_padre.Controls.SetChildIndex(this.tab_mostrar1, 0);
            // 
            // tab_registro1
            // 
            this.tab_registro1.Controls.Add(this.panelEx1);
            this.tab_registro1.Size = new System.Drawing.Size(745, 370);
            this.tab_registro1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(231)))));
            this.tab_registro1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.tab_registro1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tab_registro1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tab_registro1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tab_registro1.Style.GradientAngle = 90;
            // 
            // tab_mostrar1
            // 
            this.tab_mostrar1.Controls.Add(this.labelX1);
            this.tab_mostrar1.Controls.Add(this.cbo_usuariofiltro);
            this.tab_mostrar1.Controls.Add(this.uc_empresasedefiltro);
            this.tab_mostrar1.Controls.Add(this.uc_grilla);
            this.tab_mostrar1.Size = new System.Drawing.Size(745, 370);
            this.tab_mostrar1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(231)))));
            this.tab_mostrar1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.tab_mostrar1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tab_mostrar1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tab_mostrar1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tab_mostrar1.Style.GradientAngle = 90;
            // 
            // btn_reporte
            // 
            this.btn_reporte.Visible = false;
            // 
            // uc_grilla
            // 
            this.uc_grilla.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uc_grilla.Location = new System.Drawing.Point(4, 55);
            this.uc_grilla.Name = "uc_grilla";
            this.uc_grilla.Size = new System.Drawing.Size(738, 311);
            this.uc_grilla.TabIndex = 23;
            // 
            // panelEx1
            // 
            this.panelEx1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.uc_empresasede);
            this.panelEx1.Controls.Add(this.grupo_alcance);
            this.panelEx1.Controls.Add(this.lbl_usuario);
            this.panelEx1.Controls.Add(this.cbo_usuario);
            this.panelEx1.Controls.Add(this.labelX5);
            this.panelEx1.Controls.Add(this.cbo_tipoatencion);
            this.panelEx1.Controls.Add(this.lbl_descripcion);
            this.panelEx1.Controls.Add(this.txt_descripcion);
            this.panelEx1.Location = new System.Drawing.Point(177, 23);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(391, 335);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 7;
            // 
            // uc_empresasede
            // 
            this.uc_empresasede.BackColor = System.Drawing.Color.Transparent;
            this.uc_empresasede.Location = new System.Drawing.Point(73, 154);
            this.uc_empresasede.Name = "uc_empresasede";
            this.uc_empresasede.PosicionControl = Capa_Entidad.PosicionControl.EnLinea;
            this.uc_empresasede.Size = new System.Drawing.Size(282, 54);
            this.uc_empresasede.TabIndex = 62;
            this.uc_empresasede.DatosCambiados += new System.EventHandler(this.uc_empresasede_DatosCambiados);
            // 
            // grupo_alcance
            // 
            this.grupo_alcance.CanvasColor = System.Drawing.SystemColors.Control;
            this.grupo_alcance.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grupo_alcance.Controls.Add(this.uc_empresasedealcance);
            this.grupo_alcance.Location = new System.Drawing.Point(67, 15);
            this.grupo_alcance.Name = "grupo_alcance";
            this.grupo_alcance.Size = new System.Drawing.Size(293, 82);
            // 
            // 
            // 
            this.grupo_alcance.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grupo_alcance.Style.BackColorGradientAngle = 90;
            this.grupo_alcance.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grupo_alcance.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grupo_alcance.Style.BorderBottomWidth = 1;
            this.grupo_alcance.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grupo_alcance.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grupo_alcance.Style.BorderLeftWidth = 1;
            this.grupo_alcance.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grupo_alcance.Style.BorderRightWidth = 1;
            this.grupo_alcance.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grupo_alcance.Style.BorderTopWidth = 1;
            this.grupo_alcance.Style.Class = "";
            this.grupo_alcance.Style.CornerDiameter = 4;
            this.grupo_alcance.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grupo_alcance.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grupo_alcance.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grupo_alcance.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grupo_alcance.StyleMouseDown.Class = "";
            this.grupo_alcance.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grupo_alcance.StyleMouseOver.Class = "";
            this.grupo_alcance.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grupo_alcance.TabIndex = 61;
            this.grupo_alcance.Text = "Alcance";
            // 
            // uc_empresasedealcance
            // 
            this.uc_empresasedealcance.BackColor = System.Drawing.Color.Transparent;
            this.uc_empresasedealcance.Location = new System.Drawing.Point(3, 3);
            this.uc_empresasedealcance.Name = "uc_empresasedealcance";
            this.uc_empresasedealcance.PosicionControl = Capa_Entidad.PosicionControl.EnLinea;
            this.uc_empresasedealcance.Size = new System.Drawing.Size(282, 54);
            this.uc_empresasedealcance.TabIndex = 60;
            this.uc_empresasedealcance.DatosCambiados += new System.EventHandler(this.uc_empresasedealcance_DatosCambiados);
            // 
            // lbl_usuario
            // 
            this.lbl_usuario.AutoSize = true;
            // 
            // 
            // 
            this.lbl_usuario.BackgroundStyle.Class = "";
            this.lbl_usuario.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_usuario.Location = new System.Drawing.Point(84, 106);
            this.lbl_usuario.Name = "lbl_usuario";
            this.lbl_usuario.Size = new System.Drawing.Size(40, 15);
            this.lbl_usuario.TabIndex = 59;
            this.lbl_usuario.Text = "Usuario";
            // 
            // cbo_usuario
            // 
            this.cbo_usuario.DisplayMember = "Text";
            this.cbo_usuario.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_usuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_usuario.FormattingEnabled = true;
            this.cbo_usuario.ItemHeight = 14;
            this.cbo_usuario.Location = new System.Drawing.Point(130, 103);
            this.cbo_usuario.Name = "cbo_usuario";
            this.cbo_usuario.Size = new System.Drawing.Size(225, 20);
            this.cbo_usuario.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_usuario.TabIndex = 58;
            this.cbo_usuario.SelectionChangeCommitted += new System.EventHandler(this.cbo_usuario_SelectionChangeCommitted);
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.Class = "";
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(52, 132);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(70, 15);
            this.labelX5.TabIndex = 57;
            this.labelX5.Text = "Tipo Atención";
            // 
            // cbo_tipoatencion
            // 
            this.cbo_tipoatencion.DisplayMember = "Text";
            this.cbo_tipoatencion.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_tipoatencion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_tipoatencion.FormattingEnabled = true;
            this.cbo_tipoatencion.ItemHeight = 14;
            this.cbo_tipoatencion.Location = new System.Drawing.Point(130, 129);
            this.cbo_tipoatencion.Name = "cbo_tipoatencion";
            this.cbo_tipoatencion.Size = new System.Drawing.Size(225, 20);
            this.cbo_tipoatencion.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_tipoatencion.TabIndex = 0;
            this.cbo_tipoatencion.SelectionChangeCommitted += new System.EventHandler(this.cbo_tipoatencion_SelectionChangeCommitted);
            // 
            // lbl_descripcion
            // 
            this.lbl_descripcion.AutoSize = true;
            // 
            // 
            // 
            this.lbl_descripcion.BackgroundStyle.Class = "";
            this.lbl_descripcion.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_descripcion.Location = new System.Drawing.Point(32, 215);
            this.lbl_descripcion.Name = "lbl_descripcion";
            this.lbl_descripcion.Size = new System.Drawing.Size(92, 15);
            this.lbl_descripcion.TabIndex = 52;
            this.lbl_descripcion.Text = "Punto de Atención";
            // 
            // txt_descripcion
            // 
            // 
            // 
            // 
            this.txt_descripcion.Border.Class = "TextBoxBorder";
            this.txt_descripcion.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_descripcion.Location = new System.Drawing.Point(130, 214);
            this.txt_descripcion.Name = "txt_descripcion";
            this.txt_descripcion.Size = new System.Drawing.Size(225, 20);
            this.txt_descripcion.TabIndex = 2;
            this.txt_descripcion.TextChanged += new System.EventHandler(this.txt_descripcion_TextChanged);
            // 
            // bg_work
            // 
            this.bg_work.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bg_work_DoWork);
            this.bg_work.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bg_work_RunWorkerCompleted);
            // 
            // uc_empresasedefiltro
            // 
            this.uc_empresasedefiltro.BackColor = System.Drawing.Color.Transparent;
            this.uc_empresasedefiltro.Location = new System.Drawing.Point(12, 22);
            this.uc_empresasedefiltro.Name = "uc_empresasedefiltro";
            this.uc_empresasedefiltro.PosicionControl = Capa_Entidad.PosicionControl.Horizontal;
            this.uc_empresasedefiltro.Size = new System.Drawing.Size(352, 27);
            this.uc_empresasedefiltro.TabIndex = 61;
            this.uc_empresasedefiltro.DatosCambiados += new System.EventHandler(this.uc_empresasedefiltro_DatosCambiados);
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(383, 28);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(40, 15);
            this.labelX1.TabIndex = 63;
            this.labelX1.Text = "Usuario";
            // 
            // cbo_usuariofiltro
            // 
            this.cbo_usuariofiltro.DisplayMember = "Text";
            this.cbo_usuariofiltro.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_usuariofiltro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_usuariofiltro.FormattingEnabled = true;
            this.cbo_usuariofiltro.ItemHeight = 14;
            this.cbo_usuariofiltro.Location = new System.Drawing.Point(429, 25);
            this.cbo_usuariofiltro.Name = "cbo_usuariofiltro";
            this.cbo_usuariofiltro.Size = new System.Drawing.Size(225, 20);
            this.cbo_usuariofiltro.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_usuariofiltro.TabIndex = 62;
            // 
            // frmPuntoAtencion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 431);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.Name = "frmPuntoAtencion";
            this.Text = "Gestionar Punto de Atención";
            this.Load += new System.EventHandler(this.frmPuntoAtencion_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPuntoAtencion_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tab_padre)).EndInit();
            this.tab_padre.ResumeLayout(false);
            this.tab_registro1.ResumeLayout(false);
            this.tab_mostrar1.ResumeLayout(false);
            this.tab_mostrar1.PerformLayout();
            this.panelEx1.ResumeLayout(false);
            this.panelEx1.PerformLayout();
            this.grupo_alcance.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controles.uc_grilla uc_grilla;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.LabelX lbl_usuario;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_usuario;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_tipoatencion;
        private DevComponents.DotNetBar.LabelX lbl_descripcion;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_descripcion;
        private System.ComponentModel.BackgroundWorker bg_work;
        private Controles.uc_empresasede uc_empresasedealcance;
        private Controles.uc_empresasede uc_empresasedefiltro;
        private Controles.uc_empresasede uc_empresasede;
        private DevComponents.DotNetBar.Controls.GroupPanel grupo_alcance;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_usuariofiltro;
    }
}