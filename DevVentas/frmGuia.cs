﻿using Capa_Cliente.Basico;
using Capa_Entidad;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Cliente
{
    public partial class frmGuia : frmBasico
    {
        clsUsuario _objusuario = new clsUsuario();
        Int16 _idtipomovimiento = 0;

        public frmGuia()
        {
            InitializeComponent();
        }

        public frmGuia(TipoMovimiento tipomovimiento)
        {
            _idtipomovimiento = (Int16)tipomovimiento;
            InitializeComponent();
            //InicializarMenus();

            //switch ((TipoMovimiento)_idtipomovimiento)
            //{
            //    case TipoMovimiento.Compras:
            //        lbl_titulo.Text = "COMPRAS"; break;

            //    case TipoMovimiento.Ventas:
            //        lbl_titulo.Text = "VENTAS"; break;

            //    case TipoMovimiento.Cotizaciones:
            //        lbl_titulo.Text = "COTIZACIONES"; break;

            //    case TipoMovimiento.Pedidos:
            //        lbl_titulo.Text = "PEDIDOS"; break;

            //    default:
            //        lbl_titulo.Text = "MOVIMIENTOS"; break;

            //}

     
            //_idtipoatencion = (Int16)TipoAtencion.RECAUDACION;
        }

        private void frmGuia_Load(object sender, EventArgs e)
        {
            _objusuario.IdUsuario = 1;
            clsFunciones.Usuario = _objusuario;

            dt_fechaemision.Value = DateTime.Now;
            dt_fechatraslado.Value = DateTime.Now;
            dt_fechadesde.Value = DateTime.Now;
            dt_fechahasta.Value = DateTime.Now;

            _objusuario =clsFunciones.Usuario;

            uc_empresasedefiltro.Inicializar(_objusuario, true);
            uc_empresasede.Inicializar(_objusuario, false);

            ActivarBotones();
            CargarComboTipoMovimiento();
            cbo_tipomovimientobuscar.Enabled = cbo_tipomovimiento.Enabled = false;
            cbo_tipomovimientobuscar.SelectedValue = cbo_tipomovimiento.SelectedValue = _idtipomovimiento;



            Hashtable parametros = new Hashtable();
            clsFunciones.Instancia.CargarCombo(cbo_departamentop, "Departamento", "AyudaLista", parametros, false);
            cbo_departamentop_SelectionChangeCommitted(null, null);
            clsFunciones.Instancia.CargarCombo(cbo_departamentoll, "Departamento", "AyudaLista", parametros, false);
            cbo_departamentoll_SelectionChangeCommitted(null, null);

            clsFunciones.Instancia.CargarCombo(cbo_modalidadtraslado, "ModalidadTraslado", "AyudaLista", parametros, false);
            clsFunciones.Instancia.CargarCombo(cbo_motivotraslado, "MotivoTraslado", "AyudaLista", parametros, false);

        }

        public void ActivarBotones()
        {
            btn_nuevo.Enabled = true;
            btn_editar.Enabled = uc_grilla.dgv_mostrar.Rows.Count > 0;
            btn_eliminar.Enabled = uc_grilla.dgv_mostrar.Rows.Count > 0 && (Convert.ToInt16(uc_grilla._filaactiva.Cells["IdEstado"].Value) == (Int16)Estado.ACTIVO);
            btn_grabar.Enabled = tab_padre.SelectedTab != tab_mostrar;
            btn_cancelar.Enabled = tab_padre.SelectedTab != tab_mostrar;
            btn_reporte.Enabled = uc_grilla.dgv_mostrar.Rows.Count > 0;
            //btn_abonos.Enabled = uc_grilla.dgv_mostrar.Rows.Count > 0 && (Convert.ToInt16(uc_grilla._filaactiva.Cells["EsCredito"].Value) == 1) && (Convert.ToInt16(uc_grilla._filaactiva.Cells["IdEstado"].Value) == (Int16)Estado.ACTIVO);
            btn_buscar.Enabled = tab_padre.SelectedTab != tab_mostrar;
        }

        private void CargarComboTipoMovimiento()
        {
            Hashtable parametros = new Hashtable();

            clsFunciones.Instancia.CargarCombo(cbo_tipomovimiento, "TipoMovimiento", "AyudaLista", parametros, false);
            clsFunciones.Instancia.CargarCombo(cbo_tipomovimientobuscar, "TipoMovimiento", "AyudaLista", parametros, false);
            clsFunciones.Instancia.CargarCombo(cbo_documento, "DocumentoGuia", "AyudaLista", parametros, false);
            cbo_documento_SelectionChangeCommitted(null, null);
        }

        private void cbo_documento_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //ObtenerSerie();
            //ActivarGuardar();
            //ObtenerProceso();
        }

        private void lbl_persona_Click(object sender, EventArgs e)
        {

        }

        private void cbo_departamentop_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cbo_departamentop.SelectedValue != null)
            {
                Hashtable parametros = new Hashtable();
                parametros.Add("@p_iddepartamento", cbo_departamentop.SelectedValue);
                clsFunciones.Instancia.CargarCombo(cbo_provinciap, "Provincia", "AyudaLista", parametros, false);
                cbo_provinciap_SelectionChangeCommitted(null, null);
                //ActivarGuardar();
            }
        }

        private void cbo_departamentoll_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cbo_departamentoll.SelectedValue != null)
            {
                Hashtable parametros = new Hashtable();
                parametros.Add("@p_iddepartamento", cbo_departamentoll.SelectedValue);
                clsFunciones.Instancia.CargarCombo(cbo_provinciall, "Provincia", "AyudaLista", parametros, false);
                cbo_provinciall_SelectionChangeCommitted(null, null);
                //ActivarGuardar();
            }
        }

        private void cbo_provinciap_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cbo_provinciap.SelectedValue != null)
            {
                Hashtable parametros = new Hashtable();
                parametros.Add("@p_idprovincia", cbo_provinciap.SelectedValue);
                clsFunciones.Instancia.CargarCombo(cbo_distritop, "Distrito", "AyudaLista", parametros, false);
                //ActivarGuardar();
            }
        }       

        private void cbo_provinciall_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cbo_provinciall.SelectedValue != null)
            {
                Hashtable parametros = new Hashtable();
                parametros.Add("@p_idprovincia", cbo_provinciall.SelectedValue);
                clsFunciones.Instancia.CargarCombo(cbo_distritoll, "Distrito", "AyudaLista", parametros, false);
                //ActivarGuardar();
            }
        }
    }
}
