﻿using Capa_Entidad;
using Capa_Negocio;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZXing;
using ZXing.QrCode.Internal;
using ZXing.Rendering;

namespace Capa_Cliente.Basico
{
    public class clsFunciones
    {
        #region Obtener Instancia

        private static readonly clsFunciones _objgestordao = new clsFunciones();

        public static clsFunciones Instancia
        {
            get { return _objgestordao; }
        }

        #endregion

        const Int32 filaInicio = 3;
        const Int32 columnaInicio = 0;
        private Hashtable _listamaestra = new Hashtable();

        public void CargarCombo(DevComponents.DotNetBar.Controls.ComboBoxEx combo, String entidad, String clave, Hashtable parametros, Boolean todos)
        {
            DataTable data = new DataTable();

            data = ObtenerAyudaLista(entidad, clave, parametros);

            if (todos)
            {
                DataRow dr = data.NewRow();
                dr["Id"] = 0;
                dr["Nombre"] = "TODOS";
                //data.Rows.Add(dr);
                data.Rows.InsertAt(dr, 0);
                //data.DefaultView.Sort = "Id ASC";
            }

            combo.DataSource = null;
            combo.DataSource = data;
            combo.DisplayMember = "Nombre";
            combo.ValueMember = "Id";
        }

        public DataTable ObtenerAyudaLista(String entidad, String clave, Hashtable parametros)
        {
            DataTable dt = new DataTable();

            try
            {
                string clavehash = ObtenerClaveHash(entidad, clave, parametros);

                if (_listamaestra.Contains(clavehash))
                {
                    dt = Clonar((DataTable)_listamaestra[clavehash]);
                }
                else
                {
                    dt = clsGestionarMaestro.Instancia.ObtenerAyudaLista(entidad, clave, parametros);
                    _listamaestra[clavehash] = Clonar(dt);
                }

            }

            catch (Exception ex)
            {
                //ExcepcionNG.Guardar(ex.Message, enumTipoExcepcion.Error);
            }

            return dt; //.ConvertToByte();
        }

        private string ObtenerClaveHash(String entidad, String clave, Hashtable parametros)
        {
            string clavehash = string.Concat(entidad, clave);

            foreach (DictionaryEntry item in parametros)
            {
                if (item.Value != null && item.Key.ToString().ToLower() != "@return_value")
                {
                    clavehash = String.Concat(clavehash, item.Key.ToString(), item.Value.ToString());
                }
            }

            return clavehash;
        }

        public void LimpiarCache()
        {
            _listamaestra.Clear();
        }

        public static string Encriptar(string _cadenaAencriptar)
        {
            string result = string.Empty;
            byte[] encryted = System.Text.Encoding.Unicode.GetBytes(_cadenaAencriptar);
            result = Convert.ToBase64String(encryted);
            return result;
        }

        public static string DesEncriptar(string _cadenaAdesencriptar)
        {
            string result = string.Empty;
            byte[] decryted = Convert.FromBase64String(_cadenaAdesencriptar);
            result = System.Text.Encoding.Unicode.GetString(decryted);
            return result;
        }

        public static String ObtenerCarpetaTemporal()
        {
            return Path.GetTempPath();
        }

        public static DateTime ObtenerFechaServidor()
        {
            return DateTime.Now;
        }

//        public static void GenerarTicket(clsMovimiento objentidad)
//        {
//            try
//            {
//                DataSet dt;
//                dt = clsGestionarMovimiento.Instancia.TicketMovimiento(objentidad);

//                int n = 0; //obtiene el numero de columnas o guiones de ticket (60 mm->40 ; 70 mm ->46) tomar en cuenta que cada ticket tiene margen de 5 mm derecha e izquierda
//                // (58 mm ->32)
//                int ancho=0,alto = 0;

//                n = Convert.ToInt16(dt.Tables["Ticket"].Rows[0]["Columna"].ToString());
//                ancho = Convert.ToInt16(dt.Tables["Ticket"].Rows[0]["Ancho"].ToString());
//                alto = Convert.ToInt16(dt.Tables["Ticket"].Rows[0]["Alto"].ToString());

//                Int16 esfacturador = 0;
//                esfacturador = Convert.ToInt16(dt.Tables["Empresa"].Rows[0]["EsFacturador"].ToString());

//                Int16 escredito = 0;
//                if (dt.Tables["Movimiento"].Rows.Count > 0)
//                {
//                    escredito = Convert.ToInt16(dt.Tables["Movimiento"].Rows[0]["EsCredito"]);
//                }

//                Int16 activar = 0;
//                if (dt.Tables["Cliente"].Rows.Count > 0)
//                {
//                    activar = Convert.ToInt16(dt.Tables["Cliente"].Rows[0]["Activar"].ToString());
//                }
//                CrearTicket ticket = new CrearTicket(n,ancho,alto);

//                //Regex.Unescape quita regulares expresiones
//                ticket.ComandoCorte = Regex.Unescape(dt.Tables["Ticket"].Rows[0]["ComandoCorte"].ToString());
//                ticket.ComandoRenglon = Regex.Unescape(dt.Tables["Ticket"].Rows[0]["ComandoRenglon"].ToString());
//                ticket.ComandoCajon = Regex.Unescape(dt.Tables["Ticket"].Rows[0]["ComandoCajon"].ToString());
//                ticket.MostrarLogo = Convert.ToInt16(dt.Tables["Ticket"].Rows[0]["MostrarLogo"]);
//                ticket.MostrarComentario = Convert.ToInt16(dt.Tables["Ticket"].Rows[0]["MostrarComentario"]);
                

//                if (Convert.ToBoolean(ticket.MostrarLogo))
//                {
//                    if (dt.Tables["Empresa"].Rows[0]["Imagen"] != DBNull.Value)
//                    {
//                        ticket.HeaderImage = BytesToImage((Byte[])dt.Tables["Empresa"].Rows[0]["Imagen"]);
//                    }
//                }

//                //Image img = Image.FromFile("D:\\ImagenEmpresa2.jpg");
//                //ticket.HeaderImage = img;

//                BarcodeWriter qr = new BarcodeWriter();

//                var opciones = new ZXing.Common.EncodingOptions
//                {
//                    //Width = 200,
//                    //Height = 500,
//                    Margin = 0,
//                    PureBarcode = false
//                };

//                opciones.Hints.Add(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
//                qr.Renderer = new BitmapRenderer();
//                qr.Options = opciones;
//                qr.Format = BarcodeFormat.QR_CODE;                
//                Bitmap imagen = qr.Write(dt.Tables["Movimiento"].Rows[0]["CodigoQR"].ToString());            
                            
//                //Image qrimg = Image.FromFile("D:\\qr.png");
//                ticket.QRImage = imagen;

//                string fechatotal = DateTime.Now.ToLongDateString();
//                string fecha = DateTime.Now.ToShortDateString();
//                string hora = DateTime.Now.ToShortTimeString();
//                PrintDocument pd = new PrintDocument();
//                string impresorapredeterminada = pd.PrinterSettings.PrinterName;

//                if (Convert.ToBoolean(ticket.MostrarLogo))
//                {
//                    if (ticket.HeaderImage != null) ticket.ImprimirImagenTicket(impresorapredeterminada, 1); //imprime la cabecera
//                }

//                ticket.AbreCajon();//para abrir el cajon del dinero
//                //datos de la cabecera datos de la empresa
//                if (dt.Tables["Empresa"].Rows[0]["NombreComercial"] != String.Empty) ticket.TextoCentro(dt.Tables["Empresa"].Rows[0]["NombreComercial"].ToString());
//                if (dt.Tables["Empresa"].Rows[0]["Descripcion"] != String.Empty) ticket.TextoCentro(dt.Tables["Empresa"].Rows[0]["Descripcion"].ToString());
//                if (dt.Tables["Empresa"].Rows[0]["NroIdentidad"] != String.Empty) ticket.TextoIzquierda(dt.Tables["Empresa"].Rows[0]["TipoIdentidad"].ToString() + ": " + dt.Tables["Empresa"].Rows[0]["NroIdentidad"].ToString());
//                if (dt.Tables["Empresa"].Rows[0]["Direccion"] != String.Empty) ticket.TextoIzquierda("DIREC: " + dt.Tables["Empresa"].Rows[0]["Direccion"].ToString());
//                if (dt.Tables["Empresa"].Rows[0]["DireccionComplementaria"] != String.Empty) ticket.TextoIzquierda(dt.Tables["Empresa"].Rows[0]["DireccionComplementaria"].ToString());
//                if (dt.Tables["Empresa"].Rows[0]["Telefono"] != String.Empty) ticket.TextoIzquierda("TELEF: " + dt.Tables["Empresa"].Rows[0]["Telefono"].ToString());
//                if (dt.Tables["Empresa"].Rows[0]["Email"] != String.Empty) ticket.TextoIzquierda("EMAIL: " + dt.Tables["Empresa"].Rows[0]["Email"].ToString());
//                //ticket.TextoIzquierda("EXPEDIDO EN: LOCAL PRINCIPAL");
//                ticket.TextoIzquierda(" ");

//                //numero de documento
//                ticket.TextoCentro(dt.Tables["Movimiento"].Rows[0]["Documento"].ToString() + ": " + dt.Tables["Movimiento"].Rows[0]["NroDocumento"].ToString());
//                //
//                ticket.TextoIzquierda(" ");
//                ticket.TextoIzquierda("F.EMISION: " + Convert.ToDateTime(dt.Tables["Movimiento"].Rows[0]["FechaMovimiento"]).ToShortDateString());                
//                ticket.TextoIzquierda("F.VENCIMIENTO: " + Convert.ToDateTime(dt.Tables["Movimiento"].Rows[0]["FechaVencimiento"]).ToShortDateString());
//                ticket.TextoIzquierda("FORMA PAGO: " + (dt.Tables["Movimiento"].Rows[0]["FormaPago"]).ToString());
//                ticket.TextoIzquierda("F.IMPRESION: " + fecha + " " + hora);
//                ticket.TextoIzquierda("USUARIO: " + dt.Tables["Movimiento"].Rows[0]["Usuario"].ToString());
//                //ticket.TextoIzquierda(" ");
//                //ticket.lineasAsteriscos();


//                if (dt.Tables["Movimiento"].Rows.Count > 0)
//                {
//                    if (dt.Tables["Cliente"].Rows.Count > 0)
//                    {
//                        ticket.lineasGuio();
//                        ticket.TextoIzquierda("CLIENTE: " + dt.Tables["Cliente"].Rows[0]["Descripcion"].ToString());
//                        ticket.TextoIzquierda(dt.Tables["Cliente"].Rows[0]["TipoIdentidad"].ToString() + ": " + dt.Tables["Cliente"].Rows[0]["NroIdentidad"].ToString());
//                        ticket.TextoIzquierda("DIRECCION : " + dt.Tables["Cliente"].Rows[0]["Direccion"].ToString());
//                        //ticket.TextoIzquierda(" ");
//                        //ticket.lineasGuio();
//                    }
//                    /*
//                    switch ((TipoDocumento)Convert.ToInt16(dt.Tables["Movimiento"].Rows[0]["IdDocumento"]))
//                    {
//                        case TipoDocumento.BOLETA:
//                            //subcabecera

//                            if (dt.Tables["Cliente"].Rows.Count > 0)
//                            {
//                                ticket.lineasGuio();
//                                ticket.TextoIzquierda("CLIENTE: " + dt.Tables["Cliente"].Rows[0]["Descripcion"].ToString());
//                                ticket.TextoIzquierda(dt.Tables["Cliente"].Rows[0]["TipoIdentidad"].ToString() + ": " + dt.Tables["Cliente"].Rows[0]["NroIdentidad"].ToString());
//                                ticket.TextoIzquierda("DIRECCION : " + dt.Tables["Cliente"].Rows[0]["Direccion"].ToString());
//                                //ticket.TextoIzquierda(" ");
//                                //ticket.lineasGuio();
//                            }

//                            break;

//                        case TipoDocumento.FACTURA:
//                            ////subcabecera
//                            ticket.lineasGuio();
//                            ticket.TextoIzquierda("CLIENTE: " + dt.Tables["Cliente"].Rows[0]["Descripcion"].ToString());
//                            ticket.TextoIzquierda(dt.Tables["Cliente"].Rows[0]["TipoIdentidad"].ToString() + ": " + dt.Tables["Cliente"].Rows[0]["NroIdentidad"].ToString());
//                            ticket.TextoIzquierda("DIRECCION: " + dt.Tables["Cliente"].Rows[0]["Direccion"].ToString());
//                            //ticket.TextoIzquierda(" ");
//                            //ticket.lineasGuio(); 
//                            break;

//                        case TipoDocumento.TICKET:
//                            //subcabecera
//                            if (dt.Tables["Cliente"].Rows.Count > 0)
//                            {
//                                ticket.lineasGuio();
//                                ticket.TextoIzquierda("CLIENTE: " + dt.Tables["Cliente"].Rows[0]["Descripcion"].ToString());
//                                ticket.TextoIzquierda(dt.Tables["Cliente"].Rows[0]["TipoIdentidad"].ToString() + ": " + dt.Tables["Cliente"].Rows[0]["NroIdentidad"].ToString());
//                                ticket.TextoIzquierda("DIRECCION : " + dt.Tables["Cliente"].Rows[0]["Direccion"].ToString());
//                                //ticket.TextoIzquierda(" ");
//                                //ticket.lineasGuio();
//                            }

//                            break;

//                        case TipoDocumento.RECIBO:
//                            //subcabecera
//                            if (dt.Tables["Cliente"].Rows.Count > 0)
//                            {
//                                ticket.lineasGuio();
//                                ticket.TextoIzquierda("CLIENTE: " + dt.Tables["Cliente"].Rows[0]["Descripcion"].ToString());
//                                ticket.TextoIzquierda(dt.Tables["Cliente"].Rows[0]["TipoIdentidad"].ToString() + ": " + dt.Tables["Cliente"].Rows[0]["NroIdentidad"].ToString());
//                                ticket.TextoIzquierda("DIRECCION : " + dt.Tables["Cliente"].Rows[0]["Direccion"].ToString());
//                                //ticket.TextoIzquierda(" ");
//                                //ticket.lineasGuio();
//                            }

//                            break;

//                        default:

//                            break;
//                    }
//*/
//                }

//                //articulos a vender
//                ticket.EncabezadoVenta();

//                foreach (DataRow item in dt.Tables["MovimientoDetalle"].Rows)
//                {
//                    //ticket.AgregaArticulo(item["Producto"].ToString(), Convert.ToInt32(item["Cantidad"]), Convert.ToDecimal(item["Total"]));
//                    ticket.AgregaArticulo(Convert.ToInt32(item["Cantidad"]), item["Producto"].ToString(), Convert.ToDecimal(item["Precio"]), Convert.ToDecimal(item["Total"]));
//                }

//                ticket.lineasIgual();

//                //resumen de ventas
//                ticket.AgregarTotales("         DESCUENTO: S/.", Convert.ToDecimal(dt.Tables["Movimiento"].Rows[0]["Descuento"]));
//                ticket.AgregarTotales("         SUBTOTAL : S/.", Convert.ToDecimal(dt.Tables["Movimiento"].Rows[0]["SubTotal"]));
//                ticket.AgregarTotales("         IGV      : S/.", Convert.ToDecimal(dt.Tables["Movimiento"].Rows[0]["Igv"]));
//                ticket.AgregarTotales("         TOTAL    : S/.", Convert.ToDecimal(dt.Tables["Movimiento"].Rows[0]["Total"]));
//                ticket.TextoIzquierda(" ");

//                //texto final del ticket
//                //ticket.TextoIzquierda(" ");
//                if (Convert.ToBoolean(ticket.MostrarComentario))
//                {
//                    if (dt.Tables["Movimiento"].Rows[0]["Comentario"].ToString() != String.Empty)
//                    {
//                        ticket.TextoIzquierda("COMENTARIO: " + dt.Tables["Movimiento"].Rows[0]["Comentario"].ToString());
//                        ticket.TextoIzquierda(" ");
//                    }
//                }

//                ticket.TextoCentro("GRACIAS POR SU PREFERENCIA");
//                //ticket.TextoCentro("ESTE NO ES UN COMPROBANTE DE VENTA");
//                if (escredito == 1)
//                {
//                    ticket.TextoIzquierda(" ");
//                    ticket.TextoIzquierda("Pago Recibido: " + dt.Tables["Movimiento"].Rows[0]["PagoRecibido"].ToString());
//                    ticket.TextoIzquierda("Deuda: " + dt.Tables["Movimiento"].Rows[0]["PagoDeuda"].ToString());
//                }
//                else if (Convert.ToDecimal(dt.Tables["Movimiento"].Rows[0]["PagoVuelto"]) > 0)
//                {
//                    ticket.TextoIzquierda(" ");
//                    ticket.TextoIzquierda("Pago Recibido: " + dt.Tables["Movimiento"].Rows[0]["PagoRecibido"].ToString());
//                    ticket.TextoIzquierda("Vuelto: " + dt.Tables["Movimiento"].Rows[0]["PagoVuelto"].ToString());
//                }

//                if (activar == 1)
//                {
//                    ticket.TextoIzquierda(" ");
//                    ticket.TextoIzquierda("Puntos Ganados: " + dt.Tables["Cliente"].Rows[0]["PuntosVenta"].ToString());
//                    ticket.TextoIzquierda("Puntos Acumulados: " + dt.Tables["Cliente"].Rows[0]["Puntos"].ToString());
//                }

//                if (esfacturador == 1)
//                {
//                    //ticket.CortaTicket();
//                    ticket.ImprimirTicket(impresorapredeterminada);
//                    if (ticket.QRImage != null) ticket.ImprimirImagenTicket(impresorapredeterminada, 2);
//                    ticket.TextoCentro(dt.Tables["Movimiento"].Rows[0]["MensajeElectronica"].ToString());
//                    ticket.CortaTicketTotal(impresorapredeterminada);
//                }
//                else
//                {
//                    ticket.CortaTicket();
//                    ticket.ImprimirTicket(impresorapredeterminada);
//                    //ticket.ImprimirImagenTicket(impresorapredeterminada, 2);
//                    //ticket.CortaTicketTotal(impresorapredeterminada);
//                }
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//        }

        //anterior ticket
        //public static void GenerarTicket(clsMovimiento objentidad, bool ant)
        //{
        //    try
        //    {
        //        CrearTicket ticket = new CrearTicket();
        //        string fechatotal = DateTime.Now.ToLongDateString();
        //        string fecha = DateTime.Now.ToShortDateString();
        //        string hora = DateTime.Now.ToShortTimeString();
        //        PrintDocument pd = new PrintDocument();
        //        string impresorapredeterminada = pd.PrinterSettings.PrinterName;
        //        DataSet dt;
        //        dt = clsGestionarMovimiento.Instancia.TicketMovimiento(objentidad);

        //        ticket.AbreCajon();//para abrir el cajon del dinero
        //        //datos de la cabecera datos de la empresa
        //        ticket.TextoCentro(dt.Tables["Empresa"].Rows[0]["Descripcion"].ToString());
        //        ticket.TextoIzquierda(dt.Tables["Empresa"].Rows[0]["TipoIdentidad"].ToString() + ": " + dt.Tables["Empresa"].Rows[0]["NroIdentidad"].ToString());
        //        ticket.TextoIzquierda("DIREC: " + dt.Tables["Empresa"].Rows[0]["Direccion"].ToString());
        //        ticket.TextoIzquierda("TELEF: " + dt.Tables["Empresa"].Rows[0]["Telefono"].ToString());
        //        ticket.TextoIzquierda("EMAIL: " + dt.Tables["Empresa"].Rows[0]["Email"].ToString());
        //        //ticket.TextoIzquierda("EXPEDIDO EN: LOCAL PRINCIPAL");
        //        ticket.TextoIzquierda(" ");

        //        //numero de documento
        //        ticket.TextoCentro(dt.Tables["Movimiento"].Rows[0]["Documento"].ToString() + ": " + dt.Tables["Movimiento"].Rows[0]["NroDocumento"].ToString());
        //        //
        //        ticket.TextoIzquierda(" ");
        //        ticket.TextoIzquierda("F.EMISION: " + Convert.ToDateTime(dt.Tables["Movimiento"].Rows[0]["FechaMovimiento"]).ToShortDateString());
        //        ticket.TextoIzquierda("F.IMPRESION: " + fecha + " " + hora);
        //        //ticket.TextoIzquierda(" ");
        //        //ticket.lineasAsteriscos();
        //        ticket.lineasGuio();

        //        if (dt.Tables["Movimiento"].Rows.Count > 0)
        //        {
        //            switch ((TipoDocumento)Convert.ToInt16(dt.Tables["Movimiento"].Rows[0]["IdDocumento"]))
        //            {
        //                case TipoDocumento.BOLETA:
        //                    //subcabecera
        //                    //ticket.TextoIzquierda(" ");
        //                    if (dt.Tables["Cliente"].Rows.Count > 0)
        //                    {
        //                        ticket.TextoIzquierda("CLIENTE: " + dt.Tables["Cliente"].Rows[0]["Descripcion"].ToString());
        //                        ticket.TextoIzquierda(dt.Tables["Cliente"].Rows[0]["TipoIdentidad"].ToString() + ": " + dt.Tables["Cliente"].Rows[0]["NroIdentidad"].ToString());
        //                        ticket.TextoIzquierda("DIRECCION : " + dt.Tables["Cliente"].Rows[0]["Direccion"].ToString());
        //                        //ticket.TextoIzquierda(" ");
        //                        ticket.lineasGuio();
        //                    }

        //                    break;

        //                case TipoDocumento.FACTURA:
        //                    ////subcabecera
        //                    //ticket.TextoIzquierda(" ");
        //                    ticket.TextoIzquierda("CLIENTE: " + dt.Tables["Cliente"].Rows[0]["Descripcion"].ToString());
        //                    ticket.TextoIzquierda(dt.Tables["Cliente"].Rows[0]["TipoIdentidad"].ToString() + ": " + dt.Tables["Cliente"].Rows[0]["NroIdentidad"].ToString());
        //                    ticket.TextoIzquierda("DIRECCION : " + dt.Tables["Cliente"].Rows[0]["Direccion"].ToString());
        //                    //ticket.TextoIzquierda(" ");
        //                    ticket.lineasGuio(); ;
        //                    break;

        //                default:

        //                    break;
        //            }
        //        }

        //        //if (dt.Tables["Cliente"].Rows.Count > 0)
        //        //{
        //        //    //subcabecera
        //        //    ticket.TextoIzquierda(" ");
        //        //    ticket.TextoIzquierda("CLIENTE: " + dt.Tables["Cliente"].Rows[0]["Nombres"].ToString());
        //        //    ticket.TextoIzquierda(" ");
        //        //    ticket.TextoExtremos("FECHA : " + fecha, "HORA : " + hora);
        //        //    ticket.TextoIzquierda(" ");
        //        //    ticket.lineasAsteriscos();
        //        //}

        //        //articulos a vender
        //        ticket.EncabezadoVenta();

        //        foreach (DataRow item in dt.Tables["MovimientoDetalle"].Rows)
        //        {
        //            ticket.AgregaArticulo(item["Codigo"].ToString(), item["Producto"].ToString(), Convert.ToInt32(item["Cantidad"]), Convert.ToDecimal(item["Total"]));
        //        }

        //        ticket.lineasIgual();

        //        //resumen de ventas
        //        ticket.AgregarTotales("         DESCUENTO: S/. ", Convert.ToDecimal(dt.Tables["Movimiento"].Rows[0]["Descuento"]));
        //        ticket.AgregarTotales("         SUBTOTAL : S/. ", Convert.ToDecimal(dt.Tables["Movimiento"].Rows[0]["SubTotal"]));
        //        ticket.AgregarTotales("         IGV      : S/. ", Convert.ToDecimal(dt.Tables["Movimiento"].Rows[0]["Igv"]));
        //        ticket.AgregarTotales("         TOTAL    : S/. ", Convert.ToDecimal(dt.Tables["Movimiento"].Rows[0]["Total"]));
        //        ticket.TextoIzquierda(" ");

        //        //texto final del ticket
        //        ticket.TextoIzquierda(" ");
        //        ticket.TextoIzquierda(" ");
        //        ticket.TextoCentro("GRACIAS POR SU COMPRA");
        //        ticket.CortaTicket();
        //        ticket.ImprimirTicket(impresorapredeterminada);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public static void SoloNumerosDecimales(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        public static void SoloNumerosEnteros(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            //// only allow one decimal point
            //if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            //{
            //    e.Handled = true;
            //}
        }

        public static Keys Teclas(string tecla)
        {
            Keys key;
            switch (tecla)
            {
                case "Mostrar":
                    key = Keys.F1;
                    break;
                case "Nuevo":
                    key = Keys.F2;
                    break;
                case "Grabar":
                    key = Keys.F3;
                    break;
                case "Editar":
                    key = Keys.F4;
                    break;
                case "Cancelar":
                    key = Keys.F5;
                    break;
                case "Eliminar":
                    key = Keys.Delete;
                    break;
                case "Salir":
                    key = Keys.Escape;
                    break;
                case "Reporte":
                    key = Keys.F6;
                    break;
                case "Buscar":
                    key = Keys.F7;
                    break;
                case "Configuracion":
                    key = Keys.F8;
                    break;
                case "Abonos":
                    key = Keys.F9;
                    break;
                case "BuscarPersona":
                    key = Keys.F10;
                    break;
                case "Borrar":
                    key = Keys.F11;
                    break;
                case "Aplicar":
                    key = Keys.F8;
                    break;
                case "Seleccionar":
                    key = Keys.Enter;
                    break;
                case "ActivarBuscar":
                    key = Keys.Alt | Keys.A;
                    break;
                case "Similares":
                    key = Keys.Alt | Keys.S;
                    break;
                case "SelSimilares":
                    key = Keys.Alt | Keys.D;
                    break;

                case "Boleta":
                    key = Keys.Alt | Keys.B;
                    break;
                case "Factura":
                    key = Keys.Alt | Keys.F;
                    break;
                case "Ticket":
                    key = Keys.Alt | Keys.T;
                    break;
                default:
                    key = Keys.F1;
                    break;
            }
            return key;
        }

        //public static void GenerarTicketCaja(clsCaja objentidad)
        //{
        //    try
        //    {
        //        decimal vcon = 0, vcre = 0, ccon = 0, ccre = 0, sal = 0, ent = 0;
        //        DataSet dt;
        //        dt = clsGestionarMovimiento.Instancia.TicketCaja(objentidad);

        //        int n = 0; //obtiene el numero de columnas o guiones de ticket (58 mm->32 ; 80 mm ->46) tomar en cuenta que cada ticket tiene margen de 5 mm derecha e izquierda
        //        // (58 mm ->32)
        //        int ancho = 0, alto = 0;

        //        n = Convert.ToInt16(dt.Tables["Ticket"].Rows[0]["Columna"].ToString());
        //        ancho = Convert.ToInt16(dt.Tables["Ticket"].Rows[0]["Ancho"].ToString());
        //        alto = Convert.ToInt16(dt.Tables["Ticket"].Rows[0]["Alto"].ToString());

        //        CrearTicket ticket = new CrearTicket(n,ancho,alto);
        //        ticket.ComandoCorte = Regex.Unescape(dt.Tables["Ticket"].Rows[0]["ComandoCorte"].ToString());
        //        ticket.ComandoRenglon = Regex.Unescape(dt.Tables["Ticket"].Rows[0]["ComandoRenglon"].ToString());
        //        ticket.ComandoCajon = Regex.Unescape(dt.Tables["Ticket"].Rows[0]["ComandoCajon"].ToString());

        //        string fechatotal = DateTime.Now.ToLongDateString();
        //        string fecha = DateTime.Now.ToShortDateString();
        //        string hora = DateTime.Now.ToShortTimeString();
        //        PrintDocument pd = new PrintDocument();
        //        string impresorapredeterminada = pd.PrinterSettings.PrinterName;


        //        ticket.AbreCajon();//para abrir el cajon del dinero
        //        //datos de la cabecera datos de la empresa
        //        if (dt.Tables["Caja"].Rows[0]["NroCaja"] != String.Empty) ticket.TextoCentro("NROCAJA: " + dt.Tables["Caja"].Rows[0]["NroCaja"].ToString());
        //        if (dt.Tables["Caja"].Rows[0]["Usuario"] != String.Empty) ticket.TextoCentro("USUARIO: " + dt.Tables["Caja"].Rows[0]["Usuario"].ToString());
        //        if (dt.Tables["Caja"].Rows[0]["NombreUsuario"] != String.Empty) ticket.TextoIzquierda("NOMBRE: " + dt.Tables["Caja"].Rows[0]["NombreUsuario"].ToString());
        //        if (dt.Tables["Caja"].Rows[0]["FechaApertura"] != String.Empty) ticket.TextoIzquierda("F.APERTURA: " + Convert.ToDateTime(dt.Tables["Caja"].Rows[0]["FechaApertura"]).ToShortDateString() + " " + Convert.ToDateTime(dt.Tables["Caja"].Rows[0]["FechaApertura"]).ToShortTimeString());
        //        if (dt.Tables["Caja"].Rows[0]["FechaCierre"] != String.Empty) ticket.TextoIzquierda("F.CIERRE: " + Convert.ToDateTime(dt.Tables["Caja"].Rows[0]["FechaCierre"]).ToShortDateString() + " " + Convert.ToDateTime(dt.Tables["Caja"].Rows[0]["FechaCierre"]).ToShortTimeString());
        //        ticket.TextoIzquierda("F.IMPRESION: " + fecha + " " + hora);
        //        ticket.TextoIzquierda(" ");


        //        if (dt.Tables["VentasContado"].Rows.Count > 0)
        //        {
        //            ticket.lineasGuio();
        //            ticket.TextoIzquierda("VENTAS AL CONTADO");
        //            ticket.lineasGuio();

        //            foreach (DataRow item in dt.Tables["VentasContado"].Rows)
        //            {
        //                ticket.TextoExtremos(item["NroDocumento"].ToString(), item["Monto"].ToString());

        //                vcon = vcon + Convert.ToDecimal(item["Monto"]);
        //            }

        //            ticket.TextoExtremos("TOTAL", vcon.ToString());
        //        }

        //        if (dt.Tables["VentasCredito"].Rows.Count > 0)
        //        {
        //            ticket.lineasGuio();
        //            ticket.TextoIzquierda("VENTAS AL CREDITO");
        //            ticket.lineasGuio();

        //            foreach (DataRow item in dt.Tables["VentasCredito"].Rows)
        //            {
        //                ticket.TextoExtremos(item["NroDocumento"].ToString(), item["Monto"].ToString());

        //                vcre = vcre + Convert.ToDecimal(item["Monto"]);
        //            }

        //            ticket.TextoExtremos("TOTAL", vcre.ToString());
        //        }

        //        if (dt.Tables["ComprasContado"].Rows.Count > 0)
        //        {
        //            ticket.lineasGuio();
        //            ticket.TextoIzquierda("COMPRAS AL CONTADO");
        //            ticket.lineasGuio();

        //            foreach (DataRow item in dt.Tables["ComprasContado"].Rows)
        //            {
        //                ticket.TextoExtremos(item["NroDocumento"].ToString(), item["Monto"].ToString());

        //                ccon = ccon + Convert.ToDecimal(item["Monto"]);
        //            }

        //            ticket.TextoExtremos("TOTAL", ccon.ToString());
        //        }

        //        if (dt.Tables["ComprasCredito"].Rows.Count > 0)
        //        {
        //            ticket.lineasGuio();
        //            ticket.TextoIzquierda("COMPRAS AL CREDITO");
        //            ticket.lineasGuio();

        //            foreach (DataRow item in dt.Tables["ComprasCredito"].Rows)
        //            {
        //                ticket.TextoExtremos(item["NroDocumento"].ToString(), item["Monto"].ToString());

        //                ccre = ccre + Convert.ToDecimal(item["Monto"]);
        //            }

        //            ticket.TextoExtremos("TOTAL", ccre.ToString());
        //        }

        //        if (dt.Tables["Entradas"].Rows.Count > 0)
        //        {
        //            ticket.lineasGuio();
        //            ticket.TextoIzquierda("CAJA ENTRADAS");
        //            ticket.lineasGuio();

        //            foreach (DataRow item in dt.Tables["Entradas"].Rows)
        //            {
        //                ticket.TextoExtremos(item["Comentario"].ToString(), item["Monto"].ToString());

        //                ent = ent + Convert.ToDecimal(item["Monto"]);
        //            }

        //            ticket.TextoExtremos("TOTAL", ent.ToString());
        //        }

        //        if (dt.Tables["Salidas"].Rows.Count > 0)
        //        {
        //            ticket.lineasGuio();
        //            ticket.TextoIzquierda("CAJA SALIDAS");
        //            ticket.lineasGuio();

        //            foreach (DataRow item in dt.Tables["Salidas"].Rows)
        //            {
        //                ticket.TextoExtremos(item["Comentario"].ToString(), item["Monto"].ToString());

        //                sal = sal + Convert.ToDecimal(item["Monto"]);
        //            }

        //            ticket.TextoExtremos("TOTAL", sal.ToString());
        //        }
        //        ticket.TextoIzquierda(" ");
        //        ticket.lineasIgual();
        //        ticket.AgregarTotales("TOTAL ENTRADAS", Convert.ToDecimal(vcon + vcre + ent));
        //        ticket.AgregarTotales("TOTAL SALIDAS", Convert.ToDecimal(ccon + ccre + sal));
        //        ticket.AgregarTotales("TOTAL RECAUDADO", Convert.ToDecimal((vcon + vcre + ent) - (ccon + ccre + sal)));

        //        ticket.CortaTicket();
        //        ticket.ImprimirTicket(impresorapredeterminada);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}


        public static void ExportarExcel2007
            (DataTable informacion
    , String titulo
    , String rutaDestino
    , String nombreResponsable
    , Boolean mostrarCabecera
    , string headerColor)
        {

            // Validación agregada por la sobrecarga de metodos para la cabecera.
            Int32 _filaInicio = mostrarCabecera ? filaInicio : (Int32)0;

            // Establecer el nombre del responsable.
            nombreResponsable = string.IsNullOrEmpty(nombreResponsable) ? System.Environment.UserName : nombreResponsable;

            // Instanciar Objeto Excel.
            SpreadsheetGear.IWorkbook workbook = SpreadsheetGear.Factory.GetWorkbook();
            SpreadsheetGear.IWorksheet worksheet = workbook.Worksheets["Sheet1"];

            //Int32 filaInicio = 3;
            //Int32 columnaInicio = 0;

            Int32 totalColumnas = informacion.Columns.Count - 1;
            Int32 totalFilas = informacion.Rows.Count;

            worksheet.Name = informacion.TableName == String.Empty ? "archivo" : informacion.TableName;

            // Establecer Rango a partir del cual se copiará la información del Objeto DataTable.
            SpreadsheetGear.IRange rango = worksheet.Cells[_filaInicio, columnaInicio, _filaInicio, totalColumnas];

            // Copiar la información del DataTable.
            rango.CopyFromDataTable(informacion, SpreadsheetGear.Data.SetDataFlags.None);

            // Establecer formato del encabezado de la información.
            rango.HorizontalAlignment = SpreadsheetGear.HAlign.Center;
            rango.VerticalAlignment = SpreadsheetGear.VAlign.Center;
            rango.RowHeight = 19;
            //rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(200, 200, 100);
            //rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(149, 179, 215);

            switch (headerColor)
            {
                case "Black":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(13, 13, 13);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(255, 255, 255);
                    break;
                case "Blue":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(1, 89, 187);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(255, 255, 255);
                    break;
                case "Cyan":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(0, 176, 240);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(0, 0, 0);
                    break;
                case "DarkGray":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(109, 109, 109);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(255, 255, 255);
                    break;
                case "Gray":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(217, 217, 217);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(1, 1, 1);
                    break;
                case "Green":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(146, 208, 80);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(1, 1, 1);
                    break;
                case "Red":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(255, 0, 0);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(255, 255, 255);
                    break;
                case "Purple":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(96, 73, 122);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(255, 255, 255);
                    break;
                default:
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(149, 179, 215);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(1, 1, 1);
                    break;
            }

            rango.Columns.AutoFit();
            rango.Range.Font.Bold = true;

            // Establecer lineas de división simple.
            worksheet.Cells[_filaInicio, columnaInicio, _filaInicio + totalFilas, totalColumnas].Range.Borders.LineStyle = SpreadsheetGear.LineStyle.Continous;

            if (mostrarCabecera)
            {
                // Establecer Titulo.
                worksheet.Cells[0, columnaInicio].FormulaR1C1 = "Elaborado: " + DateTime.Now.ToString();
                worksheet.Cells[0, columnaInicio].Range.Font.Bold = true;
                worksheet.Cells[0, columnaInicio, 0, columnaInicio + 3].Range.Merge();

                worksheet.Cells[1, columnaInicio].FormulaR1C1 = "Responsable: " + nombreResponsable;
                worksheet.Cells[1, columnaInicio].Range.Font.Bold = true;
                worksheet.Cells[1, columnaInicio, 1, columnaInicio + 2].Range.Merge();

                worksheet.Cells[2, columnaInicio].FormulaR1C1 = titulo;
                worksheet.Cells[2, columnaInicio, 2, totalColumnas].Range.Merge();
                worksheet.Cells[2, columnaInicio, 2, totalColumnas].HorizontalAlignment = SpreadsheetGear.HAlign.Center;
                worksheet.Cells[2, columnaInicio, 2, totalColumnas].Range.Font.Bold = true;
                worksheet.Cells[2, columnaInicio, 2, totalColumnas].Range.Font.Size = 14;
            }

            // Ajustar el tamaño de cada columna.
            worksheet.UsedRange.Columns.AutoFit();

            // Guardar Información.
            workbook.SaveAs(rutaDestino, SpreadsheetGear.FileFormat.OpenXMLWorkbook);

        }

        public static void ExportarExcel2007(DataTable informacion, clsReporte objentidad)
        {
            // Validación agregada por la sobrecarga de metodos para la cabecera.
            Int32 _filaInicio = objentidad.MostrarCabecera ? filaInicio + 1 : (Int32)0;

            // Establecer el nombre del responsable.
            objentidad.NombreUsuario = string.IsNullOrEmpty(objentidad.NombreUsuario) ? System.Environment.UserName : objentidad.NombreUsuario;

            // Instanciar Objeto Excel.
            SpreadsheetGear.IWorkbook workbook = SpreadsheetGear.Factory.GetWorkbook();
            SpreadsheetGear.IWorksheet worksheet = workbook.Worksheets["Sheet1"];

            //Int32 filaInicio = 3;
            //Int32 columnaInicio = 0;

            Int32 totalColumnas = informacion.Columns.Count - 1;
            Int32 totalFilas = informacion.Rows.Count;

            worksheet.Name = informacion.TableName == String.Empty ? "archivo" : informacion.TableName;

            // Establecer Rango a partir del cual se copiará la información del Objeto DataTable.
            SpreadsheetGear.IRange rango = worksheet.Cells[_filaInicio, columnaInicio, _filaInicio, totalColumnas];

            // Copiar la información del DataTable.
            rango.CopyFromDataTable(informacion, SpreadsheetGear.Data.SetDataFlags.None);

            // Establecer formato del encabezado de la información.
            rango.HorizontalAlignment = SpreadsheetGear.HAlign.Center;
            rango.VerticalAlignment = SpreadsheetGear.VAlign.Center;
            rango.RowHeight = 19;
            //rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(200, 200, 100);
            //rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(149, 179, 215);

            switch (objentidad.HeaderColor)
            {
                case "Black":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(13, 13, 13);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(255, 255, 255);
                    break;
                case "Blue":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(1, 89, 187);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(255, 255, 255);
                    break;
                case "Cyan":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(0, 176, 240);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(0, 0, 0);
                    break;
                case "DarkGray":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(109, 109, 109);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(255, 255, 255);
                    break;
                case "Gray":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(217, 217, 217);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(1, 1, 1);
                    break;
                case "Green":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(146, 208, 80);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(1, 1, 1);
                    break;
                case "Red":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(255, 0, 0);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(255, 255, 255);
                    break;
                case "Purple":
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(96, 73, 122);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(255, 255, 255);
                    break;
                default:
                    rango.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(149, 179, 215);
                    rango.Font.Color = SpreadsheetGear.Drawing.Color.FromArgb(1, 1, 1);
                    break;
            }

            rango.Columns.AutoFit();
            rango.Range.Font.Bold = true;

            // Establecer lineas de división simple.
            worksheet.Cells[_filaInicio, columnaInicio, _filaInicio + totalFilas, totalColumnas].Range.Borders.LineStyle = SpreadsheetGear.LineStyle.Continous;

            if (objentidad.MostrarCabecera)
            {
                // Establecer Titulo.
                worksheet.Cells[0, columnaInicio].FormulaR1C1 = "Elaborado: " + DateTime.Now.ToString();
                worksheet.Cells[0, columnaInicio].Range.Font.Bold = true;
                worksheet.Cells[0, columnaInicio, 0, columnaInicio + 3].Range.Merge();

                worksheet.Cells[1, columnaInicio].FormulaR1C1 = "Responsable: " + objentidad.NombreUsuario;
                worksheet.Cells[1, columnaInicio].Range.Font.Bold = true;
                worksheet.Cells[1, columnaInicio, 1, columnaInicio + 2].Range.Merge();

                worksheet.Cells[2, columnaInicio].FormulaR1C1 = "Desde: " + objentidad.FechaDesde.Value.ToShortDateString() + " Hasta: " + objentidad.FechaHasta.Value.ToShortDateString();
                worksheet.Cells[2, columnaInicio].Range.Font.Bold = true;
                worksheet.Cells[2, columnaInicio, 2, columnaInicio + 2].Range.Merge();

                worksheet.Cells[3, columnaInicio].FormulaR1C1 = objentidad.Descripcion;
                worksheet.Cells[3, columnaInicio, 3, totalColumnas].Range.Merge();
                worksheet.Cells[3, columnaInicio, 3, totalColumnas].HorizontalAlignment = SpreadsheetGear.HAlign.Center;
                worksheet.Cells[3, columnaInicio, 3, totalColumnas].Range.Font.Bold = true;
                worksheet.Cells[3, columnaInicio, 3, totalColumnas].Range.Font.Size = 14;
            }

            // Ajustar el tamaño de cada columna.
            worksheet.UsedRange.Columns.AutoFit();

            // Guardar Información.
            workbook.SaveAs(objentidad.RutaDestino, SpreadsheetGear.FileFormat.OpenXMLWorkbook);

        }

        public static T Clonar<T>(T fuente)
        {
            //Verificamos que sea serializable antes de hacer la copia
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("El tipo de dato debe ser serializable.", "fuente");
            }
            if (Object.ReferenceEquals(fuente, null))
            {
                return default(T);
            }
            //Creamos un stream en memoria
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();

            using (stream)
            {
                formatter.Serialize(stream, fuente);
                stream.Seek(0, SeekOrigin.Begin);
                //Deserializamos la porcón de memoria en el nuevo objeto
                return (T)formatter.Deserialize(stream);
            }
        }

        public static byte[] ImageToBytes(Image imagen)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                imagen.Save(ms, ImageFormat.Jpeg);
                byte[] bytesImg = ms.ToArray();

                return bytesImg;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al convertir a bytes: " + ex.Message);
            }
        }

        public static Image BytesToImage(byte[] bytesImg)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                ms.Write(bytesImg, 0, bytesImg.Length);
                Image imagen = Image.FromStream(ms);

                return imagen;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al convertir en imágen: " + ex.Message);
            }
        }

        public static Image ResizeImage(Image image, int width, int height)
        {
            try
            {
                Bitmap imgBmp = new Bitmap(width, height);
                Graphics graph = Graphics.FromImage((Image)imgBmp);

                //Setear valores
                graph.SmoothingMode = SmoothingMode.AntiAlias;
                graph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graph.PixelOffsetMode = PixelOffsetMode.HighQuality;

                graph.DrawImage(image, 0, 0, width, height);
                graph.Dispose();

                return (Image)imgBmp;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al redimensionar la imágen: " + ex.Message);
            }
        }

        public static clsUsuario Usuario;

        public clsMovimiento TotalTipoAfectacion(clsListaMovimientoDetalle objlista, decimal iva)
        {
            try
            {
                clsMovimiento objmovimiento = new clsMovimiento();
                Int16 tipooperacion = 0;

                DataTable data = new DataTable();
                Hashtable parametros = new Hashtable();
                data = ObtenerAyudaLista("TipoAfectacion", "AyudaLista", parametros);


                foreach (clsMovimientoDetalle item in objlista.Elementos)
                {
                    Int16 idtipoafectacion = item.IdTipoAfectacion.Value;

                    foreach (DataRow i in data.Rows)
                    {
                        if (idtipoafectacion == Convert.ToInt16(i.ItemArray[0]))
                        {
                            tipooperacion = Convert.ToInt16(i.ItemArray[2]);
                        }
                    }

                    switch ((TipoOperacion)tipooperacion)
                    {
                        case TipoOperacion.GRAVADO:
                            objmovimiento.Gravado = objmovimiento.Gravado + item.Total.Value;
                            break;
                        case TipoOperacion.EXONERADO:
                            objmovimiento.Exonerado = objmovimiento.Exonerado + item.Total.Value;
                            break;
                        case TipoOperacion.INAFECTO:
                            objmovimiento.Inafecto = objmovimiento.Inafecto + item.Total.Value;
                            break;
                        case TipoOperacion.EXPORTACION:
                            objmovimiento.Exportacion = objmovimiento.Exportacion + item.Total.Value;
                            break;
                        case TipoOperacion.GRATUITO:
                            objmovimiento.Gratuito = objmovimiento.Gratuito + item.Total.Value;
                            break;
                        default:
                            break;
                    }

                }

                decimal baseimponible = 0;

                baseimponible = objmovimiento.Gravado / (decimal)(1 + iva);
                objmovimiento.Igv = Math.Round(baseimponible * (decimal)iva, 2);
                objmovimiento.Gravado = objmovimiento.Gravado - objmovimiento.Igv.Value;


                //objmovimiento.Gravado = objmovimiento.Gravado / (decimal)(1 + iva);               
                //objmovimiento.Igv = objmovimiento.Gravado * (decimal)iva;
                objmovimiento.Total = objmovimiento.Gravado + objmovimiento.Exonerado + objmovimiento.Inafecto + objmovimiento.Exportacion + objmovimiento.Gratuito + objmovimiento.Igv;
                objmovimiento.SubTotal = objmovimiento.Total - objmovimiento.Igv;

                return objmovimiento;

            }
            catch (Exception ex)
            {
                throw new Exception("No existe: " + ex.Message);
            }
        }

    }
}
