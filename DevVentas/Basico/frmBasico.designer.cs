﻿namespace Capa_Cliente.Basico
{
    partial class frmBasico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBasico));
            this.lbl_titulo = new DevComponents.DotNetBar.LabelX();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.btn_actualizar = new DevComponents.DotNetBar.ButtonItem();
            this.btn_nuevo = new DevComponents.DotNetBar.ButtonItem();
            this.btn_grabar = new DevComponents.DotNetBar.ButtonItem();
            this.btn_editar = new DevComponents.DotNetBar.ButtonItem();
            this.btn_cancelar = new DevComponents.DotNetBar.ButtonItem();
            this.btn_eliminar = new DevComponents.DotNetBar.ButtonItem();
            this.btn_reporte = new DevComponents.DotNetBar.ButtonItem();
            this.btn_salir = new DevComponents.DotNetBar.ButtonItem();
            this.superTooltip1 = new DevComponents.DotNetBar.SuperTooltip();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tab_padre = new DevComponents.DotNetBar.TabControl();
            this.tab_mostrar1 = new DevComponents.DotNetBar.TabControlPanel();
            this.tab_mostrar = new DevComponents.DotNetBar.TabItem(this.components);
            this.tab_registro1 = new DevComponents.DotNetBar.TabControlPanel();
            this.tab_registro = new DevComponents.DotNetBar.TabItem(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tab_padre)).BeginInit();
            this.tab_padre.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_titulo
            // 
            this.lbl_titulo.BackColor = System.Drawing.SystemColors.ActiveCaption;
            // 
            // 
            // 
            this.lbl_titulo.BackgroundStyle.Class = "";
            this.lbl_titulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_titulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_titulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_titulo.Location = new System.Drawing.Point(0, 0);
            this.lbl_titulo.Name = "lbl_titulo";
            this.lbl_titulo.Size = new System.Drawing.Size(737, 35);
            this.lbl_titulo.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.lbl_titulo.TabIndex = 6;
            this.lbl_titulo.Text = "Titulo";
            this.lbl_titulo.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "bar1 (bar1)";
            this.bar1.AccessibleName = "bar1";
            this.bar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_actualizar,
            this.btn_nuevo,
            this.btn_grabar,
            this.btn_editar,
            this.btn_cancelar,
            this.btn_eliminar,
            this.btn_reporte,
            this.btn_salir});
            this.bar1.Location = new System.Drawing.Point(0, 35);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(737, 33);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.bar1.TabIndex = 9;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // btn_actualizar
            // 
            this.btn_actualizar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btn_actualizar.Image = ((System.Drawing.Image)(resources.GetObject("btn_actualizar.Image")));
            this.btn_actualizar.Name = "btn_actualizar";
            this.btn_actualizar.Text = "Mostrar(F1)";
            // 
            // btn_nuevo
            // 
            this.btn_nuevo.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btn_nuevo.Image = ((System.Drawing.Image)(resources.GetObject("btn_nuevo.Image")));
            this.btn_nuevo.ImagePaddingHorizontal = 7;
            this.btn_nuevo.Name = "btn_nuevo";
            this.btn_nuevo.Text = "Nuevo(F2)";
            this.btn_nuevo.Click += new System.EventHandler(this.btn_nuevo_Click);
            // 
            // btn_grabar
            // 
            this.btn_grabar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btn_grabar.Image = ((System.Drawing.Image)(resources.GetObject("btn_grabar.Image")));
            this.btn_grabar.Name = "btn_grabar";
            this.btn_grabar.Text = "Grabar(F3)";
            // 
            // btn_editar
            // 
            this.btn_editar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btn_editar.Image = ((System.Drawing.Image)(resources.GetObject("btn_editar.Image")));
            this.btn_editar.Name = "btn_editar";
            this.btn_editar.Text = "Editar(F4)";
            this.btn_editar.Click += new System.EventHandler(this.btn_editar_Click);
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btn_cancelar.Image = ((System.Drawing.Image)(resources.GetObject("btn_cancelar.Image")));
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Text = "Cancelar(F5)";
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // btn_eliminar
            // 
            this.btn_eliminar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btn_eliminar.Image = ((System.Drawing.Image)(resources.GetObject("btn_eliminar.Image")));
            this.btn_eliminar.Name = "btn_eliminar";
            this.btn_eliminar.Text = "Eliminar(Del)";
            // 
            // btn_reporte
            // 
            this.btn_reporte.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btn_reporte.Image = ((System.Drawing.Image)(resources.GetObject("btn_reporte.Image")));
            this.btn_reporte.Name = "btn_reporte";
            this.btn_reporte.Text = "Reporte(F6)";
            // 
            // btn_salir
            // 
            this.btn_salir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btn_salir.Image = ((System.Drawing.Image)(resources.GetObject("btn_salir.Image")));
            this.btn_salir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Far;
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Text = "Salir(Esc)";
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // tab_padre
            // 
            this.tab_padre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.tab_padre.CanReorderTabs = true;
            this.tab_padre.Controls.Add(this.tab_mostrar1);
            this.tab_padre.Controls.Add(this.tab_registro1);
            this.tab_padre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab_padre.Location = new System.Drawing.Point(0, 35);
            this.tab_padre.Name = "tab_padre";
            this.tab_padre.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tab_padre.SelectedTabIndex = 0;
            this.tab_padre.Size = new System.Drawing.Size(737, 363);
            this.tab_padre.TabIndex = 10;
            this.tab_padre.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tab_padre.Tabs.Add(this.tab_mostrar);
            this.tab_padre.Tabs.Add(this.tab_registro);
            this.tab_padre.Text = "tab_padre";
            // 
            // tab_mostrar1
            // 
            this.tab_mostrar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab_mostrar1.Location = new System.Drawing.Point(0, 26);
            this.tab_mostrar1.Name = "tab_mostrar1";
            this.tab_mostrar1.Padding = new System.Windows.Forms.Padding(1);
            this.tab_mostrar1.Size = new System.Drawing.Size(737, 337);
            this.tab_mostrar1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(231)))));
            this.tab_mostrar1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.tab_mostrar1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tab_mostrar1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tab_mostrar1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tab_mostrar1.Style.GradientAngle = 90;
            this.tab_mostrar1.TabIndex = 1;
            this.tab_mostrar1.TabItem = this.tab_mostrar;
            // 
            // tab_mostrar
            // 
            this.tab_mostrar.AttachedControl = this.tab_mostrar1;
            this.tab_mostrar.Name = "tab_mostrar";
            this.tab_mostrar.Text = "tabItem1";
            // 
            // tab_registro1
            // 
            this.tab_registro1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab_registro1.Location = new System.Drawing.Point(0, 26);
            this.tab_registro1.Name = "tab_registro1";
            this.tab_registro1.Padding = new System.Windows.Forms.Padding(1);
            this.tab_registro1.Size = new System.Drawing.Size(737, 337);
            this.tab_registro1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(231)))));
            this.tab_registro1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.tab_registro1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tab_registro1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tab_registro1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tab_registro1.Style.GradientAngle = 90;
            this.tab_registro1.TabIndex = 2;
            this.tab_registro1.TabItem = this.tab_registro;
            // 
            // tab_registro
            // 
            this.tab_registro.AttachedControl = this.tab_registro1;
            this.tab_registro.Name = "tab_registro";
            this.tab_registro.Text = "tabItem2";
            // 
            // frmBasico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 398);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.tab_padre);
            this.Controls.Add(this.lbl_titulo);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.Name = "frmBasico";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmBasico";
            this.Load += new System.EventHandler(this.frmBasico_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tab_padre)).EndInit();
            this.tab_padre.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public DevComponents.DotNetBar.LabelX lbl_titulo;
        public DevComponents.DotNetBar.Bar bar1;
        public DevComponents.DotNetBar.ButtonItem btn_actualizar;
        private DevComponents.DotNetBar.SuperTooltip superTooltip1;
        private System.Windows.Forms.Timer timer1;
        public DevComponents.DotNetBar.ButtonItem btn_nuevo;
        public DevComponents.DotNetBar.ButtonItem btn_grabar;
        public DevComponents.DotNetBar.ButtonItem btn_editar;
        public DevComponents.DotNetBar.ButtonItem btn_cancelar;
        public DevComponents.DotNetBar.ButtonItem btn_eliminar;
        public DevComponents.DotNetBar.ButtonItem btn_salir;
        public DevComponents.DotNetBar.TabControl tab_padre;
        public DevComponents.DotNetBar.TabControlPanel tab_registro1;
        public DevComponents.DotNetBar.TabControlPanel tab_mostrar1;
        public DevComponents.DotNetBar.TabItem tab_registro;
        public DevComponents.DotNetBar.TabItem tab_mostrar;
        public DevComponents.DotNetBar.ButtonItem btn_reporte;
    }
}