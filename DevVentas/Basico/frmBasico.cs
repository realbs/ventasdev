﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Cliente.Basico
{
    public partial class frmBasico : DevComponents.DotNetBar.Office2007Form
    {       

        public frmBasico()
        {
            InitializeComponent();
        }

        public void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }        

        public void btn_nuevo_Click(object sender, EventArgs e)
        {
            this.tab_padre.SelectedTab = tab_registro;
            ActivarBotones(true);
        }       

        public void btn_cancelar_Click(object sender, EventArgs e)
        {
            ActivarBotones(false);
            tab_padre.SelectedTab = tab_mostrar;            
        }

        private void ActivarBotones(Boolean b)
        {
            this.btn_grabar.Enabled = !b;
            this.btn_cancelar.Enabled = b;
            this.btn_actualizar.Enabled = !b;
            this.btn_editar.Enabled = !b;
            this.btn_nuevo.Enabled = !b;
            this.btn_eliminar.Enabled = !b;
            this.btn_reporte.Enabled = !b;

        }       

        private void frmBasico_Load(object sender, EventArgs e)
        {
            ActivarBotones(false);
            tab_padre.SelectedTab = tab_mostrar;
        }       

        public void btn_editar_Click(object sender, EventArgs e)
        {
            ActivarBotones(true);
        }

        //frmMensaje frmmensaje = null;

        public virtual void Mensaje(String mensaje, Form frmpadre)
        {
            //frmmensaje = new frmMensaje(mensaje);
            //frmmensaje.Owner = frmpadre;
            //frmmensaje.Show();
        }

        public virtual void FinMensaje()
        {
            //if (frmmensaje != null)
            //{
            //    frmmensaje.Close();
            //    frmmensaje = null;
            //}
        }
    }
}
