﻿namespace Capa_Cliente
{
    partial class frmGuia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGuia));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.chk_condeuda = new System.Windows.Forms.CheckBox();
            this.btn_borrarfiltro = new DevComponents.DotNetBar.ButtonX();
            this.lbl_personafiltro = new DevComponents.DotNetBar.LabelX();
            this.chk_escredito = new System.Windows.Forms.CheckBox();
            this.txt_filtrarpersona = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btn_persona = new DevComponents.DotNetBar.ButtonX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.uc_empresasedefiltro = new Capa_Cliente.Controles.uc_empresasede();
            this.cbo_usuariofiltro = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.chk_nromovimiento = new System.Windows.Forms.CheckBox();
            this.txt_nrodocumento = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.dt_fechahasta = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.cbo_tipomovimientobuscar = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.dt_fechadesde = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.uc_grilla = new Capa_Cliente.Controles.uc_grilla();
            this.grupo_movimiento = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_nrocaja = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_puntoatencion = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.uc_empresasede = new Capa_Cliente.Controles.uc_empresasede();
            this.cbo_tipomovimiento = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.txt_serie = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.cbo_documento = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txt_numero = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btn_buscarpersona = new DevComponents.DotNetBar.ButtonX();
            this.btn_borrar = new DevComponents.DotNetBar.ButtonX();
            this.txt_persona = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lbl_persona = new DevComponents.DotNetBar.LabelX();
            this.txt_comentario = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.dt_fechaemision = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.cbo_distritop = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.cbo_provinciap = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.cbo_departamentop = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txt_direccionp = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.cbo_distritoll = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            this.cbo_provinciall = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.cbo_departamentoll = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txt_direccionll = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.grupo_producto = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lbl_codigo = new DevComponents.DotNetBar.LabelX();
            this.txt_codigoproducto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btn_buscar = new DevComponents.DotNetBar.ButtonX();
            this.dgv_mostrar = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.col_idproducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_marca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_unidadmedida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_preciocosto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_precioventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_espvv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_descuento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_stock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_preciomayor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_pm = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.col_idtipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_precioventa1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_precioventa2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_precioventa3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_precioventa4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_cantidadmayor1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_cantidadmayor2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_cantidadmayor3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_cantidadmayor4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_espaquete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_esserielote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_fechatraslado = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.cbo_modalidadtraslado = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX22 = new DevComponents.DotNetBar.LabelX();
            this.cbo_motivotraslado = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.textBoxX2 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX23 = new DevComponents.DotNetBar.LabelX();
            this.textBoxX3 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.labelX25 = new DevComponents.DotNetBar.LabelX();
            this.textBoxX4 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX26 = new DevComponents.DotNetBar.LabelX();
            this.textBoxX5 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX27 = new DevComponents.DotNetBar.LabelX();
            this.textBoxX6 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX28 = new DevComponents.DotNetBar.LabelX();
            this.textBoxX7 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.textBoxX8 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.labelX29 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.groupPanel5 = new DevComponents.DotNetBar.Controls.GroupPanel();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tab_padre)).BeginInit();
            this.tab_padre.SuspendLayout();
            this.tab_registro1.SuspendLayout();
            this.tab_mostrar1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dt_fechahasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_fechadesde)).BeginInit();
            this.grupo_movimiento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dt_fechaemision)).BeginInit();
            this.groupPanel1.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.grupo_producto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_mostrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_fechatraslado)).BeginInit();
            this.groupPanel4.SuspendLayout();
            this.groupPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_titulo
            // 
            // 
            // 
            // 
            this.lbl_titulo.BackgroundStyle.Class = "";
            this.lbl_titulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_titulo.Size = new System.Drawing.Size(1216, 35);
            // 
            // bar1
            // 
            this.bar1.Size = new System.Drawing.Size(1216, 33);
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Enabled = false;
            // 
            // tab_padre
            // 
            this.tab_padre.SelectedTabIndex = 1;
            this.tab_padre.Size = new System.Drawing.Size(1216, 629);
            this.tab_padre.Controls.SetChildIndex(this.tab_mostrar1, 0);
            this.tab_padre.Controls.SetChildIndex(this.tab_registro1, 0);
            // 
            // tab_registro1
            // 
            this.tab_registro1.Controls.Add(this.groupPanel5);
            this.tab_registro1.Controls.Add(this.groupPanel4);
            this.tab_registro1.Controls.Add(this.dgv_mostrar);
            this.tab_registro1.Controls.Add(this.grupo_producto);
            this.tab_registro1.Controls.Add(this.groupPanel3);
            this.tab_registro1.Controls.Add(this.groupPanel1);
            this.tab_registro1.Controls.Add(this.grupo_movimiento);
            this.tab_registro1.Size = new System.Drawing.Size(1216, 603);
            this.tab_registro1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(231)))));
            this.tab_registro1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.tab_registro1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tab_registro1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tab_registro1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tab_registro1.Style.GradientAngle = 90;
            // 
            // tab_mostrar1
            // 
            this.tab_mostrar1.Controls.Add(this.uc_grilla);
            this.tab_mostrar1.Controls.Add(this.groupPanel2);
            this.tab_mostrar1.Size = new System.Drawing.Size(1216, 603);
            this.tab_mostrar1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(231)))));
            this.tab_mostrar1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.tab_mostrar1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tab_mostrar1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tab_mostrar1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tab_mostrar1.Style.GradientAngle = 90;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.groupPanel2.Controls.Add(this.chk_condeuda);
            this.groupPanel2.Controls.Add(this.btn_borrarfiltro);
            this.groupPanel2.Controls.Add(this.lbl_personafiltro);
            this.groupPanel2.Controls.Add(this.chk_escredito);
            this.groupPanel2.Controls.Add(this.txt_filtrarpersona);
            this.groupPanel2.Controls.Add(this.btn_persona);
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.Controls.Add(this.uc_empresasedefiltro);
            this.groupPanel2.Controls.Add(this.cbo_usuariofiltro);
            this.groupPanel2.Controls.Add(this.chk_nromovimiento);
            this.groupPanel2.Controls.Add(this.txt_nrodocumento);
            this.groupPanel2.Controls.Add(this.dt_fechahasta);
            this.groupPanel2.Controls.Add(this.labelX8);
            this.groupPanel2.Controls.Add(this.cbo_tipomovimientobuscar);
            this.groupPanel2.Controls.Add(this.labelX9);
            this.groupPanel2.Controls.Add(this.dt_fechadesde);
            this.groupPanel2.Controls.Add(this.labelX11);
            this.groupPanel2.Location = new System.Drawing.Point(12, 13);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(1059, 80);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.Class = "";
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.Class = "";
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.Class = "";
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 41;
            this.groupPanel2.Text = "BUSCAR MOVIMIENTOS";
            // 
            // chk_condeuda
            // 
            this.chk_condeuda.AutoSize = true;
            this.chk_condeuda.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_condeuda.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chk_condeuda.Location = new System.Drawing.Point(952, 0);
            this.chk_condeuda.Name = "chk_condeuda";
            this.chk_condeuda.Size = new System.Drawing.Size(98, 21);
            this.chk_condeuda.TabIndex = 72;
            this.chk_condeuda.Text = "Con Deuda";
            this.chk_condeuda.UseVisualStyleBackColor = true;
            // 
            // btn_borrarfiltro
            // 
            this.btn_borrarfiltro.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_borrarfiltro.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_borrarfiltro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_borrarfiltro.Image = ((System.Drawing.Image)(resources.GetObject("btn_borrarfiltro.Image")));
            this.btn_borrarfiltro.Location = new System.Drawing.Point(773, 2);
            this.btn_borrarfiltro.Name = "btn_borrarfiltro";
            this.btn_borrarfiltro.Size = new System.Drawing.Size(65, 23);
            this.btn_borrarfiltro.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_borrarfiltro.TabIndex = 71;
            // 
            // lbl_personafiltro
            // 
            this.lbl_personafiltro.AutoSize = true;
            // 
            // 
            // 
            this.lbl_personafiltro.BackgroundStyle.Class = "";
            this.lbl_personafiltro.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_personafiltro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_personafiltro.Location = new System.Drawing.Point(611, 7);
            this.lbl_personafiltro.Name = "lbl_personafiltro";
            this.lbl_personafiltro.Size = new System.Drawing.Size(64, 18);
            this.lbl_personafiltro.TabIndex = 70;
            this.lbl_personafiltro.Text = "Proveedor";
            // 
            // chk_escredito
            // 
            this.chk_escredito.AutoSize = true;
            this.chk_escredito.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_escredito.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chk_escredito.Location = new System.Drawing.Point(853, 0);
            this.chk_escredito.Name = "chk_escredito";
            this.chk_escredito.Size = new System.Drawing.Size(92, 21);
            this.chk_escredito.TabIndex = 69;
            this.chk_escredito.Text = "Es Crédito";
            this.chk_escredito.UseVisualStyleBackColor = true;
            // 
            // txt_filtrarpersona
            // 
            // 
            // 
            // 
            this.txt_filtrarpersona.Border.Class = "TextBoxBorder";
            this.txt_filtrarpersona.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_filtrarpersona.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_filtrarpersona.Location = new System.Drawing.Point(611, 29);
            this.txt_filtrarpersona.Name = "txt_filtrarpersona";
            this.txt_filtrarpersona.ReadOnly = true;
            this.txt_filtrarpersona.Size = new System.Drawing.Size(227, 23);
            this.txt_filtrarpersona.TabIndex = 67;
            // 
            // btn_persona
            // 
            this.btn_persona.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_persona.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_persona.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_persona.Image = ((System.Drawing.Image)(resources.GetObject("btn_persona.Image")));
            this.btn_persona.Location = new System.Drawing.Point(692, 2);
            this.btn_persona.Name = "btn_persona";
            this.btn_persona.Size = new System.Drawing.Size(65, 23);
            this.btn_persona.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_persona.TabIndex = 66;
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.Class = "";
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.Location = new System.Drawing.Point(853, 31);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(49, 18);
            this.labelX2.TabIndex = 65;
            this.labelX2.Text = "Usuario";
            // 
            // uc_empresasedefiltro
            // 
            this.uc_empresasedefiltro.BackColor = System.Drawing.Color.Transparent;
            this.uc_empresasedefiltro.Location = new System.Drawing.Point(3, 0);
            this.uc_empresasedefiltro.Name = "uc_empresasedefiltro";
            this.uc_empresasedefiltro.PosicionControl = Capa_Entidad.PosicionControl.EnLinea;
            this.uc_empresasedefiltro.Size = new System.Drawing.Size(241, 54);
            this.uc_empresasedefiltro.TabIndex = 65;
            // 
            // cbo_usuariofiltro
            // 
            this.cbo_usuariofiltro.DisplayMember = "Text";
            this.cbo_usuariofiltro.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_usuariofiltro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_usuariofiltro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_usuariofiltro.FormattingEnabled = true;
            this.cbo_usuariofiltro.ItemHeight = 14;
            this.cbo_usuariofiltro.Location = new System.Drawing.Point(908, 29);
            this.cbo_usuariofiltro.Name = "cbo_usuariofiltro";
            this.cbo_usuariofiltro.Size = new System.Drawing.Size(136, 20);
            this.cbo_usuariofiltro.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_usuariofiltro.TabIndex = 64;
            // 
            // chk_nromovimiento
            // 
            this.chk_nromovimiento.AutoSize = true;
            this.chk_nromovimiento.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chk_nromovimiento.Location = new System.Drawing.Point(250, 33);
            this.chk_nromovimiento.Name = "chk_nromovimiento";
            this.chk_nromovimiento.Size = new System.Drawing.Size(98, 17);
            this.chk_nromovimiento.TabIndex = 64;
            this.chk_nromovimiento.Text = "NroDocumento";
            this.chk_nromovimiento.UseVisualStyleBackColor = true;
            // 
            // txt_nrodocumento
            // 
            // 
            // 
            // 
            this.txt_nrodocumento.Border.Class = "TextBoxBorder";
            this.txt_nrodocumento.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_nrodocumento.Location = new System.Drawing.Point(352, 31);
            this.txt_nrodocumento.Name = "txt_nrodocumento";
            this.txt_nrodocumento.ReadOnly = true;
            this.txt_nrodocumento.Size = new System.Drawing.Size(101, 20);
            this.txt_nrodocumento.TabIndex = 62;
            // 
            // dt_fechahasta
            // 
            // 
            // 
            // 
            this.dt_fechahasta.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dt_fechahasta.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechahasta.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dt_fechahasta.ButtonDropDown.Visible = true;
            this.dt_fechahasta.Location = new System.Drawing.Point(506, 32);
            // 
            // 
            // 
            this.dt_fechahasta.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dt_fechahasta.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dt_fechahasta.MonthCalendar.BackgroundStyle.Class = "";
            this.dt_fechahasta.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechahasta.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dt_fechahasta.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dt_fechahasta.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dt_fechahasta.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dt_fechahasta.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dt_fechahasta.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dt_fechahasta.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dt_fechahasta.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dt_fechahasta.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechahasta.MonthCalendar.DisplayMonth = new System.DateTime(2017, 1, 1, 0, 0, 0, 0);
            this.dt_fechahasta.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.dt_fechahasta.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dt_fechahasta.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dt_fechahasta.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dt_fechahasta.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dt_fechahasta.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dt_fechahasta.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dt_fechahasta.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechahasta.MonthCalendar.TodayButtonVisible = true;
            this.dt_fechahasta.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dt_fechahasta.Name = "dt_fechahasta";
            this.dt_fechahasta.Size = new System.Drawing.Size(94, 20);
            this.dt_fechahasta.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dt_fechahasta.TabIndex = 21;
            // 
            // labelX8
            // 
            this.labelX8.AutoSize = true;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.Class = "";
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Location = new System.Drawing.Point(469, 34);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(31, 15);
            this.labelX8.TabIndex = 20;
            this.labelX8.Text = "Hasta";
            // 
            // cbo_tipomovimientobuscar
            // 
            this.cbo_tipomovimientobuscar.DisplayMember = "Text";
            this.cbo_tipomovimientobuscar.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_tipomovimientobuscar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_tipomovimientobuscar.FormattingEnabled = true;
            this.cbo_tipomovimientobuscar.ItemHeight = 14;
            this.cbo_tipomovimientobuscar.Location = new System.Drawing.Point(352, 6);
            this.cbo_tipomovimientobuscar.Name = "cbo_tipomovimientobuscar";
            this.cbo_tipomovimientobuscar.Size = new System.Drawing.Size(101, 20);
            this.cbo_tipomovimientobuscar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_tipomovimientobuscar.TabIndex = 18;
            // 
            // labelX9
            // 
            this.labelX9.AutoSize = true;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.Class = "";
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Location = new System.Drawing.Point(258, 7);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(83, 15);
            this.labelX9.TabIndex = 19;
            this.labelX9.Text = "Tipo Movimiento";
            // 
            // dt_fechadesde
            // 
            // 
            // 
            // 
            this.dt_fechadesde.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dt_fechadesde.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechadesde.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dt_fechadesde.ButtonDropDown.Visible = true;
            this.dt_fechadesde.Location = new System.Drawing.Point(506, 7);
            // 
            // 
            // 
            this.dt_fechadesde.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dt_fechadesde.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dt_fechadesde.MonthCalendar.BackgroundStyle.Class = "";
            this.dt_fechadesde.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechadesde.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dt_fechadesde.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dt_fechadesde.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dt_fechadesde.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dt_fechadesde.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dt_fechadesde.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dt_fechadesde.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dt_fechadesde.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dt_fechadesde.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechadesde.MonthCalendar.DisplayMonth = new System.DateTime(2017, 1, 1, 0, 0, 0, 0);
            this.dt_fechadesde.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.dt_fechadesde.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dt_fechadesde.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dt_fechadesde.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dt_fechadesde.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dt_fechadesde.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dt_fechadesde.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dt_fechadesde.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechadesde.MonthCalendar.TodayButtonVisible = true;
            this.dt_fechadesde.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dt_fechadesde.Name = "dt_fechadesde";
            this.dt_fechadesde.Size = new System.Drawing.Size(94, 20);
            this.dt_fechadesde.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dt_fechadesde.TabIndex = 11;
            // 
            // labelX11
            // 
            this.labelX11.AutoSize = true;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.Class = "";
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Location = new System.Drawing.Point(466, 8);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(34, 15);
            this.labelX11.TabIndex = 10;
            this.labelX11.Text = "Desde";
            // 
            // uc_grilla
            // 
            this.uc_grilla.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uc_grilla.Location = new System.Drawing.Point(12, 98);
            this.uc_grilla.Name = "uc_grilla";
            this.uc_grilla.Size = new System.Drawing.Size(1192, 407);
            this.uc_grilla.TabIndex = 43;
            // 
            // grupo_movimiento
            // 
            this.grupo_movimiento.BackColor = System.Drawing.Color.Transparent;
            this.grupo_movimiento.CanvasColor = System.Drawing.SystemColors.Control;
            this.grupo_movimiento.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.grupo_movimiento.Controls.Add(this.txt_nrocaja);
            this.grupo_movimiento.Controls.Add(this.txt_puntoatencion);
            this.grupo_movimiento.Controls.Add(this.labelX12);
            this.grupo_movimiento.Controls.Add(this.labelX7);
            this.grupo_movimiento.Controls.Add(this.uc_empresasede);
            this.grupo_movimiento.Controls.Add(this.cbo_tipomovimiento);
            this.grupo_movimiento.Controls.Add(this.labelX10);
            this.grupo_movimiento.Controls.Add(this.txt_serie);
            this.grupo_movimiento.Controls.Add(this.labelX4);
            this.grupo_movimiento.Controls.Add(this.labelX5);
            this.grupo_movimiento.Controls.Add(this.cbo_documento);
            this.grupo_movimiento.Controls.Add(this.txt_numero);
            this.grupo_movimiento.Controls.Add(this.btn_buscarpersona);
            this.grupo_movimiento.Controls.Add(this.btn_borrar);
            this.grupo_movimiento.Controls.Add(this.txt_persona);
            this.grupo_movimiento.Controls.Add(this.lbl_persona);
            this.grupo_movimiento.Controls.Add(this.txt_comentario);
            this.grupo_movimiento.Controls.Add(this.labelX14);
            this.grupo_movimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grupo_movimiento.Location = new System.Drawing.Point(12, 13);
            this.grupo_movimiento.Name = "grupo_movimiento";
            this.grupo_movimiento.Size = new System.Drawing.Size(490, 329);
            // 
            // 
            // 
            this.grupo_movimiento.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grupo_movimiento.Style.BackColorGradientAngle = 90;
            this.grupo_movimiento.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grupo_movimiento.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grupo_movimiento.Style.BorderBottomWidth = 1;
            this.grupo_movimiento.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grupo_movimiento.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grupo_movimiento.Style.BorderLeftWidth = 1;
            this.grupo_movimiento.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grupo_movimiento.Style.BorderRightWidth = 1;
            this.grupo_movimiento.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grupo_movimiento.Style.BorderTopWidth = 1;
            this.grupo_movimiento.Style.Class = "";
            this.grupo_movimiento.Style.CornerDiameter = 4;
            this.grupo_movimiento.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grupo_movimiento.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grupo_movimiento.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grupo_movimiento.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grupo_movimiento.StyleMouseDown.Class = "";
            this.grupo_movimiento.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grupo_movimiento.StyleMouseOver.Class = "";
            this.grupo_movimiento.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grupo_movimiento.TabIndex = 40;
            this.grupo_movimiento.Text = "MOVIMIENTO";
            // 
            // txt_nrocaja
            // 
            // 
            // 
            // 
            this.txt_nrocaja.Border.Class = "TextBoxBorder";
            this.txt_nrocaja.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_nrocaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nrocaja.Location = new System.Drawing.Point(3, 107);
            this.txt_nrocaja.Name = "txt_nrocaja";
            this.txt_nrocaja.ReadOnly = true;
            this.txt_nrocaja.Size = new System.Drawing.Size(202, 20);
            this.txt_nrocaja.TabIndex = 63;
            // 
            // txt_puntoatencion
            // 
            // 
            // 
            // 
            this.txt_puntoatencion.Border.Class = "TextBoxBorder";
            this.txt_puntoatencion.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_puntoatencion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_puntoatencion.Location = new System.Drawing.Point(5, 163);
            this.txt_puntoatencion.Name = "txt_puntoatencion";
            this.txt_puntoatencion.ReadOnly = true;
            this.txt_puntoatencion.Size = new System.Drawing.Size(202, 20);
            this.txt_puntoatencion.TabIndex = 62;
            // 
            // labelX12
            // 
            this.labelX12.AutoSize = true;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.Class = "";
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX12.Location = new System.Drawing.Point(5, 86);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(43, 15);
            this.labelX12.TabIndex = 61;
            this.labelX12.Text = "NroCaja";
            // 
            // labelX7
            // 
            this.labelX7.AutoSize = true;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.Class = "";
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.Location = new System.Drawing.Point(5, 138);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(77, 15);
            this.labelX7.TabIndex = 59;
            this.labelX7.Text = "Punto Atención";
            // 
            // uc_empresasede
            // 
            this.uc_empresasede.BackColor = System.Drawing.Color.Transparent;
            this.uc_empresasede.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uc_empresasede.Location = new System.Drawing.Point(5, 0);
            this.uc_empresasede.Name = "uc_empresasede";
            this.uc_empresasede.PosicionControl = Capa_Entidad.PosicionControl.EnParalelo;
            this.uc_empresasede.Size = new System.Drawing.Size(202, 87);
            this.uc_empresasede.TabIndex = 57;
            // 
            // cbo_tipomovimiento
            // 
            this.cbo_tipomovimiento.DisplayMember = "Text";
            this.cbo_tipomovimiento.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_tipomovimiento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_tipomovimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_tipomovimiento.FormattingEnabled = true;
            this.cbo_tipomovimiento.ItemHeight = 14;
            this.cbo_tipomovimiento.Location = new System.Drawing.Point(226, 26);
            this.cbo_tipomovimiento.Name = "cbo_tipomovimiento";
            this.cbo_tipomovimiento.Size = new System.Drawing.Size(249, 20);
            this.cbo_tipomovimiento.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_tipomovimiento.TabIndex = 18;
            // 
            // labelX10
            // 
            this.labelX10.AutoSize = true;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.Class = "";
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.Location = new System.Drawing.Point(226, 5);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(59, 15);
            this.labelX10.TabIndex = 19;
            this.labelX10.Text = "Movimiento";
            // 
            // txt_serie
            // 
            // 
            // 
            // 
            this.txt_serie.Border.Class = "TextBoxBorder";
            this.txt_serie.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_serie.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_serie.Location = new System.Drawing.Point(226, 105);
            this.txt_serie.Name = "txt_serie";
            this.txt_serie.ReadOnly = true;
            this.txt_serie.Size = new System.Drawing.Size(84, 21);
            this.txt_serie.TabIndex = 49;
            // 
            // labelX4
            // 
            this.labelX4.AutoSize = true;
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.Class = "";
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.Location = new System.Drawing.Point(226, 85);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(45, 16);
            this.labelX4.TabIndex = 41;
            this.labelX4.Text = "Número";
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.Class = "";
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.Location = new System.Drawing.Point(226, 44);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(59, 15);
            this.labelX5.TabIndex = 44;
            this.labelX5.Text = "Documento";
            // 
            // cbo_documento
            // 
            this.cbo_documento.DisplayMember = "Text";
            this.cbo_documento.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_documento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_documento.DropDownWidth = 150;
            this.cbo_documento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_documento.FormattingEnabled = true;
            this.cbo_documento.ItemHeight = 14;
            this.cbo_documento.Location = new System.Drawing.Point(226, 63);
            this.cbo_documento.Name = "cbo_documento";
            this.cbo_documento.Size = new System.Drawing.Size(249, 20);
            this.cbo_documento.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_documento.TabIndex = 43;
            this.cbo_documento.SelectionChangeCommitted += new System.EventHandler(this.cbo_documento_SelectionChangeCommitted);
            // 
            // txt_numero
            // 
            // 
            // 
            // 
            this.txt_numero.Border.Class = "TextBoxBorder";
            this.txt_numero.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_numero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_numero.Location = new System.Drawing.Point(316, 105);
            this.txt_numero.Name = "txt_numero";
            this.txt_numero.ReadOnly = true;
            this.txt_numero.Size = new System.Drawing.Size(159, 21);
            this.txt_numero.TabIndex = 42;
            // 
            // btn_buscarpersona
            // 
            this.btn_buscarpersona.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_buscarpersona.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_buscarpersona.Image = ((System.Drawing.Image)(resources.GetObject("btn_buscarpersona.Image")));
            this.btn_buscarpersona.Location = new System.Drawing.Point(298, 132);
            this.btn_buscarpersona.Name = "btn_buscarpersona";
            this.btn_buscarpersona.Size = new System.Drawing.Size(80, 23);
            this.btn_buscarpersona.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_buscarpersona.TabIndex = 69;
            this.btn_buscarpersona.Text = "(F10)";
            // 
            // btn_borrar
            // 
            this.btn_borrar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_borrar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_borrar.Image = ((System.Drawing.Image)(resources.GetObject("btn_borrar.Image")));
            this.btn_borrar.Location = new System.Drawing.Point(384, 132);
            this.btn_borrar.Name = "btn_borrar";
            this.btn_borrar.Size = new System.Drawing.Size(80, 23);
            this.btn_borrar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_borrar.TabIndex = 71;
            this.btn_borrar.Text = "F(11)";
            // 
            // txt_persona
            // 
            // 
            // 
            // 
            this.txt_persona.Border.Class = "TextBoxBorder";
            this.txt_persona.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_persona.Location = new System.Drawing.Point(226, 163);
            this.txt_persona.Name = "txt_persona";
            this.txt_persona.ReadOnly = true;
            this.txt_persona.Size = new System.Drawing.Size(249, 20);
            this.txt_persona.TabIndex = 70;
            // 
            // lbl_persona
            // 
            this.lbl_persona.AutoSize = true;
            // 
            // 
            // 
            this.lbl_persona.BackgroundStyle.Class = "";
            this.lbl_persona.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_persona.Location = new System.Drawing.Point(227, 138);
            this.lbl_persona.Name = "lbl_persona";
            this.lbl_persona.Size = new System.Drawing.Size(61, 15);
            this.lbl_persona.TabIndex = 68;
            this.lbl_persona.Text = "Destinatario";
            this.lbl_persona.Click += new System.EventHandler(this.lbl_persona_Click);
            // 
            // txt_comentario
            // 
            // 
            // 
            // 
            this.txt_comentario.Border.Class = "TextBoxBorder";
            this.txt_comentario.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_comentario.Location = new System.Drawing.Point(3, 210);
            this.txt_comentario.Multiline = true;
            this.txt_comentario.Name = "txt_comentario";
            this.txt_comentario.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_comentario.Size = new System.Drawing.Size(470, 46);
            this.txt_comentario.TabIndex = 65;
            // 
            // labelX14
            // 
            this.labelX14.AutoSize = true;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.Class = "";
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Location = new System.Drawing.Point(5, 189);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(59, 15);
            this.labelX14.TabIndex = 64;
            this.labelX14.Text = "Comentario";
            // 
            // dt_fechaemision
            // 
            // 
            // 
            // 
            this.dt_fechaemision.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dt_fechaemision.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechaemision.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dt_fechaemision.ButtonDropDown.Visible = true;
            this.dt_fechaemision.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dt_fechaemision.Location = new System.Drawing.Point(121, 3);
            // 
            // 
            // 
            this.dt_fechaemision.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dt_fechaemision.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dt_fechaemision.MonthCalendar.BackgroundStyle.Class = "";
            this.dt_fechaemision.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechaemision.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechaemision.MonthCalendar.DisplayMonth = new System.DateTime(2017, 1, 1, 0, 0, 0, 0);
            this.dt_fechaemision.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.dt_fechaemision.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dt_fechaemision.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dt_fechaemision.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dt_fechaemision.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dt_fechaemision.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dt_fechaemision.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dt_fechaemision.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechaemision.MonthCalendar.TodayButtonVisible = true;
            this.dt_fechaemision.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dt_fechaemision.Name = "dt_fechaemision";
            this.dt_fechaemision.Size = new System.Drawing.Size(184, 20);
            this.dt_fechaemision.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dt_fechaemision.TabIndex = 11;
            // 
            // labelX6
            // 
            this.labelX6.AutoSize = true;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.Class = "";
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.Location = new System.Drawing.Point(37, 8);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(75, 15);
            this.labelX6.TabIndex = 10;
            this.labelX6.Text = "Fecha Emisión";
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.groupPanel1.Controls.Add(this.labelX13);
            this.groupPanel1.Controls.Add(this.cbo_distritop);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.cbo_provinciap);
            this.groupPanel1.Controls.Add(this.labelX15);
            this.groupPanel1.Controls.Add(this.cbo_departamentop);
            this.groupPanel1.Controls.Add(this.txt_direccionp);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel1.Location = new System.Drawing.Point(508, 13);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(342, 133);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.Class = "";
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.Class = "";
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.Class = "";
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 41;
            this.groupPanel1.Text = "PARTIDA";
            // 
            // labelX13
            // 
            this.labelX13.AutoSize = true;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.Class = "";
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.Location = new System.Drawing.Point(53, 58);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(37, 15);
            this.labelX13.TabIndex = 107;
            this.labelX13.Text = "Distrito";
            // 
            // cbo_distritop
            // 
            this.cbo_distritop.DisplayMember = "Text";
            this.cbo_distritop.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_distritop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_distritop.FormattingEnabled = true;
            this.cbo_distritop.ItemHeight = 14;
            this.cbo_distritop.Location = new System.Drawing.Point(103, 55);
            this.cbo_distritop.Name = "cbo_distritop";
            this.cbo_distritop.Size = new System.Drawing.Size(225, 20);
            this.cbo_distritop.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_distritop.TabIndex = 106;
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(40, 31);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(48, 15);
            this.labelX1.TabIndex = 105;
            this.labelX1.Text = "Provincia";
            // 
            // cbo_provinciap
            // 
            this.cbo_provinciap.DisplayMember = "Text";
            this.cbo_provinciap.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_provinciap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_provinciap.FormattingEnabled = true;
            this.cbo_provinciap.ItemHeight = 14;
            this.cbo_provinciap.Location = new System.Drawing.Point(103, 29);
            this.cbo_provinciap.Name = "cbo_provinciap";
            this.cbo_provinciap.Size = new System.Drawing.Size(225, 20);
            this.cbo_provinciap.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_provinciap.TabIndex = 104;
            this.cbo_provinciap.SelectionChangeCommitted += new System.EventHandler(this.cbo_provinciap_SelectionChangeCommitted);
            // 
            // labelX15
            // 
            this.labelX15.AutoSize = true;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.Class = "";
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Location = new System.Drawing.Point(10, 5);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(72, 15);
            this.labelX15.TabIndex = 103;
            this.labelX15.Text = "Departamento";
            // 
            // cbo_departamentop
            // 
            this.cbo_departamentop.DisplayMember = "Text";
            this.cbo_departamentop.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_departamentop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_departamentop.FormattingEnabled = true;
            this.cbo_departamentop.ItemHeight = 14;
            this.cbo_departamentop.Location = new System.Drawing.Point(103, 3);
            this.cbo_departamentop.Name = "cbo_departamentop";
            this.cbo_departamentop.Size = new System.Drawing.Size(225, 20);
            this.cbo_departamentop.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_departamentop.TabIndex = 102;
            this.cbo_departamentop.SelectionChangeCommitted += new System.EventHandler(this.cbo_departamentop_SelectionChangeCommitted);
            // 
            // txt_direccionp
            // 
            // 
            // 
            // 
            this.txt_direccionp.Border.Class = "TextBoxBorder";
            this.txt_direccionp.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_direccionp.Location = new System.Drawing.Point(103, 81);
            this.txt_direccionp.Name = "txt_direccionp";
            this.txt_direccionp.Size = new System.Drawing.Size(225, 20);
            this.txt_direccionp.TabIndex = 100;
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.Class = "";
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(39, 83);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(48, 15);
            this.labelX3.TabIndex = 101;
            this.labelX3.Text = "Dirección";
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel3.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.groupPanel3.Controls.Add(this.labelX16);
            this.groupPanel3.Controls.Add(this.cbo_distritoll);
            this.groupPanel3.Controls.Add(this.labelX17);
            this.groupPanel3.Controls.Add(this.cbo_provinciall);
            this.groupPanel3.Controls.Add(this.labelX18);
            this.groupPanel3.Controls.Add(this.cbo_departamentoll);
            this.groupPanel3.Controls.Add(this.txt_direccionll);
            this.groupPanel3.Controls.Add(this.labelX19);
            this.groupPanel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel3.Location = new System.Drawing.Point(856, 13);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(342, 133);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.Class = "";
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.Class = "";
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.Class = "";
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 42;
            this.groupPanel3.Text = "LLEGADA";
            // 
            // labelX16
            // 
            this.labelX16.AutoSize = true;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.Class = "";
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.Location = new System.Drawing.Point(53, 58);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(37, 15);
            this.labelX16.TabIndex = 107;
            this.labelX16.Text = "Distrito";
            // 
            // cbo_distritoll
            // 
            this.cbo_distritoll.DisplayMember = "Text";
            this.cbo_distritoll.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_distritoll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_distritoll.FormattingEnabled = true;
            this.cbo_distritoll.ItemHeight = 14;
            this.cbo_distritoll.Location = new System.Drawing.Point(103, 55);
            this.cbo_distritoll.Name = "cbo_distritoll";
            this.cbo_distritoll.Size = new System.Drawing.Size(225, 20);
            this.cbo_distritoll.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_distritoll.TabIndex = 106;
            // 
            // labelX17
            // 
            this.labelX17.AutoSize = true;
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.Class = "";
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX17.Location = new System.Drawing.Point(40, 31);
            this.labelX17.Name = "labelX17";
            this.labelX17.Size = new System.Drawing.Size(48, 15);
            this.labelX17.TabIndex = 105;
            this.labelX17.Text = "Provincia";
            // 
            // cbo_provinciall
            // 
            this.cbo_provinciall.DisplayMember = "Text";
            this.cbo_provinciall.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_provinciall.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_provinciall.FormattingEnabled = true;
            this.cbo_provinciall.ItemHeight = 14;
            this.cbo_provinciall.Location = new System.Drawing.Point(103, 29);
            this.cbo_provinciall.Name = "cbo_provinciall";
            this.cbo_provinciall.Size = new System.Drawing.Size(225, 20);
            this.cbo_provinciall.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_provinciall.TabIndex = 104;
            this.cbo_provinciall.SelectionChangeCommitted += new System.EventHandler(this.cbo_provinciall_SelectionChangeCommitted);
            // 
            // labelX18
            // 
            this.labelX18.AutoSize = true;
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.Class = "";
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.Location = new System.Drawing.Point(10, 5);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(72, 15);
            this.labelX18.TabIndex = 103;
            this.labelX18.Text = "Departamento";
            // 
            // cbo_departamentoll
            // 
            this.cbo_departamentoll.DisplayMember = "Text";
            this.cbo_departamentoll.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_departamentoll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_departamentoll.FormattingEnabled = true;
            this.cbo_departamentoll.ItemHeight = 14;
            this.cbo_departamentoll.Location = new System.Drawing.Point(103, 3);
            this.cbo_departamentoll.Name = "cbo_departamentoll";
            this.cbo_departamentoll.Size = new System.Drawing.Size(225, 20);
            this.cbo_departamentoll.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_departamentoll.TabIndex = 102;
            this.cbo_departamentoll.SelectionChangeCommitted += new System.EventHandler(this.cbo_departamentoll_SelectionChangeCommitted);
            // 
            // txt_direccionll
            // 
            // 
            // 
            // 
            this.txt_direccionll.Border.Class = "TextBoxBorder";
            this.txt_direccionll.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_direccionll.Location = new System.Drawing.Point(103, 81);
            this.txt_direccionll.Name = "txt_direccionll";
            this.txt_direccionll.Size = new System.Drawing.Size(225, 20);
            this.txt_direccionll.TabIndex = 100;
            // 
            // labelX19
            // 
            this.labelX19.AutoSize = true;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.Class = "";
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.Location = new System.Drawing.Point(39, 83);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(48, 15);
            this.labelX19.TabIndex = 101;
            this.labelX19.Text = "Dirección";
            // 
            // grupo_producto
            // 
            this.grupo_producto.BackColor = System.Drawing.Color.Transparent;
            this.grupo_producto.CanvasColor = System.Drawing.SystemColors.Control;
            this.grupo_producto.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.grupo_producto.Controls.Add(this.lbl_codigo);
            this.grupo_producto.Controls.Add(this.txt_codigoproducto);
            this.grupo_producto.Controls.Add(this.btn_buscar);
            this.grupo_producto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grupo_producto.Location = new System.Drawing.Point(12, 360);
            this.grupo_producto.Name = "grupo_producto";
            this.grupo_producto.Size = new System.Drawing.Size(698, 53);
            // 
            // 
            // 
            this.grupo_producto.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grupo_producto.Style.BackColorGradientAngle = 90;
            this.grupo_producto.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grupo_producto.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grupo_producto.Style.BorderBottomWidth = 1;
            this.grupo_producto.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grupo_producto.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grupo_producto.Style.BorderLeftWidth = 1;
            this.grupo_producto.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grupo_producto.Style.BorderRightWidth = 1;
            this.grupo_producto.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grupo_producto.Style.BorderTopWidth = 1;
            this.grupo_producto.Style.Class = "";
            this.grupo_producto.Style.CornerDiameter = 4;
            this.grupo_producto.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grupo_producto.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grupo_producto.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grupo_producto.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grupo_producto.StyleMouseDown.Class = "";
            this.grupo_producto.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grupo_producto.StyleMouseOver.Class = "";
            this.grupo_producto.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grupo_producto.TabIndex = 43;
            this.grupo_producto.Text = "PRODUCTO";
            // 
            // lbl_codigo
            // 
            this.lbl_codigo.AutoSize = true;
            this.lbl_codigo.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_codigo.BackgroundStyle.Class = "";
            this.lbl_codigo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_codigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_codigo.Location = new System.Drawing.Point(5, 7);
            this.lbl_codigo.Name = "lbl_codigo";
            this.lbl_codigo.Size = new System.Drawing.Size(37, 15);
            this.lbl_codigo.TabIndex = 56;
            this.lbl_codigo.Text = "Código";
            // 
            // txt_codigoproducto
            // 
            // 
            // 
            // 
            this.txt_codigoproducto.Border.Class = "TextBoxBorder";
            this.txt_codigoproducto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_codigoproducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_codigoproducto.Location = new System.Drawing.Point(47, 6);
            this.txt_codigoproducto.Name = "txt_codigoproducto";
            this.txt_codigoproducto.Size = new System.Drawing.Size(535, 20);
            this.txt_codigoproducto.TabIndex = 35;
            // 
            // btn_buscar
            // 
            this.btn_buscar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_buscar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_buscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_buscar.Image = ((System.Drawing.Image)(resources.GetObject("btn_buscar.Image")));
            this.btn_buscar.Location = new System.Drawing.Point(588, 5);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(93, 23);
            this.btn_buscar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_buscar.TabIndex = 36;
            this.btn_buscar.Text = "Buscar(F7)";
            // 
            // dgv_mostrar
            // 
            this.dgv_mostrar.AllowUserToAddRows = false;
            this.dgv_mostrar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_mostrar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_mostrar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_idproducto,
            this.col_codigo,
            this.col_descripcion,
            this.col_marca,
            this.col_cantidad,
            this.col_unidadmedida,
            this.col_preciocosto,
            this.col_precioventa,
            this.col_espvv,
            this.col_precio,
            this.col_importe,
            this.col_descuento,
            this.col_total,
            this.col_stock,
            this.col_preciomayor,
            this.col_pm,
            this.col_idtipo,
            this.col_precioventa1,
            this.col_precioventa2,
            this.col_precioventa3,
            this.col_precioventa4,
            this.col_cantidadmayor1,
            this.col_cantidadmayor2,
            this.col_cantidadmayor3,
            this.col_cantidadmayor4,
            this.col_espaquete,
            this.col_esserielote});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_mostrar.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgv_mostrar.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgv_mostrar.Location = new System.Drawing.Point(12, 429);
            this.dgv_mostrar.MultiSelect = false;
            this.dgv_mostrar.Name = "dgv_mostrar";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_mostrar.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgv_mostrar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgv_mostrar.Size = new System.Drawing.Size(1153, 266);
            this.dgv_mostrar.TabIndex = 44;
            // 
            // col_idproducto
            // 
            this.col_idproducto.DataPropertyName = "IdProducto";
            this.col_idproducto.HeaderText = "Idproducto";
            this.col_idproducto.Name = "col_idproducto";
            this.col_idproducto.ReadOnly = true;
            this.col_idproducto.Visible = false;
            this.col_idproducto.Width = 50;
            // 
            // col_codigo
            // 
            this.col_codigo.DataPropertyName = "Codigo";
            this.col_codigo.HeaderText = "Codigo";
            this.col_codigo.Name = "col_codigo";
            this.col_codigo.ReadOnly = true;
            this.col_codigo.Width = 90;
            // 
            // col_descripcion
            // 
            this.col_descripcion.DataPropertyName = "Descripcion";
            this.col_descripcion.HeaderText = "Descripción";
            this.col_descripcion.Name = "col_descripcion";
            this.col_descripcion.ReadOnly = true;
            this.col_descripcion.Width = 250;
            // 
            // col_marca
            // 
            this.col_marca.DataPropertyName = "Marca";
            this.col_marca.HeaderText = "Marca";
            this.col_marca.Name = "col_marca";
            this.col_marca.ReadOnly = true;
            this.col_marca.Visible = false;
            // 
            // col_cantidad
            // 
            this.col_cantidad.DataPropertyName = "Cantidad";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_cantidad.DefaultCellStyle = dataGridViewCellStyle8;
            this.col_cantidad.HeaderText = "Cant";
            this.col_cantidad.Name = "col_cantidad";
            this.col_cantidad.ReadOnly = true;
            this.col_cantidad.Width = 50;
            // 
            // col_unidadmedida
            // 
            this.col_unidadmedida.DataPropertyName = "UnidadMedida";
            this.col_unidadmedida.HeaderText = "UnidadMedida";
            this.col_unidadmedida.Name = "col_unidadmedida";
            this.col_unidadmedida.ReadOnly = true;
            this.col_unidadmedida.Visible = false;
            this.col_unidadmedida.Width = 80;
            // 
            // col_preciocosto
            // 
            this.col_preciocosto.DataPropertyName = "PrecioCosto";
            this.col_preciocosto.HeaderText = "PrecioCosto";
            this.col_preciocosto.Name = "col_preciocosto";
            this.col_preciocosto.ReadOnly = true;
            this.col_preciocosto.Visible = false;
            // 
            // col_precioventa
            // 
            this.col_precioventa.DataPropertyName = "PrecioVenta";
            this.col_precioventa.HeaderText = "PrecioVenta";
            this.col_precioventa.Name = "col_precioventa";
            this.col_precioventa.ReadOnly = true;
            this.col_precioventa.Visible = false;
            // 
            // col_espvv
            // 
            this.col_espvv.DataPropertyName = "EsPvv";
            this.col_espvv.HeaderText = "Pvv";
            this.col_espvv.Name = "col_espvv";
            this.col_espvv.Visible = false;
            // 
            // col_precio
            // 
            this.col_precio.DataPropertyName = "Precio";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.NullValue = null;
            this.col_precio.DefaultCellStyle = dataGridViewCellStyle9;
            this.col_precio.HeaderText = "Precio";
            this.col_precio.Name = "col_precio";
            this.col_precio.ReadOnly = true;
            this.col_precio.Width = 70;
            // 
            // col_importe
            // 
            this.col_importe.DataPropertyName = "Importe";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_importe.DefaultCellStyle = dataGridViewCellStyle10;
            this.col_importe.HeaderText = "Importe";
            this.col_importe.Name = "col_importe";
            this.col_importe.ReadOnly = true;
            this.col_importe.Width = 80;
            // 
            // col_descuento
            // 
            this.col_descuento.DataPropertyName = "Descuento";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_descuento.DefaultCellStyle = dataGridViewCellStyle11;
            this.col_descuento.HeaderText = "Descuento";
            this.col_descuento.Name = "col_descuento";
            this.col_descuento.ReadOnly = true;
            this.col_descuento.Width = 62;
            // 
            // col_total
            // 
            this.col_total.DataPropertyName = "Total";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_total.DefaultCellStyle = dataGridViewCellStyle12;
            this.col_total.HeaderText = "Total";
            this.col_total.Name = "col_total";
            this.col_total.ReadOnly = true;
            this.col_total.Width = 80;
            // 
            // col_stock
            // 
            this.col_stock.DataPropertyName = "Stock";
            this.col_stock.HeaderText = "Stock";
            this.col_stock.Name = "col_stock";
            this.col_stock.Visible = false;
            // 
            // col_preciomayor
            // 
            this.col_preciomayor.DataPropertyName = "PrecioMayor";
            this.col_preciomayor.HeaderText = "PrecioMayor";
            this.col_preciomayor.Name = "col_preciomayor";
            this.col_preciomayor.Visible = false;
            // 
            // col_pm
            // 
            this.col_pm.HeaderText = "Pm";
            this.col_pm.Name = "col_pm";
            this.col_pm.Width = 30;
            // 
            // col_idtipo
            // 
            this.col_idtipo.DataPropertyName = "IdTipo";
            this.col_idtipo.HeaderText = "IdTipo";
            this.col_idtipo.Name = "col_idtipo";
            this.col_idtipo.Visible = false;
            // 
            // col_precioventa1
            // 
            this.col_precioventa1.DataPropertyName = "PrecioVenta1";
            this.col_precioventa1.HeaderText = "PrecioVenta1";
            this.col_precioventa1.Name = "col_precioventa1";
            this.col_precioventa1.Visible = false;
            // 
            // col_precioventa2
            // 
            this.col_precioventa2.DataPropertyName = "PrecioVenta2";
            this.col_precioventa2.HeaderText = "PrecioVenta2";
            this.col_precioventa2.Name = "col_precioventa2";
            this.col_precioventa2.Visible = false;
            // 
            // col_precioventa3
            // 
            this.col_precioventa3.DataPropertyName = "PrecioVenta3";
            this.col_precioventa3.HeaderText = "PrecioVenta3";
            this.col_precioventa3.Name = "col_precioventa3";
            this.col_precioventa3.Visible = false;
            // 
            // col_precioventa4
            // 
            this.col_precioventa4.DataPropertyName = "PrecioVenta4";
            this.col_precioventa4.HeaderText = "PrecioVenta4";
            this.col_precioventa4.Name = "col_precioventa4";
            this.col_precioventa4.Visible = false;
            // 
            // col_cantidadmayor1
            // 
            this.col_cantidadmayor1.DataPropertyName = "CantidadMayor1";
            this.col_cantidadmayor1.HeaderText = "CantidadMayor1";
            this.col_cantidadmayor1.Name = "col_cantidadmayor1";
            this.col_cantidadmayor1.Visible = false;
            // 
            // col_cantidadmayor2
            // 
            this.col_cantidadmayor2.DataPropertyName = "CantidadMayor2";
            this.col_cantidadmayor2.HeaderText = "CantidadMayor2";
            this.col_cantidadmayor2.Name = "col_cantidadmayor2";
            this.col_cantidadmayor2.Visible = false;
            // 
            // col_cantidadmayor3
            // 
            this.col_cantidadmayor3.DataPropertyName = "CantidadMayor3";
            this.col_cantidadmayor3.HeaderText = "CantidadMayor3";
            this.col_cantidadmayor3.Name = "col_cantidadmayor3";
            this.col_cantidadmayor3.Visible = false;
            // 
            // col_cantidadmayor4
            // 
            this.col_cantidadmayor4.DataPropertyName = "CantidadMayor4";
            this.col_cantidadmayor4.HeaderText = "CantidadMayor4";
            this.col_cantidadmayor4.Name = "col_cantidadmayor4";
            this.col_cantidadmayor4.Visible = false;
            // 
            // col_espaquete
            // 
            this.col_espaquete.DataPropertyName = "EsPaquete";
            this.col_espaquete.HeaderText = "EsPaquete";
            this.col_espaquete.Name = "col_espaquete";
            this.col_espaquete.Visible = false;
            // 
            // col_esserielote
            // 
            this.col_esserielote.DataPropertyName = "EsSerieLote";
            this.col_esserielote.HeaderText = "EsSerieLote";
            this.col_esserielote.Name = "col_esserielote";
            this.col_esserielote.Visible = false;
            // 
            // dt_fechatraslado
            // 
            // 
            // 
            // 
            this.dt_fechatraslado.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dt_fechatraslado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechatraslado.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dt_fechatraslado.ButtonDropDown.Visible = true;
            this.dt_fechatraslado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dt_fechatraslado.Location = new System.Drawing.Point(121, 28);
            // 
            // 
            // 
            this.dt_fechatraslado.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dt_fechatraslado.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dt_fechatraslado.MonthCalendar.BackgroundStyle.Class = "";
            this.dt_fechatraslado.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechatraslado.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dt_fechatraslado.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dt_fechatraslado.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dt_fechatraslado.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dt_fechatraslado.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dt_fechatraslado.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dt_fechatraslado.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dt_fechatraslado.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dt_fechatraslado.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechatraslado.MonthCalendar.DisplayMonth = new System.DateTime(2017, 1, 1, 0, 0, 0, 0);
            this.dt_fechatraslado.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.dt_fechatraslado.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dt_fechatraslado.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dt_fechatraslado.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dt_fechatraslado.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dt_fechatraslado.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dt_fechatraslado.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dt_fechatraslado.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechatraslado.MonthCalendar.TodayButtonVisible = true;
            this.dt_fechatraslado.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dt_fechatraslado.Name = "dt_fechatraslado";
            this.dt_fechatraslado.Size = new System.Drawing.Size(184, 20);
            this.dt_fechatraslado.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dt_fechatraslado.TabIndex = 67;
            // 
            // labelX20
            // 
            this.labelX20.AutoSize = true;
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.Class = "";
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX20.Location = new System.Drawing.Point(36, 29);
            this.labelX20.Name = "labelX20";
            this.labelX20.Size = new System.Drawing.Size(79, 15);
            this.labelX20.TabIndex = 66;
            this.labelX20.Text = "Fecha Traslado";
            // 
            // labelX21
            // 
            this.labelX21.AutoSize = true;
            // 
            // 
            // 
            this.labelX21.BackgroundStyle.Class = "";
            this.labelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX21.Location = new System.Drawing.Point(6, 89);
            this.labelX21.Name = "labelX21";
            this.labelX21.Size = new System.Drawing.Size(109, 15);
            this.labelX21.TabIndex = 109;
            this.labelX21.Text = "Modalidad Transporte";
            // 
            // cbo_modalidadtraslado
            // 
            this.cbo_modalidadtraslado.DisplayMember = "Text";
            this.cbo_modalidadtraslado.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_modalidadtraslado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_modalidadtraslado.FormattingEnabled = true;
            this.cbo_modalidadtraslado.ItemHeight = 14;
            this.cbo_modalidadtraslado.Location = new System.Drawing.Point(121, 87);
            this.cbo_modalidadtraslado.Name = "cbo_modalidadtraslado";
            this.cbo_modalidadtraslado.Size = new System.Drawing.Size(184, 20);
            this.cbo_modalidadtraslado.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_modalidadtraslado.TabIndex = 108;
            // 
            // labelX22
            // 
            this.labelX22.AutoSize = true;
            // 
            // 
            // 
            this.labelX22.BackgroundStyle.Class = "";
            this.labelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX22.Location = new System.Drawing.Point(31, 61);
            this.labelX22.Name = "labelX22";
            this.labelX22.Size = new System.Drawing.Size(81, 15);
            this.labelX22.TabIndex = 107;
            this.labelX22.Text = "Motivo Traslado";
            // 
            // cbo_motivotraslado
            // 
            this.cbo_motivotraslado.DisplayMember = "Text";
            this.cbo_motivotraslado.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbo_motivotraslado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_motivotraslado.FormattingEnabled = true;
            this.cbo_motivotraslado.ItemHeight = 14;
            this.cbo_motivotraslado.Location = new System.Drawing.Point(121, 58);
            this.cbo_motivotraslado.Name = "cbo_motivotraslado";
            this.cbo_motivotraslado.Size = new System.Drawing.Size(184, 20);
            this.cbo_motivotraslado.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbo_motivotraslado.TabIndex = 106;
            // 
            // textBoxX2
            // 
            // 
            // 
            // 
            this.textBoxX2.Border.Class = "TextBoxBorder";
            this.textBoxX2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX2.Location = new System.Drawing.Point(121, 113);
            this.textBoxX2.Name = "textBoxX2";
            this.textBoxX2.Size = new System.Drawing.Size(184, 20);
            this.textBoxX2.TabIndex = 111;
            // 
            // labelX23
            // 
            this.labelX23.AutoSize = true;
            // 
            // 
            // 
            this.labelX23.BackgroundStyle.Class = "";
            this.labelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX23.Location = new System.Drawing.Point(8, 114);
            this.labelX23.Name = "labelX23";
            this.labelX23.Size = new System.Drawing.Size(107, 15);
            this.labelX23.TabIndex = 110;
            this.labelX23.Text = "Peso Bruto Total(KG)";
            // 
            // textBoxX3
            // 
            // 
            // 
            // 
            this.textBoxX3.Border.Class = "TextBoxBorder";
            this.textBoxX3.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX3.Location = new System.Drawing.Point(121, 139);
            this.textBoxX3.Name = "textBoxX3";
            this.textBoxX3.Size = new System.Drawing.Size(184, 20);
            this.textBoxX3.TabIndex = 113;
            // 
            // labelX24
            // 
            this.labelX24.AutoSize = true;
            // 
            // 
            // 
            this.labelX24.BackgroundStyle.Class = "";
            this.labelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX24.Location = new System.Drawing.Point(25, 140);
            this.labelX24.Name = "labelX24";
            this.labelX24.Size = new System.Drawing.Size(90, 15);
            this.labelX24.TabIndex = 112;
            this.labelX24.Text = "Número de Bultos";
            // 
            // labelX25
            // 
            this.labelX25.AutoSize = true;
            // 
            // 
            // 
            this.labelX25.BackgroundStyle.Class = "";
            this.labelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX25.Location = new System.Drawing.Point(23, 84);
            this.labelX25.Name = "labelX25";
            this.labelX25.Size = new System.Drawing.Size(55, 15);
            this.labelX25.TabIndex = 116;
            this.labelX25.Text = "Certificado";
            // 
            // textBoxX4
            // 
            // 
            // 
            // 
            this.textBoxX4.Border.Class = "TextBoxBorder";
            this.textBoxX4.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX4.Location = new System.Drawing.Point(84, 83);
            this.textBoxX4.Name = "textBoxX4";
            this.textBoxX4.Size = new System.Drawing.Size(184, 20);
            this.textBoxX4.TabIndex = 117;
            // 
            // labelX26
            // 
            this.labelX26.AutoSize = true;
            // 
            // 
            // 
            this.labelX26.BackgroundStyle.Class = "";
            this.labelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX26.Location = new System.Drawing.Point(23, 63);
            this.labelX26.Name = "labelX26";
            this.labelX26.Size = new System.Drawing.Size(30, 15);
            this.labelX26.TabIndex = 114;
            this.labelX26.Text = "Placa";
            // 
            // textBoxX5
            // 
            // 
            // 
            // 
            this.textBoxX5.Border.Class = "TextBoxBorder";
            this.textBoxX5.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX5.Location = new System.Drawing.Point(84, 57);
            this.textBoxX5.Name = "textBoxX5";
            this.textBoxX5.Size = new System.Drawing.Size(184, 20);
            this.textBoxX5.TabIndex = 115;
            // 
            // labelX27
            // 
            this.labelX27.AutoSize = true;
            // 
            // 
            // 
            this.labelX27.BackgroundStyle.Class = "";
            this.labelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX27.Location = new System.Drawing.Point(23, 112);
            this.labelX27.Name = "labelX27";
            this.labelX27.Size = new System.Drawing.Size(43, 15);
            this.labelX27.TabIndex = 118;
            this.labelX27.Text = "Licencia";
            // 
            // textBoxX6
            // 
            // 
            // 
            // 
            this.textBoxX6.Border.Class = "TextBoxBorder";
            this.textBoxX6.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX6.Location = new System.Drawing.Point(84, 111);
            this.textBoxX6.Name = "textBoxX6";
            this.textBoxX6.Size = new System.Drawing.Size(184, 20);
            this.textBoxX6.TabIndex = 119;
            // 
            // labelX28
            // 
            this.labelX28.AutoSize = true;
            // 
            // 
            // 
            this.labelX28.BackgroundStyle.Class = "";
            this.labelX28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX28.Location = new System.Drawing.Point(23, 143);
            this.labelX28.Name = "labelX28";
            this.labelX28.Size = new System.Drawing.Size(53, 15);
            this.labelX28.TabIndex = 120;
            this.labelX28.Text = "Conductor";
            // 
            // textBoxX7
            // 
            // 
            // 
            // 
            this.textBoxX7.Border.Class = "TextBoxBorder";
            this.textBoxX7.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX7.Location = new System.Drawing.Point(84, 142);
            this.textBoxX7.Name = "textBoxX7";
            this.textBoxX7.Size = new System.Drawing.Size(184, 20);
            this.textBoxX7.TabIndex = 121;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Image = ((System.Drawing.Image)(resources.GetObject("buttonX1.Image")));
            this.buttonX1.Location = new System.Drawing.Point(170, 3);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(80, 23);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.TabIndex = 125;
            this.buttonX1.Text = "F(11)";
            // 
            // textBoxX8
            // 
            // 
            // 
            // 
            this.textBoxX8.Border.Class = "TextBoxBorder";
            this.textBoxX8.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX8.Location = new System.Drawing.Point(15, 31);
            this.textBoxX8.Name = "textBoxX8";
            this.textBoxX8.ReadOnly = true;
            this.textBoxX8.Size = new System.Drawing.Size(253, 20);
            this.textBoxX8.TabIndex = 124;
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Image = ((System.Drawing.Image)(resources.GetObject("buttonX2.Image")));
            this.buttonX2.Location = new System.Drawing.Point(84, 3);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(80, 23);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.TabIndex = 123;
            this.buttonX2.Text = "(F10)";
            // 
            // labelX29
            // 
            this.labelX29.AutoSize = true;
            // 
            // 
            // 
            this.labelX29.BackgroundStyle.Class = "";
            this.labelX29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX29.Location = new System.Drawing.Point(17, 10);
            this.labelX29.Name = "labelX29";
            this.labelX29.Size = new System.Drawing.Size(67, 15);
            this.labelX29.TabIndex = 122;
            this.labelX29.Text = "Transportista";
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel4.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.groupPanel4.Controls.Add(this.dt_fechaemision);
            this.groupPanel4.Controls.Add(this.labelX24);
            this.groupPanel4.Controls.Add(this.labelX21);
            this.groupPanel4.Controls.Add(this.cbo_modalidadtraslado);
            this.groupPanel4.Controls.Add(this.textBoxX3);
            this.groupPanel4.Controls.Add(this.labelX22);
            this.groupPanel4.Controls.Add(this.labelX6);
            this.groupPanel4.Controls.Add(this.cbo_motivotraslado);
            this.groupPanel4.Controls.Add(this.textBoxX2);
            this.groupPanel4.Controls.Add(this.labelX20);
            this.groupPanel4.Controls.Add(this.labelX23);
            this.groupPanel4.Controls.Add(this.dt_fechatraslado);
            this.groupPanel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel4.Location = new System.Drawing.Point(508, 152);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.Size = new System.Drawing.Size(342, 190);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.Class = "";
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.Class = "";
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.Class = "";
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 126;
            this.groupPanel4.Text = "ENVIO";
            // 
            // groupPanel5
            // 
            this.groupPanel5.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel5.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.groupPanel5.Controls.Add(this.buttonX2);
            this.groupPanel5.Controls.Add(this.textBoxX5);
            this.groupPanel5.Controls.Add(this.buttonX1);
            this.groupPanel5.Controls.Add(this.labelX26);
            this.groupPanel5.Controls.Add(this.textBoxX4);
            this.groupPanel5.Controls.Add(this.textBoxX8);
            this.groupPanel5.Controls.Add(this.labelX25);
            this.groupPanel5.Controls.Add(this.textBoxX6);
            this.groupPanel5.Controls.Add(this.labelX27);
            this.groupPanel5.Controls.Add(this.labelX29);
            this.groupPanel5.Controls.Add(this.textBoxX7);
            this.groupPanel5.Controls.Add(this.labelX28);
            this.groupPanel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel5.Location = new System.Drawing.Point(856, 152);
            this.groupPanel5.Name = "groupPanel5";
            this.groupPanel5.Size = new System.Drawing.Size(342, 190);
            // 
            // 
            // 
            this.groupPanel5.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel5.Style.BackColorGradientAngle = 90;
            this.groupPanel5.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderBottomWidth = 1;
            this.groupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderLeftWidth = 1;
            this.groupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderRightWidth = 1;
            this.groupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderTopWidth = 1;
            this.groupPanel5.Style.Class = "";
            this.groupPanel5.Style.CornerDiameter = 4;
            this.groupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseDown.Class = "";
            this.groupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseOver.Class = "";
            this.groupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel5.TabIndex = 127;
            this.groupPanel5.Text = "TRANSPORTISTA";
            // 
            // frmGuia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 664);
            this.DoubleBuffered = true;
            this.Name = "frmGuia";
            this.Text = "frmGuia";
            this.Load += new System.EventHandler(this.frmGuia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tab_padre)).EndInit();
            this.tab_padre.ResumeLayout(false);
            this.tab_registro1.ResumeLayout(false);
            this.tab_mostrar1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dt_fechahasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_fechadesde)).EndInit();
            this.grupo_movimiento.ResumeLayout(false);
            this.grupo_movimiento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dt_fechaemision)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel3.PerformLayout();
            this.grupo_producto.ResumeLayout(false);
            this.grupo_producto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_mostrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_fechatraslado)).EndInit();
            this.groupPanel4.ResumeLayout(false);
            this.groupPanel4.PerformLayout();
            this.groupPanel5.ResumeLayout(false);
            this.groupPanel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private System.Windows.Forms.CheckBox chk_condeuda;
        private DevComponents.DotNetBar.ButtonX btn_borrarfiltro;
        private DevComponents.DotNetBar.LabelX lbl_personafiltro;
        private System.Windows.Forms.CheckBox chk_escredito;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_filtrarpersona;
        private DevComponents.DotNetBar.ButtonX btn_persona;
        private DevComponents.DotNetBar.LabelX labelX2;
        private Controles.uc_empresasede uc_empresasedefiltro;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_usuariofiltro;
        private System.Windows.Forms.CheckBox chk_nromovimiento;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_nrodocumento;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dt_fechahasta;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_tipomovimientobuscar;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dt_fechadesde;
        private DevComponents.DotNetBar.LabelX labelX11;
        private Controles.uc_grilla uc_grilla;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.GroupPanel grupo_movimiento;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_comentario;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_nrocaja;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_puntoatencion;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX7;
        private Controles.uc_empresasede uc_empresasede;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_tipomovimiento;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_serie;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dt_fechaemision;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_documento;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_numero;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_distritop;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_provinciap;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_departamentop;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_direccionp;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.LabelX labelX16;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_distritoll;
        private DevComponents.DotNetBar.LabelX labelX17;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_provinciall;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_departamentoll;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_direccionll;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.Controls.GroupPanel grupo_producto;
        private DevComponents.DotNetBar.LabelX lbl_codigo;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_codigoproducto;
        private DevComponents.DotNetBar.ButtonX btn_buscar;
        public DevComponents.DotNetBar.Controls.DataGridViewX dgv_mostrar;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_idproducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_marca;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_unidadmedida;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_preciocosto;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_precioventa;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_espvv;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_importe;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_descuento;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_total;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_stock;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_preciomayor;
        private System.Windows.Forms.DataGridViewCheckBoxColumn col_pm;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_idtipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_precioventa1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_precioventa2;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_precioventa3;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_precioventa4;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_cantidadmayor1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_cantidadmayor2;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_cantidadmayor3;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_cantidadmayor4;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_espaquete;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_esserielote;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dt_fechatraslado;
        private DevComponents.DotNetBar.LabelX labelX20;
        private DevComponents.DotNetBar.ButtonX btn_borrar;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_persona;
        private DevComponents.DotNetBar.ButtonX btn_buscarpersona;
        private DevComponents.DotNetBar.LabelX lbl_persona;
        private DevComponents.DotNetBar.LabelX labelX21;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_modalidadtraslado;
        private DevComponents.DotNetBar.LabelX labelX22;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbo_motivotraslado;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX3;
        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX2;
        private DevComponents.DotNetBar.LabelX labelX23;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX8;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.LabelX labelX29;
        private DevComponents.DotNetBar.LabelX labelX28;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX7;
        private DevComponents.DotNetBar.LabelX labelX27;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX6;
        private DevComponents.DotNetBar.LabelX labelX25;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX4;
        private DevComponents.DotNetBar.LabelX labelX26;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX5;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel5;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
    }
}