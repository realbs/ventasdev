﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Capa_Entidad
{
    /// <summary>
    /// Entidad clsReporte
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.sistema.com.pe/sistema/serviciosweb")]
    public class clsReporte
    {
        #region Campos

        private Int32 _intidreporte;
        private String _strdescripcion;
        private String _strsp;
        private Int16? _intestabular;
        private Int16? _intidestado;
        private DateTime? _datfechadesde;
        private DateTime? _datfechahasta;

        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdReporte
        {
            get { return _intidreporte; }
            set { _intidreporte = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Descripcion
        {
            get { return _strdescripcion; }
            set { _strdescripcion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String SP
        {
            get { return _strsp; }
            set { _strsp = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? EsTabular
        {
            get { return _intestabular; }
            set { _intestabular = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        public DateTime? FechaDesde
        {
            get { return _datfechadesde; }
            set { _datfechadesde = value; }
        }


        public DateTime? FechaHasta
        {
            get { return _datfechahasta; }
            set { _datfechahasta = value; }
        }

        public Int16? IdEmpresa { get; set; }
        public Int16? IdSede { get; set; }
        public Int16? IdRubro { get; set; }
        public Int32 IdUsuario { get; set; }
        public String Empresa { get; set; }
        public String Sede { get; set; }
        public String RutaDestino { get; set; }
        public String NombreUsuario { get; set; }
        public Boolean MostrarCabecera { get; set; }
        public String HeaderColor { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsReporte()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsReporte(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idreporte")) IdReporte = dr["idreporte"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idreporte"]);
            if (dr.Table.Columns.Contains("descripcion")) Descripcion = dr["descripcion"] == DBNull.Value ? String.Empty : dr["descripcion"].ToString();
            if (dr.Table.Columns.Contains("sp")) SP = dr["sp"] == DBNull.Value ? String.Empty : dr["sp"].ToString();
            if (dr.Table.Columns.Contains("estabular")) EsTabular = dr["estabular"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["estabular"]);
            if (dr.Table.Columns.Contains("idestado")) IdEstado = dr["idestado"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idestado"]);
            if (dr.Table.Columns.Contains("FechaDesde")) FechaDesde = dr["FechaDesde"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["FechaDesde"]);
            if (dr.Table.Columns.Contains("FechaHasta")) FechaHasta = dr["FechaHasta"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["FechaHasta"]);
        }

        #endregion

    }

    /// <summary>
    /// Entidad Lista clsListaReporte
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaReporte
    {
        #region Campos

        private List<clsReporte> _objelementos = new List<clsReporte>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsReporte> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaReporte()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaReporte(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new clsReporte(drw));
            }
        }

        #endregion

    }

    /// <summary>
    /// Entidad clsReporteCredito
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.sistema.com.pe/sistema/serviciosweb")]
    public class clsReporteCredito
    {
        #region Campos

        private Int32? _intidmovimiento;
        private Int32? _intidcliente;
        private Int32? _intidpago;
        private String _strnroidentidad;
        private String _strnombres;
        private String _strdocumento;
        private String _strnrodocumento;
        private DateTime? _datfechamovimiento;
        private DateTime? _datfechavencimiento;
        private Decimal? _dectotal;
        private Decimal? _dectotalcredito;
        private Decimal? _decpagorecibido;
        private Decimal? _decpagodeuda;
        private DateTime? _datfechapago;
        private Decimal? _decmonto;
        private Decimal? _decclientedeuda;
        private Decimal? _dectotaldeuda;
        private String _strtipopago;

        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int32? IdMovimiento
        {
            get { return _intidmovimiento; }
            set { _intidmovimiento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32? IdCliente
        {
            get { return _intidcliente; }
            set { _intidcliente = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32? IdPago
        {
            get { return _intidpago; }
            set { _intidpago = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NroIdentidad
        {
            get { return _strnroidentidad; }
            set { _strnroidentidad = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Nombres
        {
            get { return _strnombres; }
            set { _strnombres = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Documento
        {
            get { return _strdocumento; }
            set { _strdocumento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? FechaMovimiento
        {
            get { return _datfechamovimiento; }
            set { _datfechamovimiento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? FechaVencimiento
        {
            get { return _datfechavencimiento; }
            set { _datfechavencimiento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? Total
        {
            get { return _dectotal; }
            set { _dectotal = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? TotalCredito
        {
            get { return _dectotalcredito; }
            set { _dectotalcredito = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? PagoRecibido
        {
            get { return _decpagorecibido; }
            set { _decpagorecibido = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? PagoDeuda
        {
            get { return _decpagodeuda; }
            set { _decpagodeuda = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? FechaPago
        {
            get { return _datfechapago; }
            set { _datfechapago = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? Monto
        {
            get { return _decmonto; }
            set { _decmonto = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? ClienteDeuda
        {
            get { return _decclientedeuda; }
            set { _decclientedeuda = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? TotalDeuda
        {
            get { return _dectotaldeuda; }
            set { _dectotaldeuda = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String TipoPago
        {
            get { return _strtipopago; }
            set { _strtipopago = value; }
        }

        public String Direccion { get; set; }
        public String Vencido { get; set; }
        public Decimal? ClienteCredito { get; set; }
        public Decimal? ClienteRecibido { get; set; }
        public Int16 ClienteVencido { get; set; }
        public Decimal? TotalTotalCredito { get; set; }
        public Decimal? TotalRecibido { get; set; }
        public Int16 TotalVencido { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsReporteCredito()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsReporteCredito(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idmovimiento")) IdMovimiento = dr["idmovimiento"] == DBNull.Value ? (Int32?)null : Convert.ToInt32(dr["idmovimiento"]);
            if (dr.Table.Columns.Contains("idcliente")) IdCliente = dr["idcliente"] == DBNull.Value ? (Int32?)null : Convert.ToInt32(dr["idcliente"]);
            if (dr.Table.Columns.Contains("idpago")) IdPago = dr["idpago"] == DBNull.Value ? (Int32?)null : Convert.ToInt32(dr["idpago"]);
            if (dr.Table.Columns.Contains("nroidentidad")) NroIdentidad = dr["nroidentidad"] == DBNull.Value ? String.Empty : dr["nroidentidad"].ToString();
            if (dr.Table.Columns.Contains("nombres")) Nombres = dr["nombres"] == DBNull.Value ? String.Empty : dr["nombres"].ToString();
            if (dr.Table.Columns.Contains("documento")) Documento = dr["documento"] == DBNull.Value ? String.Empty : dr["documento"].ToString();
            if (dr.Table.Columns.Contains("nrodocumento")) NroDocumento = dr["nrodocumento"] == DBNull.Value ? String.Empty : dr["nrodocumento"].ToString();
            if (dr.Table.Columns.Contains("fechamovimiento")) FechaMovimiento = dr["fechamovimiento"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["fechamovimiento"]);
            if (dr.Table.Columns.Contains("fechavencimiento")) FechaVencimiento = dr["fechavencimiento"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["fechavencimiento"]);
            if (dr.Table.Columns.Contains("total")) Total = dr["total"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["total"]);
            if (dr.Table.Columns.Contains("totalcredito")) TotalCredito = dr["totalcredito"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["totalcredito"]);
            if (dr.Table.Columns.Contains("pagorecibido")) PagoRecibido = dr["pagorecibido"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["pagorecibido"]);
            if (dr.Table.Columns.Contains("pagodeuda")) PagoDeuda = dr["pagodeuda"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["pagodeuda"]);
            if (dr.Table.Columns.Contains("fechapago")) FechaPago = dr["fechapago"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["fechapago"]);
            if (dr.Table.Columns.Contains("monto")) Monto = dr["monto"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["monto"]);
            if (dr.Table.Columns.Contains("ClienteDeuda")) ClienteDeuda = dr["ClienteDeuda"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["ClienteDeuda"]);
            if (dr.Table.Columns.Contains("TotalDeuda")) TotalDeuda = dr["TotalDeuda"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["TotalDeuda"]);
            if (dr.Table.Columns.Contains("TipoPago")) TipoPago = dr["TipoPago"] == DBNull.Value ? String.Empty : dr["TipoPago"].ToString();
        }

        #endregion

    }

    /// <summary>
    /// Entidad Lista clsListaReporteCredito
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaReporteCredito
    {
        #region Campos

        private List<clsReporteCredito> _objelementos = new List<clsReporteCredito>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsReporteCredito> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaReporteCredito()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaReporteCredito(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new clsReporteCredito(drw));
            }
        }

        #endregion

    }





}
