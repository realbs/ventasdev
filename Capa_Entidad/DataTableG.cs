﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Capa_Entidad
{
    [Serializable()]
    public class DataTableG
    {
        public String Nombre { get; set; }

        /// <summary>
        /// Lista de columnas que contiene el conjunto Entidad.
        /// </summary>
        public Columnas Columnas { get; set; }

        /// <summary>
        /// Lista de filas que contiene el conjunto de Entidad.
        /// </summary>
        public Filas Filas { get; set; }

        /// <summary>
        /// Constructor predeterminado.
        /// </summary>
        public DataTableG()
        {
            Establecer("entidad");
        }

        public DataTableG(string nombreEntidad)
        {
            Establecer(nombreEntidad);
        }

        private void Establecer(string nombreEntidad)
        {
            Nombre = nombreEntidad;
            Columnas = new Columnas();
            Filas = new Filas();
            Filas.Parent = this;
        }


        /// <summary>
        /// Convierte el objeto en Byte[]
        /// </summary>
        /// <returns></returns>
        public byte[] ConvertToByte()
        {
            MemoryStream memorystream = new MemoryStream();
            BinaryFormatter bf = new BinaryFormatter();

            bf.Serialize(memorystream, this);

            return memorystream.ToArray();
        }

        /// <summary>
        /// Convierte el objeto a DataTable.
        /// </summary>
        /// <returns></returns>
        public System.Data.DataTable ConvertToDataTable()
        {
            return ConvertToDataTable("dtInfo");
        }

        /// <summary>
        /// Convierte el objeto a DataTable.
        /// </summary>
        /// <param name="nombreDataTable"></param>
        /// <returns></returns>
        public System.Data.DataTable ConvertToDataTable(string nombreDataTable)
        {
            System.Data.DataTable dtresultado = new System.Data.DataTable(nombreDataTable);

            CrearColumnasDT(dtresultado);

            PasarInformacion(dtresultado);

            return dtresultado;
        }

        private void PasarInformacion(System.Data.DataTable dt)
        {
            foreach (Fila registro in Filas)
            {
                dt.Rows.Add(registro.ToArray());
            }
        }

        private void CrearColumnasDT(System.Data.DataTable dt)
        {
            foreach (Columna esquema in Columnas)
            {
                System.Data.DataColumn columna = new System.Data.DataColumn();
                columna.ColumnName = esquema.Nombre;
                columna.DataType = esquema.TipoDato;

                dt.Columns.Add(columna);
            }
        }

        public void CargarDataTable(System.Data.DataTable dt)
        {
            Columnas.Clear();
            foreach (System.Data.DataColumn col in dt.Columns)
            {
                Columnas.Add(new Columna() { Nombre = col.ColumnName, TipoDato = col.DataType });
            }

            Fila fila;
            Object[] valores;
            Filas.Clear();
            foreach (System.Data.DataRow row in dt.Rows)
            {
                fila = new Fila();
                valores = new Object[dt.Columns.Count];
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    valores[i] = row[i];
                }
                fila.Add(valores);
                Filas.Add(fila);
            }
        }
    }
}
