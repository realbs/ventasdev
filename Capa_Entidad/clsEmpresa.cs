﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Capa_Entidad
{
    /// <summary>
    /// Entidad clsEmpresa
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.sistema.com.pe/sistema/serviciosweb")]
    public class clsEmpresa
    {
        #region Campos

        private Int16 _intidempresa;
        private String _strdescripcion;
        private String _strtelefono;
        private String _strdireccion;
        private String _strnroidentidad;
        private Int16? _intidtipoidentidad;
        private String _stremail;
        private Int16? _intidestado;
        private String _stroslastlogin;
        private DateTime _datoslastdate;
        private String _stroslastapp;
        private String _strosfirstlogin;
        private DateTime _datosfirstdate;
        private String _strosfirstapp;
        private String _strnombrecomercial;

        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Descripcion
        {
            get { return _strdescripcion; }
            set { _strdescripcion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Telefono
        {
            get { return _strtelefono; }
            set { _strtelefono = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Direccion
        {
            get { return _strdireccion; }
            set { _strdireccion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NroIdentidad
        {
            get { return _strnroidentidad; }
            set { _strnroidentidad = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdTipoIdentidad
        {
            get { return _intidtipoidentidad; }
            set { _intidtipoidentidad = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Email
        {
            get { return _stremail; }
            set { _stremail = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String osLastLogin
        {
            get { return _stroslastlogin; }
            set { _stroslastlogin = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime osLastDate
        {
            get { return _datoslastdate; }
            set { _datoslastdate = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String osLastApp
        {
            get { return _stroslastapp; }
            set { _stroslastapp = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String osFirstLogin
        {
            get { return _strosfirstlogin; }
            set { _strosfirstlogin = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime osFirstDate
        {
            get { return _datosfirstdate; }
            set { _datosfirstdate = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String osFirstApp
        {
            get { return _strosfirstapp; }
            set { _strosfirstapp = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NombreComercial
        {
            get { return _strnombrecomercial; }
            set { _strnombrecomercial = value; }
        }

        public Byte[] Imagen { get; set; }
        public Int32 IdUsuario { get; set; }
        public Int16? IdDepartamento { get; set; }
        public Int16? IdProvincia { get; set; }
        public Int16? IdDistrito { get; set; }
        public String DireccionComplementaria { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsEmpresa()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsEmpresa(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idempresa")) IdEmpresa = dr["idempresa"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["idempresa"]);
            if (dr.Table.Columns.Contains("descripcion")) Descripcion = dr["descripcion"] == DBNull.Value ? String.Empty : dr["descripcion"].ToString();
            if (dr.Table.Columns.Contains("telefono")) Telefono = dr["telefono"] == DBNull.Value ? String.Empty : dr["telefono"].ToString();
            if (dr.Table.Columns.Contains("direccion")) Direccion = dr["direccion"] == DBNull.Value ? String.Empty : dr["direccion"].ToString();
            if (dr.Table.Columns.Contains("nroidentidad")) NroIdentidad = dr["nroidentidad"] == DBNull.Value ? String.Empty : dr["nroidentidad"].ToString();
            if (dr.Table.Columns.Contains("idtipoidentidad")) IdTipoIdentidad = dr["idtipoidentidad"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idtipoidentidad"]);
            if (dr.Table.Columns.Contains("email")) Email = dr["email"] == DBNull.Value ? String.Empty : dr["email"].ToString();
            if (dr.Table.Columns.Contains("idestado")) IdEstado = dr["idestado"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idestado"]);           
            if (dr.Table.Columns.Contains("NombreComercial")) NombreComercial = dr["NombreComercial"] == DBNull.Value ? String.Empty : dr["NombreComercial"].ToString();
            if (dr.Table.Columns.Contains("Imagen")) Imagen = dr["Imagen"] == DBNull.Value ? (Byte[])null : (Byte[])(dr["Imagen"]);
            if (dr.Table.Columns.Contains("IdDepartamento")) IdDepartamento = dr["IdDepartamento"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["IdDepartamento"]);
            if (dr.Table.Columns.Contains("IdProvincia")) IdProvincia = dr["IdProvincia"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["IdProvincia"]);
            if (dr.Table.Columns.Contains("IdDistrito")) IdDistrito = dr["IdDistrito"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["IdDistrito"]);
            if (dr.Table.Columns.Contains("DireccionComplementaria")) DireccionComplementaria = dr["DireccionComplementaria"] == DBNull.Value ? String.Empty : dr["DireccionComplementaria"].ToString();
        }

        #endregion

    }

    /// <summary>
    /// Entidad Lista clsListaEmpresa
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaEmpresa
    {
        #region Campos

        private List<clsEmpresa> _objelementos = new List<clsEmpresa>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsEmpresa> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaEmpresa()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaEmpresa(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new clsEmpresa(drw));
            }
        }

        #endregion

    }


}
