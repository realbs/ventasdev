﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa_Entidad
{

    public enum TipoBusqueda
    {
        Producto = 1,
        Categoria = 2,
        Cliente = 3,
        Proveedor = 4,
    }

    public enum TipoMovimiento
    {
        Compras = 1,
        Ventas = 2,
        Entradas = 3,
        Salidas = 4,
        Cotizaciones = 5,
        Pedidos = 6
    }

    public enum TipoIdentidad
    {
        DNI = 1,
        RUC = 2,
    }

    public enum TipoDocumento
    {
        BOLETA = 1,
        FACTURA = 2,
        TICKET = 3,
        NOTACREDITO = 4,
        NOTADEBITO = 5,
        GUIAREMISIONREMITENTE = 6,
        GUIAREMISIONTRANSPORTISTA = 7,
        COTIZACION = 8,
        PEDIDO = 9,
        RECIBO=10
    }

    public enum Tipo
    {
        PRODUCTO = 1,
        SERVICIO = 2,
    }

    public enum Estado
    {
        ANULADO = 0,
        ACTIVO = 1,
        REGISTRADO = 2,
        ENVIADO = 3,
        DEBAJA = 4,
        ABIERTO = 5,
        CERRADO = 6,
        CONERROR = 7,
        EJECUTADO = 8,
    }

    public enum TipoPago
    {
        EFECTIVO = 1,
        TARJETA = 2,
    }

    public enum Rol
    {
        ADMINISTRADOR = 1,
        JEFE = 2,
        SUPERVISOR = 3,
        COMPRADOR = 4,
        VENDEDOR = 5,
        FACTURADOR = 6,
        ADMINISTRADORB = 7,
        VENDEDORB = 8,
    }

    public enum PosicionControl
    {
        /// <summary>
        /// Posiciona los controles de ayuda en linea con las etiquetas.
        /// </summary>
        EnLinea = 1,

        /// <summary>
        /// Posiciona los controles de ayuda debajo de las etiquetas.
        /// </summary>
        EnParalelo = 2,

        /// <summary>
        /// Posiciona los controles horizontalmente.
        /// </summary>
        Horizontal = 3,
    }

    public enum TipoAtencion
    {
        RECAUDACION = 1,
        ATENCIONCLIENTE = 2,
    }

    public enum Rubro
    {
        MINIMARKET = 1,
        FARMACIA = 2,
        ROPA = 3,
        ARMA = 4,
    }

    public enum TipoImpresion
    {
        Ticket = 1,
        A4 = 2,
        A5 = 3,
    }

    public enum TipoOperacion
    {
        GRAVADO = 1,
        EXONERADO = 2,
        INAFECTO = 3,
        EXPORTACION = 4,
        GRATUITO = 5,
    }

}
