﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Capa_Entidad
{
    /// <summary>
    /// Entidad clsPuntoAtencion
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.sistema.com.pe/sistema/serviciosweb")]
    public class clsPuntoAtencion
    {
        #region Campos

        private Int16 _intidpuntoatencion;
        private String _strdescripcion;
        private Int16? _intidtipoatencion;
        private Int32? _intidusuario;
        private Int16? _intidestado;

        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdPuntoAtencion
        {
            get { return _intidpuntoatencion; }
            set { _intidpuntoatencion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Descripcion
        {
            get { return _strdescripcion; }
            set { _strdescripcion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdTipoAtencion
        {
            get { return _intidtipoatencion; }
            set { _intidtipoatencion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32? IdUsuario
        {
            get { return _intidusuario; }
            set { _intidusuario = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        public Int16 IdEmpresa { get; set; }
        public Int16 IdSede { get; set; }
        public Int32 IdUsuarioPrincipal { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsPuntoAtencion()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsPuntoAtencion(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idpuntoatencion")) IdPuntoAtencion = dr["idpuntoatencion"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["idpuntoatencion"]);
            if (dr.Table.Columns.Contains("descripcion")) Descripcion = dr["descripcion"] == DBNull.Value ? String.Empty : dr["descripcion"].ToString();
            if (dr.Table.Columns.Contains("idtipoatencion")) IdTipoAtencion = dr["idtipoatencion"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idtipoatencion"]);
            if (dr.Table.Columns.Contains("idusuario")) IdUsuario = dr["idusuario"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idusuario"]);
            if (dr.Table.Columns.Contains("idestado")) IdEstado = dr["idestado"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idestado"]);
        }

        #endregion

    }

    /// <summary>
    /// Entidad Lista clsListaPuntoAtencion
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaPuntoAtencion
    {
        #region Campos

        private List<clsPuntoAtencion> _objelementos = new List<clsPuntoAtencion>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsPuntoAtencion> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaPuntoAtencion()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaPuntoAtencion(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new clsPuntoAtencion(drw));
            }
        }

        #endregion

    }


}
