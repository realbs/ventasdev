﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Capa_Entidad
{
    /// <summary>
    /// Entidad clsCaja
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.sistema.com.pe/sistema/serviciosweb")]
    public class clsCaja
    {
        #region Campos

        private Int32 _intidcaja;
        private Int16? _intidtipoatencion;
        private Int16? _intidpuntoatencion;
        private Int32? _intidusuario;
        private Decimal? _decsaldoinicial;
        private DateTime? _datfechapago;
        private DateTime? _datfechaapertura;
        private DateTime? _datfechacierre;
        private DateTime? _datultimomovimiento;
        private Int32? _intnrotransacciones;
        private Decimal? _dectotalrecaudado;
        private Int16? _intidempresa;
        private Int16? _intidsede;
        private Int16? _intidestado;

        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdCaja
        {
            get { return _intidcaja; }
            set { _intidcaja = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdTipoAtencion
        {
            get { return _intidtipoatencion; }
            set { _intidtipoatencion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdPuntoAtencion
        {
            get { return _intidpuntoatencion; }
            set { _intidpuntoatencion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32? IdUsuario
        {
            get { return _intidusuario; }
            set { _intidusuario = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? SaldoInicial
        {
            get { return _decsaldoinicial; }
            set { _decsaldoinicial = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? FechaPago
        {
            get { return _datfechapago; }
            set { _datfechapago = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? FechaApertura
        {
            get { return _datfechaapertura; }
            set { _datfechaapertura = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? FechaCierre
        {
            get { return _datfechacierre; }
            set { _datfechacierre = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UltimoMovimiento
        {
            get { return _datultimomovimiento; }
            set { _datultimomovimiento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32? NroTransacciones
        {
            get { return _intnrotransacciones; }
            set { _intnrotransacciones = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? TotalRecaudado
        {
            get { return _dectotalrecaudado; }
            set { _dectotalrecaudado = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdSede
        {
            get { return _intidsede; }
            set { _intidsede = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        public Int32 IdUsuarioPrincipal { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsCaja()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsCaja(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idcaja")) IdCaja = dr["idcaja"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idcaja"]);
            if (dr.Table.Columns.Contains("idtipoatencion")) IdTipoAtencion = dr["idtipoatencion"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idtipoatencion"]);
            if (dr.Table.Columns.Contains("idpuntoatencion")) IdPuntoAtencion = dr["idpuntoatencion"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idpuntoatencion"]);
            if (dr.Table.Columns.Contains("idusuario")) IdUsuario = dr["idusuario"] == DBNull.Value ? (Int32?)null : Convert.ToInt32(dr["idusuario"]);
            if (dr.Table.Columns.Contains("saldoinicial")) SaldoInicial = dr["saldoinicial"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["saldoinicial"]);
            if (dr.Table.Columns.Contains("fechapago")) FechaPago = dr["fechapago"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["fechapago"]);
            if (dr.Table.Columns.Contains("fechaapertura")) FechaApertura = dr["fechaapertura"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["fechaapertura"]);
            if (dr.Table.Columns.Contains("fechacierre")) FechaCierre = dr["fechacierre"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["fechacierre"]);
            if (dr.Table.Columns.Contains("ultimomovimiento")) UltimoMovimiento = dr["ultimomovimiento"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["ultimomovimiento"]);
            if (dr.Table.Columns.Contains("nrotransacciones")) NroTransacciones = dr["nrotransacciones"] == DBNull.Value ? (Int32?)null : Convert.ToInt32(dr["nrotransacciones"]);
            if (dr.Table.Columns.Contains("totalrecaudado")) TotalRecaudado = dr["totalrecaudado"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["totalrecaudado"]);
            if (dr.Table.Columns.Contains("idempresa")) IdEmpresa = dr["idempresa"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idempresa"]);
            if (dr.Table.Columns.Contains("idsede")) IdSede = dr["idsede"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idsede"]);
            if (dr.Table.Columns.Contains("idestado")) IdEstado = dr["idestado"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idestado"]);
        }

        #endregion

    }

    /// <summary>
    /// Entidad Lista clsListaCaja
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaCaja
    {
        #region Campos

        private List<clsCaja> _objelementos = new List<clsCaja>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsCaja> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaCaja()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaCaja(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new clsCaja(drw));
            }
        }

        #endregion

    }


}
