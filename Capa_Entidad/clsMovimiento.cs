﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Capa_Entidad
{
    /// <summary>
    /// Entidad clsMovimiento
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.sistema.com.pe/sistema/serviciosweb")]
    public class clsMovimiento
    {
        #region Campos

        private Int32 _intidmovimiento;
        private DateTime? _datfechamovimiento;
        private Int16? _intidtipomovimiento;
        private String _strguia;
        private Int16? _intidestado;
        private DateTime? _datfechadesde;
        private DateTime? _datfechahasta;
        private Int16? _intiddestino;
        private Int16? _intidmecanico;
        private Decimal? _dechorometro;
        private Int32 _intidusuario;
        private Decimal? _decimporte;
        private Decimal? _decdescuento;
        private Decimal? _decsubtotal;
        private Decimal? _decigv;
        private Decimal? _dectotal;
        private Int16? _intiddocumento;
        private Int16? _intidtipopago;
        private String _strserie;
        private String _strnumero;
        private String _strnrodocumento;
        private Int16? _intidproveedor;
        private Int16? _intidcliente;
        private Int16? _intidsede;
        private Int16? _intsel;
        private String _strestado;
        private String _strdocumento;
        private String _stridmovimientos;
        private Int16 _intidempresa;
        private String _strestadofacturador;
        private Decimal? _decpagorecibido;
        private Decimal? _decpagovuelto;
       
        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdMovimiento
        {
            get { return _intidmovimiento; }
            set { _intidmovimiento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? FechaMovimiento
        {
            get { return _datfechamovimiento; }
            set { _datfechamovimiento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdTipoMovimiento
        {
            get { return _intidtipomovimiento; }
            set { _intidtipomovimiento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdDocumento
        {
            get { return _intiddocumento; }
            set { _intiddocumento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdTipoPago
        {
            get { return _intidtipopago; }
            set { _intidtipopago = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Serie
        {
            get { return _strserie; }
            set { _strserie = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Numero
        {
            get { return _strnumero; }
            set { _strnumero = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? Importe
        {
            get { return _decimporte; }
            set { _decimporte = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? Descuento
        {
            get { return _decdescuento; }
            set { _decdescuento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? SubTotal
        {
            get { return _decsubtotal; }
            set { _decsubtotal = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? Igv
        {
            get { return _decigv; }
            set { _decigv = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? Total
        {
            get { return _dectotal; }
            set { _dectotal = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Guia
        {
            get { return _strguia; }
            set { _strguia = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        public DateTime? FechaDesde
        {
            get { return _datfechadesde; }
            set { _datfechadesde = value; }
        }


        public DateTime? FechaHasta
        {
            get { return _datfechahasta; }
            set { _datfechahasta = value; }
        }

        public Int16? IdDestino
        {
            get { return _intiddestino; }
            set { _intiddestino = value; }
        }

        public Int16? IdMecanico
        {
            get { return _intidmecanico; }
            set { _intidmecanico = value; }
        }

        public Decimal? Horometro
        {
            get { return _dechorometro; }
            set { _dechorometro = value; }
        }

        public Int32 IdUsuario
        {
            get { return _intidusuario; }
            set { _intidusuario = value; }
        }

        public Int16? IdProveedor
        {
            get { return _intidproveedor; }
            set { _intidproveedor = value; }
        }

        public Int16? IdCliente
        {
            get { return _intidcliente; }
            set { _intidcliente = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16? IdSede
        {
            get { return _intidsede; }
            set { _intidsede = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16? Sel
        {
            get { return _intsel; }
            set { _intsel = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Estado
        {
            get { return _strestado; }
            set { _strestado = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Documento
        {
            get { return _strdocumento; }
            set { _strdocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String IdMovimientos
        {
            get { return _stridmovimientos; }
            set { _stridmovimientos = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String EstadoFacturador
        {
            get { return _strestadofacturador; }
            set { _strestadofacturador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal? PagoRecibido
        {
            get { return _decpagorecibido; }
            set { _decpagorecibido = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal? PagoVuelto
        {
            get { return _decpagovuelto; }
            set { _decpagovuelto = value; }
        }

        public Int16 IdPuntoAtencion {get;set;}
        public Int32 IdCaja { get; set; }
        public Int16 EsCredito { get; set; }
        public Decimal? TotalCredito { get; set; }
        public DateTime? FechaVencimiento { get; set; }
        public Decimal? PagoDeuda { get; set; }
        public String Usuario { get; set; }
        public Int16 ConDeuda { get; set; }
        public String Comentario { get; set; }
        public String MensajeRespuesta { get; set; }
        public String MensajeRespuestaBaja { get; set; }
        public String RutaXML { get; set; }
        public String RutaPDF { get; set; }
        public Decimal? DescuentoMonedas { get; set; }
        public Int32 IdUsuarioPrincipal { get; set; }

        public Decimal Gravado { get; set; }
        public Decimal Exonerado { get; set; }
        public Decimal Inafecto { get; set; }
        public Decimal Gratuito { get; set; }
        public Decimal Exportacion { get; set; }

        public Int32 IdMovimientoRelacionado { get; set; }
        public String NroDocumentoRelacionado { get; set; }
        public Int16 IdTipoNota { get; set; }

        public String Cliente { get; set; }
        public String Proveedor { get; set; }
        public Int16 IdDocumentoRelacionado { get; set; }

        public String OrdenCompra { get; set; }
        public String Codigo { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsMovimiento()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsMovimiento(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idmovimiento")) IdMovimiento = dr["idmovimiento"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idmovimiento"]);
            if (dr.Table.Columns.Contains("fechamovimiento")) FechaMovimiento = dr["fechamovimiento"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["fechamovimiento"]);
            if (dr.Table.Columns.Contains("idtipomovimiento")) IdTipoMovimiento = dr["idtipomovimiento"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idtipomovimiento"]);
            if (dr.Table.Columns.Contains("guia")) Guia = dr["guia"] == DBNull.Value ? String.Empty : dr["guia"].ToString();
            if (dr.Table.Columns.Contains("idestado")) IdEstado = dr["idestado"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idestado"]);
            if (dr.Table.Columns.Contains("FechaDesde")) FechaDesde = dr["FechaDesde"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["FechaDesde"]);
            if (dr.Table.Columns.Contains("FechaHasta")) FechaHasta = dr["FechaHasta"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["FechaHasta"]);
            if (dr.Table.Columns.Contains("IdUsuario")) IdUsuario = dr["IdUsuario"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["IdUsuario"]);
            if (dr.Table.Columns.Contains("importe")) Importe = dr["importe"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["importe"]);
            if (dr.Table.Columns.Contains("Descuento")) Descuento = dr["Descuento"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["Descuento"]);
            if (dr.Table.Columns.Contains("SubTotal")) SubTotal = dr["SubTotal"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["SubTotal"]);
            if (dr.Table.Columns.Contains("igv")) Igv = dr["igv"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["igv"]);
            if (dr.Table.Columns.Contains("total")) Total = dr["total"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["total"]);
            if (dr.Table.Columns.Contains("iddocumento")) IdDocumento = dr["iddocumento"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["iddocumento"]);
            if (dr.Table.Columns.Contains("idtipopago")) IdTipoPago = dr["idtipopago"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idtipopago"]);
            if (dr.Table.Columns.Contains("serie")) Serie = dr["serie"] == DBNull.Value ? String.Empty : dr["serie"].ToString();
            if (dr.Table.Columns.Contains("Numero")) Numero = dr["Numero"] == DBNull.Value ? String.Empty : dr["Numero"].ToString();
            if (dr.Table.Columns.Contains("nrodocumento")) NroDocumento = dr["nrodocumento"] == DBNull.Value ? String.Empty : dr["nrodocumento"].ToString();
            if (dr.Table.Columns.Contains("IdProveedor")) IdProveedor = dr["IdProveedor"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["IdProveedor"]);
            if (dr.Table.Columns.Contains("IdCliente")) IdCliente = dr["IdCliente"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["IdCliente"]);
            if (dr.Table.Columns.Contains("IdSede")) IdSede = dr["IdSede"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["IdSede"]);
            if (dr.Table.Columns.Contains("Sel")) Sel = dr["Sel"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["Sel"]);
            if (dr.Table.Columns.Contains("Estado")) Estado = dr["Estado"] == DBNull.Value ? String.Empty : dr["Estado"].ToString();
            if (dr.Table.Columns.Contains("Documento")) Documento = dr["Documento"] == DBNull.Value ? String.Empty : dr["Documento"].ToString();
            if (dr.Table.Columns.Contains("IdMovimientos")) IdMovimientos = dr["IdMovimientos"] == DBNull.Value ? String.Empty : dr["IdMovimientos"].ToString();
            if (dr.Table.Columns.Contains("IdEmpresa")) IdEmpresa = dr["IdEmpresa"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["IdEmpresa"]);
            if (dr.Table.Columns.Contains("EstadoFacturador")) EstadoFacturador = dr["EstadoFacturador"] == DBNull.Value ? String.Empty : dr["EstadoFacturador"].ToString();
            if (dr.Table.Columns.Contains("PagoRecibido")) PagoRecibido = dr["PagoRecibido"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["PagoRecibido"]);
            if (dr.Table.Columns.Contains("PagoVuelto")) PagoVuelto = dr["PagoVuelto"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["PagoVuelto"]);
            if (dr.Table.Columns.Contains("IdPuntoAtencion")) IdPuntoAtencion = dr["IdPuntoAtencion"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["IdPuntoAtencion"]);
            if (dr.Table.Columns.Contains("IdCaja")) IdCaja = dr["IdCaja"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["IdCaja"]);
            if (dr.Table.Columns.Contains("EsCredito")) EsCredito = dr["EsCredito"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["EsCredito"]);
            if (dr.Table.Columns.Contains("TotalCredito")) TotalCredito = dr["TotalCredito"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["TotalCredito"]);
            if (dr.Table.Columns.Contains("PagoDeuda")) PagoDeuda = dr["PagoDeuda"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["PagoDeuda"]);
            if (dr.Table.Columns.Contains("FechaVencimiento")) FechaVencimiento = dr["FechaVencimiento"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["FechaVencimiento"]);
            if (dr.Table.Columns.Contains("Usuario")) Usuario = dr["Usuario"] == DBNull.Value ? String.Empty : dr["Usuario"].ToString();
            if (dr.Table.Columns.Contains("Comentario")) Comentario = dr["Comentario"] == DBNull.Value ? String.Empty : dr["Comentario"].ToString();
            if (dr.Table.Columns.Contains("MensajeRespuesta")) MensajeRespuesta = dr["MensajeRespuesta"] == DBNull.Value ? String.Empty : dr["MensajeRespuesta"].ToString();
            if (dr.Table.Columns.Contains("MensajeRespuestaBaja")) MensajeRespuestaBaja = dr["MensajeRespuestaBaja"] == DBNull.Value ? String.Empty : dr["MensajeRespuestaBaja"].ToString();
            if (dr.Table.Columns.Contains("DescuentoMonedas")) DescuentoMonedas = dr["DescuentoMonedas"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["DescuentoMonedas"]);

            if (dr.Table.Columns.Contains("Gravado")) Gravado = dr["Gravado"] == DBNull.Value ? (Decimal)0 : Convert.ToDecimal(dr["Gravado"]);
            if (dr.Table.Columns.Contains("Exonerado")) Exonerado = dr["Exonerado"] == DBNull.Value ? (Decimal)0 : Convert.ToDecimal(dr["Exonerado"]);
            if (dr.Table.Columns.Contains("Inafecto")) Inafecto = dr["Inafecto"] == DBNull.Value ? (Decimal)0 : Convert.ToDecimal(dr["Inafecto"]);
            if (dr.Table.Columns.Contains("Gratuito")) Gratuito = dr["Gratuito"] == DBNull.Value ? (Decimal)0 : Convert.ToDecimal(dr["Gratuito"]);
            if (dr.Table.Columns.Contains("Exportacion")) Exportacion = dr["Exportacion"] == DBNull.Value ? (Decimal)0 : Convert.ToDecimal(dr["Exportacion"]);

            if (dr.Table.Columns.Contains("IdMovimientoRelacionado")) IdMovimientoRelacionado = dr["IdMovimientoRelacionado"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["IdMovimientoRelacionado"]);
            if (dr.Table.Columns.Contains("NroDocumentoRelacionado")) NroDocumentoRelacionado = dr["NroDocumentoRelacionado"] == DBNull.Value ? String.Empty : dr["NroDocumentoRelacionado"].ToString();
            if (dr.Table.Columns.Contains("IdTipoNota")) IdTipoNota = dr["IdTipoNota"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["IdTipoNota"]);
            if (dr.Table.Columns.Contains("OrdenCompra")) OrdenCompra = dr["OrdenCompra"] == DBNull.Value ? String.Empty : dr["OrdenCompra"].ToString();
            if (dr.Table.Columns.Contains("Codigo")) Codigo = dr["Codigo"] == DBNull.Value ? String.Empty : dr["Codigo"].ToString();
            if (dr.Table.Columns.Contains("IdDocumentoRelacionado")) IdDocumentoRelacionado = dr["IdDocumentoRelacionado"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["IdDocumentoRelacionado"]);
        }

        #endregion

    }

    /// <summary>
    /// Entidad Lista clsListaMovimiento
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaMovimiento
    {
        #region Campos

        private List<clsMovimiento> _objelementos = new List<clsMovimiento>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsMovimiento> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaMovimiento()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaMovimiento(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new clsMovimiento(drw));
            }
        }

        #endregion

    }

    /// <summary>
    /// Entidad clsMovimientoDetalle
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.sistema.com.pe/sistema/serviciosweb")]
    public class clsMovimientoDetalle
    {
        #region Campos

        private Int32 _intidmovimientodetalle;
        private Int32? _intidmovimiento;
        private Int32? _intidproducto;
        private Decimal? _deccantidad;
        private Int16? _intidestado;
        private String _strcodigo;
        private String _strunidadmedida;
        private String _strmarca;
        private String _strdescripcion;
        private Decimal? _decpreciocosto;
        private Decimal? _decprecioventa;
        private Decimal? _decprecio;
        private Decimal? _decimporte;
        private Decimal? _decdescuento;
        private Decimal? _dectotal;
        
        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdMovimientoDetalle
        {
            get { return _intidmovimientodetalle; }
            set { _intidmovimientodetalle = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32? IdMovimiento
        {
            get { return _intidmovimiento; }
            set { _intidmovimiento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32? IdProducto
        {
            get { return _intidproducto; }
            set { _intidproducto = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? Cantidad
        {
            get { return _deccantidad; }
            set { _deccantidad = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        public String UnidadMedida
        {
            get { return _strunidadmedida; }
            set { _strunidadmedida = value; }
        }

        public String Codigo
        {
            get { return _strcodigo; }
            set { _strcodigo = value; }
        }

        public String Marca
        {
            get { return _strmarca; }
            set { _strmarca = value; }
        }

        public String Descripcion
        {
            get { return _strdescripcion; }
            set { _strdescripcion = value; }
        }

        public Decimal? PrecioCosto
        {
            get { return _decpreciocosto; }
            set { _decpreciocosto = value; }
        }

        public Decimal? PrecioVenta
        {
            get { return _decprecioventa; }
            set { _decprecioventa = value; }
        }

        public Decimal? Precio
        {
            get { return _decprecio; }
            set { _decprecio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal? Importe
        {
            get { return _decimporte; }
            set { _decimporte = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? Descuento
        {
            get { return _decdescuento; }
            set { _decdescuento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal? Total
        {
            get { return _dectotal; }
            set { _dectotal = value; }
        }

        public Int16? EsPvv { set; get; }
        public Decimal? Stock { set; get; }
        public Decimal? PrecioMayor { get; set; }
        public Decimal? MinimoStock { set; get; }
        public Int16? IdTipo { set; get; }

        //precios de venta
        public Decimal? PrecioVenta1 { get; set; }
        public Decimal? CantidadMayor1 { get; set; }
        public Decimal? PrecioVenta2 { get; set; }
        public Decimal? CantidadMayor2 { get; set; }
        public Decimal? PrecioVenta3 { get; set; }
        public Decimal? CantidadMayor3 { get; set; }
        public Decimal? PrecioVenta4 { get; set; }
        public Decimal? CantidadMayor4 { get; set; }

        public Int16? IdTipoAfectacion { set; get; }
        public Int16? EsPaquete { get; set; }
        public Int16? EsSerieLote { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsMovimientoDetalle()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsMovimientoDetalle(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idmovimientodetalle")) IdMovimientoDetalle = dr["idmovimientodetalle"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idmovimientodetalle"]);
            if (dr.Table.Columns.Contains("idmovimiento")) IdMovimiento = dr["idmovimiento"] == DBNull.Value ? (Int32?)null : Convert.ToInt32(dr["idmovimiento"]);
            if (dr.Table.Columns.Contains("idproducto")) IdProducto = dr["idproducto"] == DBNull.Value ? (Int32?)null : Convert.ToInt32(dr["idproducto"]);
            if (dr.Table.Columns.Contains("descripcion")) Descripcion = dr["descripcion"] == DBNull.Value ? String.Empty : dr["descripcion"].ToString();
            if (dr.Table.Columns.Contains("cantidad")) Cantidad = dr["cantidad"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["cantidad"]);
            if (dr.Table.Columns.Contains("idestado")) IdEstado = dr["idestado"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idestado"]);
            if (dr.Table.Columns.Contains("codigo")) Codigo = dr["codigo"] == DBNull.Value ? String.Empty : dr["codigo"].ToString();
            if (dr.Table.Columns.Contains("UnidadMedida")) UnidadMedida = dr["UnidadMedida"] == DBNull.Value ? String.Empty : dr["UnidadMedida"].ToString();
            if (dr.Table.Columns.Contains("Marca")) Marca = dr["Marca"] == DBNull.Value ? String.Empty : dr["Marca"].ToString();
            if (dr.Table.Columns.Contains("PrecioCosto")) PrecioCosto = dr["PrecioCosto"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["PrecioCosto"]);
            if (dr.Table.Columns.Contains("PrecioVenta")) PrecioVenta = dr["PrecioVenta"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["PrecioVenta"]);
            if (dr.Table.Columns.Contains("EsPvv")) EsPvv = dr["EsPvv"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["EsPvv"]);
            if (dr.Table.Columns.Contains("Precio")) Precio = dr["Precio"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["Precio"]);
            if (dr.Table.Columns.Contains("importe")) Importe = dr["importe"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["importe"]);
            if (dr.Table.Columns.Contains("descuento")) Descuento = dr["descuento"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["descuento"]);
            if (dr.Table.Columns.Contains("Total")) Total = dr["Total"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["Total"]);
            if (dr.Table.Columns.Contains("Stock")) Stock = dr["Stock"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["Stock"]);
            if (dr.Table.Columns.Contains("IdTipo")) IdTipo = dr["IdTipo"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["IdTipo"]);

            if (dr.Table.Columns.Contains("PrecioVenta1")) PrecioVenta1 = dr["PrecioVenta1"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["PrecioVenta1"]);
            if (dr.Table.Columns.Contains("CantidadMayor1")) CantidadMayor1 = dr["CantidadMayor1"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["CantidadMayor1"]);
            if (dr.Table.Columns.Contains("PrecioVenta2")) PrecioVenta2 = dr["PrecioVenta2"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["PrecioVenta2"]);
            if (dr.Table.Columns.Contains("CantidadMayor2")) CantidadMayor2 = dr["CantidadMayor2"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["CantidadMayor2"]);
            if (dr.Table.Columns.Contains("PrecioVenta3")) PrecioVenta3 = dr["PrecioVenta3"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["PrecioVenta3"]);
            if (dr.Table.Columns.Contains("CantidadMayor3")) CantidadMayor3 = dr["CantidadMayor3"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["CantidadMayor3"]);
            if (dr.Table.Columns.Contains("PrecioVenta4")) PrecioVenta4 = dr["PrecioVenta4"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["PrecioVenta4"]);
            if (dr.Table.Columns.Contains("CantidadMayor4")) CantidadMayor4 = dr["CantidadMayor4"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["CantidadMayor4"]);

            if (dr.Table.Columns.Contains("IdTipoAfectacion")) IdTipoAfectacion = dr["IdTipoAfectacion"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["IdTipoAfectacion"]);
            if (dr.Table.Columns.Contains("EsPaquete")) EsPaquete = dr["EsPaquete"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["EsPaquete"]);
            if (dr.Table.Columns.Contains("EsSerieLote")) EsSerieLote = dr["EsSerieLote"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["EsSerieLote"]);
        }

        #endregion

    }

    /// <summary>
    /// Entidad Lista clsListaMovimientoDetalle
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaMovimientoDetalle
    {
        #region Campos

        private List<clsMovimientoDetalle> _objelementos = new List<clsMovimientoDetalle>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsMovimientoDetalle> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaMovimientoDetalle()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaMovimientoDetalle(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new clsMovimientoDetalle(drw));
            }
        }

        #endregion

    }

}
