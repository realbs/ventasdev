﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Capa_Entidad
{

    [Serializable()]
    public class Fila
    {
        public Filas Parent { get; set; }
        private Object[] listaObjetos;

        public Fila()
        {
            listaObjetos = new Object[0];
        }

        public Object this[int i]
        {
            get { return listaObjetos[i]; }
        }

        public Object this[string nombreColumna]
        {
            get { return listaObjetos[Parent.Parent.Columnas.IndiceColumna(nombreColumna)]; }
        }

        public void Add(object[] item)
        {
            listaObjetos = item;
        }

        public void Clear()
        {
            listaObjetos = new Object[0];
        }

        public int Count
        {
            get { return listaObjetos.Length; }
        }

        public Object[] ToArray()
        {
            return listaObjetos;
        }
    }


    [Serializable()]
    public class Filas : ICollection<Fila>
    {
        public DataTableG Parent { get; set; }
        private List<Fila> listaFila;

        public Filas()
        {
            listaFila = new List<Fila>();
        }

        public Fila this[int i]
        {
            get { return listaFila[i]; }
        }

        #region ICollection<Fila> Members

        public void Add(Fila item)
        {
            item.Parent = this;
            listaFila.Add(item);
        }

        public void Clear()
        {
            listaFila = new List<Fila>();
        }

        public bool Contains(Fila item)
        {
            return listaFila.Contains(item);
        }

        public void CopyTo(Fila[] array, int arrayIndex)
        {
            listaFila.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return listaFila.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Fila item)
        {
            return listaFila.Remove(item);
        }

        #endregion

        #region IEnumerable<Fila> Members

        public IEnumerator<Fila> GetEnumerator()
        {
            return listaFila.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
