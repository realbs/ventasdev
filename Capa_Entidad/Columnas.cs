﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Capa_Entidad
{

    [Serializable()]
    public class Columna
    {
        public string Nombre { get; set; }
        public System.Type TipoDato { get; set; }
    }


    [Serializable()]
    public class Columnas : ICollection<Columna>
    {
        private List<Columna> listaColumnas;

        public Columnas()
        {
            listaColumnas = new List<Columna>();
        }

        /// <summary>
        /// Devuelve el valor de la columna segun indice.
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public Columna this[int i]
        {
            get { return listaColumnas[i]; }
        }

        /// <summary>
        /// Devuelve el valor de la columna segun nombre de columna.
        /// </summary>
        /// <param name="nombreColumna"></param>
        /// <returns></returns>
        public Columna this[string nombreColumna]
        {
            get { return (listaColumnas.Find(delegate(Columna p) { return p.Nombre.ToLower().Equals(nombreColumna.ToLower()); })); }
        }

        /// <summary>
        /// Devuelve el indice de una columna.
        /// </summary>
        /// <param name="nombreColumna"></param>
        /// <returns></returns>
        public int IndiceColumna(string nombreColumna)
        {
            return (listaColumnas.FindIndex(delegate(Columna p) { return p.Nombre.ToLower().Equals(nombreColumna.ToLower()); }));
        }

        #region ICollection<Columna> Members

        public void Add(Columna item)
        {
            listaColumnas.Add(item);
        }

        public void Clear()
        {
            listaColumnas = new List<Columna>();
        }

        public bool Contains(Columna item)
        {
            return listaColumnas.Contains(item);
        }

        public void CopyTo(Columna[] array, int arrayIndex)
        {
            listaColumnas.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return listaColumnas.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Columna item)
        {
            return listaColumnas.Remove(item);
        }

        #endregion

        #region IEnumerable<Columna> Members

        public IEnumerator<Columna> GetEnumerator()
        {
            return listaColumnas.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
