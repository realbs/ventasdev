﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Capa_Entidad
{
    /// <summary>
    /// Entidad clsUsuario
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.sistema.com.pe/sistema/serviciosweb")]
    public class clsUsuario
    {
        #region Campos

        private Int32 _intidusuario;
        private String _strnombreusuario;
        private String _strusuario;
        private String _strcontrasenia;
        private Int16 _intidestado;
        private Int16 _intidrol;
        private Int16 _intidpermiso;
        private String _strcontrasenianueva;
        private Int16 _intidempresa;
        private Int16 _intidsede;
        private Int16 _intidrubro;
   
        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdUsuario
        {
            get { return _intidusuario; }
            set { _intidusuario = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NombreUsuario
        {
            get { return _strnombreusuario; }
            set { _strnombreusuario = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Usuario
        {
            get { return _strusuario; }
            set { _strusuario = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Contrasenia
        {
            get { return _strcontrasenia; }
            set { _strcontrasenia = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String ContraseniaNueva
        {
            get { return _strcontrasenianueva; }
            set { _strcontrasenianueva = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        public Int16 IdRol
        {
            get { return _intidrol; }
            set { _intidrol = value; }
        }

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public Int16 IdPermiso
        {
            get { return _intidpermiso; }
            set { _intidpermiso = value; }
        }

        public Int16 IdSede
        {
            get { return _intidsede; }
            set { _intidsede = value; }
        }

        public Int16 IdRubro
        {
            get { return _intidrubro; }
            set { _intidrubro = value; }
        }

        public String Permiso { get; set; }
        public Int16 EsSuper { get; set; }
        public Int16 Acciones { get; set; }
        public Int16 IdPlan { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsUsuario()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsUsuario(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idusuario")) IdUsuario = dr["idusuario"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idusuario"]);
            if (dr.Table.Columns.Contains("nombreusuario")) NombreUsuario = dr["nombreusuario"] == DBNull.Value ? String.Empty : dr["nombreusuario"].ToString();
            if (dr.Table.Columns.Contains("usuario")) Usuario = dr["usuario"] == DBNull.Value ? String.Empty : dr["usuario"].ToString();
            if (dr.Table.Columns.Contains("Contrasenia")) Contrasenia = dr["Contrasenia"] == DBNull.Value ? String.Empty : dr["Contrasenia"].ToString();
            if (dr.Table.Columns.Contains("idestado")) IdEstado = dr["idestado"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["idestado"]);
            if (dr.Table.Columns.Contains("IdRol")) IdRol = dr["IdRol"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["IdRol"]);
            if (dr.Table.Columns.Contains("IdPermiso")) IdPermiso = dr["IdPermiso"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["IdPermiso"]);
            if (dr.Table.Columns.Contains("ContraseniaNueva")) ContraseniaNueva = dr["ContraseniaNueva"] == DBNull.Value ? String.Empty : dr["ContraseniaNueva"].ToString();
            if (dr.Table.Columns.Contains("IdEmpresa")) IdEmpresa = dr["IdEmpresa"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["IdEmpresa"]);
            if (dr.Table.Columns.Contains("IdSede")) IdSede = dr["IdSede"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["IdSede"]);
            if (dr.Table.Columns.Contains("IdRubro")) IdRubro = dr["IdRubro"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["IdRubro"]);
            if (dr.Table.Columns.Contains("Permiso")) Permiso = dr["Permiso"] == DBNull.Value ? String.Empty : dr["Permiso"].ToString();
            if (dr.Table.Columns.Contains("EsSuper")) EsSuper = dr["EsSuper"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["EsSuper"]);
            if (dr.Table.Columns.Contains("Acciones")) Acciones = dr["Acciones"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["Acciones"]);
            if (dr.Table.Columns.Contains("IdPlan")) IdPlan = dr["IdPlan"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["IdPlan"]);
        }

        #endregion

    }

    /// <summary>
    /// Entidad Lista clsListaUsuario
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaUsuario
    {
        #region Campos

        private List<clsUsuario> _objelementos = new List<clsUsuario>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsUsuario> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaUsuario()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaUsuario(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new clsUsuario(drw));
            }
        }

        #endregion

    }


}
