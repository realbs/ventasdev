﻿using Capa_Entidad;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Capa_Datos
{
    public partial class clsGestionDato
    {
        #region Campos estaticos y privados

        private static Hashtable _httablacomando = new Hashtable();
        private static SqlConnection _objconexion;
        private static readonly clsGestionDato _objgestiondato = new clsGestionDato();
        private static Int32 _inttiempofuerapredeterminado = 30;
        private static String claveConexionDB = string.Empty, cadenaconexion = string.Empty;

        #endregion

        #region Obtener Instancia

        /// <summary>
        /// Método que obtiene una instancia unica.
        /// </summary>
        /// <returns></returns>
        public static clsGestionDato ObtenerInstancia()
        {
            return _objgestiondato;
        }

        /// <summary>
        /// Propiedad que obtiene una instancia única.
        /// </summary>
        public static clsGestionDato Instancia
        {
            get
            {
                return _objgestiondato;
            }
        }

        private clsGestionDato()
        {
            if (ConfigurationManager.AppSettings["tiempoEsperaConexionDB"] != null)
                _inttiempofuerapredeterminado = Convert.ToInt32(ConfigurationManager.AppSettings["tiempoEsperaConexionDB"].ToString());
            else
                _inttiempofuerapredeterminado = 30;
        }

        #endregion

        #region manejador de conexion

        /// <summary>
        /// Crea un objeto conexion.
        /// </summary>
        /// <returns></returns>
        private SqlConnection PreparaConexion()
        {
            //if (_objconexion == null)
            _objconexion = ObtenerConexion();

            return _objconexion;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public SqlConnection ObtenerConexion()
        {
            return ObtenerConexion(_inttiempofuerapredeterminado);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public SqlConnection ObtenerConexion(Int32 tiempo)
        {
            SqlConnection cn = new SqlConnection();

            try
            {
                cn = new SqlConnection(ObtenerCadenaConexion(tiempo));
                //cn.InfoMessage += new SqlInfoMessageEventHandler(conexion_InfoMessage);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return cn;
        }

        /// <summary>
        /// controla los eventos de la conexión.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void conexion_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            //ExcepcionNG.Guardar(e.Errors, enumTipoExcepcion.Informativo);
        }

        /// <summary>
        /// Obtiene la cadena de conexión a la DB.
        /// </summary>
        /// <returns></returns>
        public static String ObtenerCadenaConexion(int tiempo)
        {
            //if (string.IsNullOrEmpty(cadenaconexion))
            //{
            //    if (string.IsNullOrEmpty(claveConexionDB))
            //        claveConexionDB = ConfigurationManager.AppSettings["claveConexionDB"].ToString();

            //    cadenaconexion = ConfigurationManager.ConnectionStrings[claveConexionDB].ConnectionString;


            if (string.IsNullOrEmpty(cadenaconexion))
            {
                if (string.IsNullOrEmpty(claveConexionDB))
                    claveConexionDB = ConfigurationManager.AppSettings["claveConexionDB"].ToString();

                cadenaconexion = ConfigurationManager.ConnectionStrings[claveConexionDB].ConnectionString;

                //verificar si la cadena es encriptada
                string encriptado = ConfigurationManager.AppSettings["e"].ToString();
                if (encriptado.Equals("1"))
                {
                    cadenaconexion = DesEncriptar(cadenaconexion);
                }
                //
            }           

            return cadenaconexion;
        }

        #endregion

        #region manejador de transacciones

        [MethodImpl(MethodImplOptions.Synchronized)]
        public SqlTransaction ObtenerTransaccion(SqlConnection conexion)
        {
            return conexion.BeginTransaction();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void ConfirmarTransaccion(SqlTransaction transaccion)
        {
            transaccion.Commit();

            if (transaccion.Connection != null && transaccion.Connection.State == ConnectionState.Open)
                transaccion.Connection.Close();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void CancelarTransaccion(SqlTransaction transaccion)
        {
            transaccion.Rollback();

            if (transaccion.Connection != null && transaccion.Connection.State == ConnectionState.Open)
                transaccion.Connection.Close();
        }

        #endregion

        #region Métodos privados

        [MethodImpl(MethodImplOptions.Synchronized)]
        private Boolean CargarValoresParametros(SqlCommand comando, Hashtable parametros)
        {
            if (comando == null) { return false; }
            if (comando.Parameters.Count == 0) { return true; }

            foreach (SqlParameter param in comando.Parameters)
            {
                if (param.Direction == ParameterDirection.ReturnValue) { continue; }

                if (!parametros.Contains(param.ParameterName.ToLower()))
                {
                    if (param.IsNullable) { continue; }
                    StringBuilder _sbmensaje = new StringBuilder();
                    _sbmensaje.AppendLine("Se ha modificado el esquema de la entidad a ejecutar");
                    _sbmensaje.AppendLine("");
                    _sbmensaje.AppendLine("Objeto modificado: " + comando.CommandText);
                    _sbmensaje.AppendLine("Parametro en Caché: " + param.ParameterName.ToLower());

                    throw new Exception(_sbmensaje.ToString());
                }

                param.Value = parametros[param.ParameterName.ToLower()];
            }

            return true;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private Boolean RecuperarValoresParametrosSalida(SqlCommand comando, Hashtable parametros)
        {
            if (parametros == null)
            {
                return true;
            }
            if (comando == null) { return false; }
            if (comando.Parameters.Count == 0) { return true; }

            //foreach (SqlParameter param in comando.Parameters)

            foreach (SqlParameter param in comando.Parameters)
            {
                if (param.Direction == ParameterDirection.Input) { continue; }
                if (!comando.Parameters.Contains(param.ParameterName.ToLower()))
                {
                    if (param.IsNullable) { continue; }
                    if (param.Direction != ParameterDirection.ReturnValue)
                        throw new Exception("Se ha modificado el esquema de la entidad ejecutada");
                }

                parametros[param.ParameterName.ToLower()] = param.Value;

            }

            return true;

        }

        /// <summary>
        /// Abre un archivo XLS y devuelve un DataTable.
        /// </summary>JLMF
        /// <param name="rutaarchivoXLS"></param>
        /// <param name="hoja"></param>
        /// <returns></returns>
        public DataTable AbrirXLS(String rutaarchivoXLS, String hoja)
        {
            OleDbDataAdapter _objcnn;
            OleDbConnection _objcmd;
            String _strcadena;
            DataTable _dt;

            _strcadena = "Provider = Microsoft.Jet.Oledb.4.0; data source= " + rutaarchivoXLS + @";Extended properties =""Excel 8.0;HDR=Yes;IMEX=1""";

            _objcmd = new OleDbConnection(_strcadena);

            _objcmd.Open();

            _objcnn = new OleDbDataAdapter("Select * from [" + hoja + "$]", _objcmd);

            _dt = new DataTable();

            try
            {
                _objcnn.Fill(_dt);

                _objcmd.Close();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _dt;
        }
        /// <summary>
        /// Actualiza una celda especifica o un rango de celdas en un Archivo Excel.
        /// </summary>JLMF
        /// <param name="rutaarchivoXLS"></param>
        /// <param name="hoja"></param>
        /// <param name="fila"></param>
        /// <param name="columna"></param>
        /// <param name="valor"></param>
        public void UpdateColumnaXLS(String rutaarchivoXLS, String hoja, String fila, String columna, Int16 valor)
        {
            OleDbConnection _objcmd;
            OleDbCommand _objcmm;
            String _strcadena;

            _strcadena = "Provider = Microsoft.Jet.Oledb.4.0; data source= " + rutaarchivoXLS + @";Extended properties =""Excel 8.0;HDR=No""";

            _objcmd = new OleDbConnection(_strcadena);

            _objcmd.Open();

            _objcmm = new OleDbCommand();

            _objcmm.Connection = _objcmd;

            // Ejecutar la sentencia de actualizacion

            _objcmm.CommandText = "UPDATE [" + hoja + "$" + fila + ":" + columna + "] SET F1=" + valor;

            _objcmm.ExecuteNonQuery();

            // Cerrando la conexion

            _objcmm.Connection.Close();

            _objcmd.Close();
        }

        #endregion

        #region Funcionalidad publica

        #region EjecutarComandoEscalar

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Object EjecutarComandoEscalar(String entidad
                                                   , String claveprocedimiento
                                                   , Hashtable parametros)
        {
            return EjecutarComandoEscalar(entidad, claveprocedimiento, parametros, PreparaConexion());
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Object EjecutarComandoEscalar(String entidad
                                                   , String claveprocedimiento
                                                   , Hashtable parametros
                                                   , String cadenaconexion)
        {
            SqlConnection sqlCon = null;

            if (!String.IsNullOrEmpty(cadenaconexion))
            {
                sqlCon = new SqlConnection(cadenaconexion);
            }

            return EjecutarComandoEscalar
                (entidad
                , claveprocedimiento
                , parametros
                , sqlCon);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Object EjecutarComandoEscalar(String entidad
                                                   , String claveprocedimiento
                                                   , Hashtable parametros
                                                   , SqlConnection objconexion)
        {
            Object _objvalor;
            SqlCommand _objcomandotmp;
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            if (objconexion == null) objconexion = PreparaConexion();

            _objcomandotmp = ObtenerObjetoSQLCommand(entidad, claveprocedimiento, parametros, objconexion);

            if (_objcomandotmp == null) { return (Object)null; }

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if (!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif
                _objcomandotmp.Connection.Open();
            }

            _objvalor = _objcomandotmp.ExecuteScalar();

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                //--Desactivar la impersonalización
                if (impersonationContext != null)
                { impersonationContext.Undo(); }
            }

            if (parametros != null && !RecuperarValoresParametrosSalida(_objcomandotmp, parametros)) { return -1; }

            return _objvalor;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Object EjecutarComandoEscalar(SqlTransaction osqltran
                                                   , String entidad
                                                   , String claveprocedimiento
                                                   , Hashtable parametros)
        {
            Object _objvalor;
            SqlCommand _objcomandotmp;
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            _objcomandotmp = ObtenerObjetoSQLCommand(entidad, claveprocedimiento, parametros, osqltran);
            _objcomandotmp.Transaction = osqltran;

            if (_objcomandotmp == null) { return (Object)null; }

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if (!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif
                _objcomandotmp.Connection.Open();
            }

            _objvalor = _objcomandotmp.ExecuteScalar();

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                //--Desactivar la impersonalización
                if (impersonationContext != null)
                { impersonationContext.Undo(); }
            }

            if (parametros != null && !RecuperarValoresParametrosSalida(_objcomandotmp, parametros)) { return -1; }

            return _objvalor;

        }

        #endregion

        #region EjecutarComandoRegistro

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataRow EjecutarComandoRegistro(String entidad, String claveprocedimiento)
        {
            Hashtable _htparametros = new Hashtable();
            return EjecutarComandoRegistro(entidad, claveprocedimiento, _htparametros);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataRow EjecutarComandoRegistro(String entidad
                                                     , String claveprocedimiento
                                                     , Hashtable parametros)
        {
            return EjecutarComandoRegistro(entidad, claveprocedimiento, parametros, PreparaConexion());
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataRow EjecutarComandoRegistro(String entidad
                                                     , String claveprocedimiento
                                                     , Hashtable parametros
                                                     , String cadenaconexion)
        {
            SqlConnection sqlcon;

            if (String.IsNullOrEmpty(cadenaconexion))
            {
                sqlcon = PreparaConexion();
            }
            else
            {
                sqlcon = new SqlConnection(cadenaconexion);
            }

            return EjecutarComandoRegistro(entidad, claveprocedimiento, parametros, sqlcon);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataRow EjecutarComandoRegistro(String entidad
                                                               , String claveprocedimiento
                                                               , Hashtable parametros
                                                               , SqlConnection sqlconn)
        {
            DataTable _objtabladato;
            SqlCommand _objcomandotmp;
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            if (sqlconn == null) sqlconn = PreparaConexion();

            _objcomandotmp = ObtenerObjetoSQLCommand(entidad, claveprocedimiento, parametros, sqlconn);

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if (!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif
                _objcomandotmp.Connection.Open();
            }

            _objtabladato = new DataTable();
            _objtabladato.TableName = entidad.ToLower();

            using (SqlDataAdapter odataadapter = new SqlDataAdapter(_objcomandotmp))
                odataadapter.Fill(_objtabladato);

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                //--Desactivar la impersonalización
                if (impersonationContext != null)
                { impersonationContext.Undo(); }
            }

            RecuperarValoresParametrosSalida(_objcomandotmp, parametros);

            if (_objtabladato.Rows.Count == 0) { return null; }

            return _objtabladato.Rows[0];
        }

        #endregion

        #region EjecutarComandoEntidad

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataTable EjecutarComandoEntidad(String entidad
                                                               , String claveprocedimiento
                                                               , Hashtable parametros)
        {
            return EjecutarComandoEntidad(entidad, claveprocedimiento, parametros, String.Empty);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataTable EjecutarComandoEntidad(String entidad
                                                               , String claveprocedimiento
                                                               , Hashtable parametros
                                                               , Int32 tiempo)
        {
            return EjecutarComandoEntidad(entidad, claveprocedimiento, parametros, String.Empty, tiempo);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataTable EjecutarComandoEntidad(String entidad
                                                               , String claveprocedimiento
                                                               , Hashtable parametros
                                                               , String cadenaconexion)
        {
            return EjecutarComandoEntidad(entidad, claveprocedimiento, parametros, cadenaconexion, _inttiempofuerapredeterminado);

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataTable EjecutarComandoEntidad(String entidad
                                                               , String claveprocedimiento
                                                               , Hashtable parametros
                                                               , SqlConnection conexion)
        {
            return EjecutarComandoEntidad(entidad, claveprocedimiento, parametros, _inttiempofuerapredeterminado, conexion);

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataTable EjecutarComandoEntidad(String entidad
                                                               , String claveprocedimiento
                                                               , Hashtable parametros
                                                               , String cadenaconexion
                                                               , Int32 tiempo)
        {
            SqlConnection sqlcon = new SqlConnection();

            if (!String.IsNullOrEmpty(cadenaconexion))
                sqlcon = new SqlConnection(cadenaconexion);
            else
            {
                sqlcon = PreparaConexion();
            }

            return EjecutarComandoEntidad
                (entidad
                , claveprocedimiento
                , parametros
                , tiempo
                , sqlcon);

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataTable EjecutarComandoEntidad(String entidad
                                                               , String claveprocedimiento
                                                               , Hashtable parametros
                                                               , Int32 tiempo
                                                               , SqlConnection sqlconn)
        {
            DataTable _objtabladato;
            SqlCommand _objcomandotmp;
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            if (sqlconn == null) sqlconn = PreparaConexion();

            _objcomandotmp = ObtenerObjetoSQLCommand(entidad, claveprocedimiento, parametros, sqlconn);

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if (!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif
                _objcomandotmp.Connection.Open();
            }

            _objtabladato = new DataTable();
            _objtabladato.TableName = entidad.ToLower();

            using (SqlDataAdapter odataadapter = new SqlDataAdapter(_objcomandotmp))
                odataadapter.Fill(_objtabladato);

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                //--Desactivar la impersonalización
                if (impersonationContext != null)
                { impersonationContext.Undo(); }
            }

            RecuperarValoresParametrosSalida(_objcomandotmp, parametros);

            return _objtabladato;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataTable EjecutarComandoEntidad(SqlTransaction sqltransaction, String entidad, String claveprocedimiento)
        {
            Hashtable _htfiltro = new Hashtable();

            return EjecutarComandoEntidad(sqltransaction, entidad, claveprocedimiento, _htfiltro);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataTable EjecutarComandoEntidad(String entidad, String claveprocedimiento)
        {
            Hashtable _htfiltro = new Hashtable();

            return EjecutarComandoEntidad(entidad, claveprocedimiento, _htfiltro);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataTable EjecutarComandoEntidad(SqlTransaction sqltransaction, String entidad, String claveprocedimiento, Hashtable parametros)
        {
            DataTable _objtabladato;
            SqlCommand _objcomandotmp;
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            _objcomandotmp = ObtenerObjetoSQLCommand(entidad, claveprocedimiento, parametros, sqltransaction);
            _objcomandotmp.Transaction = sqltransaction;

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if (!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif
                _objcomandotmp.Connection.Open();
            }

            _objtabladato = new DataTable();
            _objtabladato.TableName = entidad.ToLower();

            using (SqlDataAdapter osqldataadapter = new SqlDataAdapter(_objcomandotmp))
                osqldataadapter.Fill(_objtabladato);

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                //--Desactivar la impersonalización
                if (impersonationContext != null)
                { impersonationContext.Undo(); }
            }

            RecuperarValoresParametrosSalida(_objcomandotmp, parametros);

            return _objtabladato;
        }

        //[MethodImpl(MethodImplOptions.Synchronized)]
        //public virtual OptimusNG.Entidades.Data.DataTableNGC EjecutarComandoEntidadNGC(String entidad, String claveprocedimiento, Hashtable parametros)
        //{
        //    OptimusNG.Entidades.Data.DataTableNGC _objtabladato;
        //    Fila fila;
        //    Object[] valores;
        //    SqlCommand _objcomandotmp;
        //    Boolean _bolconexionactiva = true;
        //    WindowsImpersonationContext impersonationContext = null;

        //    _objcomandotmp = ObtenerObjetoSQLCommand(entidad, claveprocedimiento, parametros, (SqlConnection)null);

        //    if (_objcomandotmp.Connection.State == ConnectionState.Closed)
        //    {
        //        _bolconexionactiva = false;

        //        //--Activar la impersonalización
        //        //#if (!DEBUG)
        //        if (WindowsIdentity.GetCurrent(true) != null)
        //            impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
        //        //#endif
        //        _objcomandotmp.Connection.Open();
        //    }

        //    _objtabladato = new OptimusNG.Entidades.Data.DataTableNGC();
        //    _objtabladato.Nombre = entidad.ToLower();

        //    using (SqlDataReader reader = _objcomandotmp.ExecuteReader())
        //    {
        //        for (int i = 0; i < reader.FieldCount; i++)
        //        {
        //            _objtabladato.Columnas.Add(new Columna() { Nombre = reader.GetName(i), TipoDato = reader.GetFieldType(i) });
        //        }

        //        while (reader.Read())
        //        {
        //            fila = new Fila();
        //            valores = new Object[reader.FieldCount];

        //            reader.GetValues(valores);

        //            fila.Add(valores);
        //            _objtabladato.Filas.Add(fila);
        //        }
        //    }

        //    if (!_bolconexionactiva)
        //    {
        //        _objcomandotmp.Connection.Close();
        //        _objcomandotmp.Connection = null;

        //        //--Desactivar la impersonalización
        //        if (impersonationContext != null)
        //        { impersonationContext.Undo(); }
        //    }

        //    RecuperarValoresParametrosSalida(_objcomandotmp, parametros);

        //    return _objtabladato;
        //}


        #endregion

        #region EjecutarComandoConjuntoEntidad

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entidad"></param>
        /// <param name="claveprocedimiento"></param>
        /// <param name="parametros"></param>
        /// <param name="nombreconjuntoentidad"></param>
        /// <param name="listanombretablas"></param>
        /// <returns></returns>
        public virtual DataSet EjecutarComandoConjuntoEntidad(String entidad
                            , String claveprocedimiento
                            , Hashtable parametros
                            , String nombreconjuntoentidad
                            , params String[] listanombretablas)
        {
            return EjecutarComandoConjuntoEntidad(entidad
                            , claveprocedimiento
                            , nombreconjuntoentidad
                            , String.Empty
                            , parametros
                            , listanombretablas);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entidad"></param>
        /// <param name="claveprocedimiento"></param>
        /// <param name="parametros"></param>
        /// <param name="nombreconjuntoentidad"></param>
        /// <param name="listanombretablas"></param>
        /// <returns></returns>
        /// 
        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataSet EjecutarComandoConjuntoEntidad(String entidad
                                    , String claveprocedimiento
                                    , String nombreconjuntoentidad
                                    , String cadenaconexion
                                    , Hashtable parametros
                                    , params String[] listanombretablas)
        {
            SqlConnection sqlCon = null;

            if (String.IsNullOrEmpty(cadenaconexion))
            {
                sqlCon = PreparaConexion();
            }
            else
            {
                sqlCon = new SqlConnection(cadenaconexion);
            }

            return EjecutarComandoConjuntoEntidad
                (entidad
            , claveprocedimiento
            , nombreconjuntoentidad
            , sqlCon
            , parametros
            , listanombretablas);
        }

        /// <summary>
        /// Ejecutar con objeto Conexión.
        /// </summary>
        /// <param name="entidad"></param>
        /// <param name="claveprocedimiento"></param>
        /// <param name="nombreconjuntoentidad"></param>
        /// <param name="cadenaconexion"></param>
        /// <param name="parametros"></param>
        /// <param name="listanombretablas"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataSet EjecutarComandoConjuntoEntidad(String entidad
                                    , String claveprocedimiento
                                    , String nombreconjuntoentidad
                                    , SqlConnection conexion
                                    , Hashtable parametros
                                    , params String[] listanombretablas)
        {

            SqlCommand _objcomandotmp;
            DataSet _dsresultado;
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            if (conexion == null) conexion = PreparaConexion();

            _objcomandotmp = ObtenerObjetoSQLCommand(entidad, claveprocedimiento, parametros, conexion);

            if (_objcomandotmp == null) { return (DataSet)null; }

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if (!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif

                _objcomandotmp.Connection.Open();
            }

            _dsresultado = new DataSet(nombreconjuntoentidad.ToLower());

            _dsresultado.Load(_objcomandotmp.ExecuteReader(), LoadOption.OverwriteChanges, listanombretablas);

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                //--Desactivar la impersonalización
                if (impersonationContext != null)
                { impersonationContext.Undo(); }
            }

            if (!RecuperarValoresParametrosSalida(_objcomandotmp, parametros)) { return new DataSet(); }

            return _dsresultado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entidad"></param>
        /// <param name="claveprocedimiento"></param>
        /// <param name="parametros"></param>
        /// <param name="nombreconjuntoentidad"></param>
        /// <param name="listanombretablas"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataSet EjecutarComandoConjuntoEntidad(SqlTransaction sqltransaction
                                    , String entidad
                                    , String claveprocedimiento
                                    , Hashtable parametros
                                    , String nombreconjuntoentidad
                                    , params String[] listanombretablas)
        {
            SqlCommand _objcomandotmp;
            DataSet _dsresultado;
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            _objcomandotmp = ObtenerObjetoSQLCommand(entidad, claveprocedimiento, parametros, sqltransaction);
            _objcomandotmp.Transaction = sqltransaction;
            _objcomandotmp.Connection = sqltransaction.Connection;

            if (_objcomandotmp == null) { return (DataSet)null; }

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if (!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif

                _objcomandotmp.Connection.Open();
            }

            _dsresultado = new DataSet(nombreconjuntoentidad.ToLower());

            _dsresultado.Load(_objcomandotmp.ExecuteReader(), LoadOption.OverwriteChanges, listanombretablas);

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                //--Desactivar la impersonalización
                if (impersonationContext != null)
                { impersonationContext.Undo(); }
            }

            if (!RecuperarValoresParametrosSalida(_objcomandotmp, parametros)) { return new DataSet(); }

            return _dsresultado;
        }

        #endregion

        #region EjecutarEscalar

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Object EjecutarEscalar(String sql)
        {
            return EjecutarEscalar(sql, String.Empty);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Object EjecutarEscalar(String sql, String cadenaconexion)
        {
            Object _objresultado;
            SqlCommand _objcomandotmpescalar = new SqlCommand(sql);
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            //if (_objcomandotmpescalar.Connection == null)
            if (String.IsNullOrEmpty(cadenaconexion))
            {
                _objcomandotmpescalar.Connection = PreparaConexion();
            }
            else
            {
                _objcomandotmpescalar.Connection = new SqlConnection(cadenaconexion);
            }

            if (_objcomandotmpescalar.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if (!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif

                _objcomandotmpescalar.Connection.Open();
            }

            _objresultado = _objcomandotmpescalar.ExecuteScalar();

            if (!_bolconexionactiva)
            {
                _objcomandotmpescalar.Connection.Close();
                _objcomandotmpescalar.Connection = null;

                //--Desactivar la impersonalización
                if (impersonationContext != null)
                { impersonationContext.Undo(); }
            }

            return _objresultado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comandotextosql"></param>
        /// <returns></returns>
        public virtual Object EjecutarEscalar(String entidad
                                     , String claveprocedimiento
                                     , Hashtable parametros)
        {
            return EjecutarEscalar(entidad, claveprocedimiento, parametros, String.Empty);
        }

        //[MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Object EjecutarEscalar(String entidad
                                    , String claveprocedimiento
                                    , Hashtable parametros
                                    , String cadenaconexion)
        {
            Object _objresultado;
            SqlCommand _objcomandotmp;
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;
            SqlConnection sqlcon;

            if (String.IsNullOrEmpty(cadenaconexion))
            {
                sqlcon = PreparaConexion();
            }
            else
            {
                sqlcon = new SqlConnection(cadenaconexion);
            }

            _objcomandotmp = ObtenerObjetoSQLCommand(entidad, claveprocedimiento, parametros, sqlcon);

            if (_objcomandotmp == null) { return (Object)null; }

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if (!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif

                _objcomandotmp.Connection.Open();
            }

            _objresultado = _objcomandotmp.ExecuteScalar();

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                //--Desactivar la impersonalización
                if (impersonationContext != null)
                { impersonationContext.Undo(); }
            }

            if (!RecuperarValoresParametrosSalida(_objcomandotmp, parametros)) { return null; }

            return _objresultado;
        }

        #endregion

        #region Ejecutar Comando

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Int32 EjecutarComando(String entidad
                                           , String claveprocedimiento
                                           , Hashtable parametros)
        {
            return EjecutarComando(entidad, claveprocedimiento, parametros, PreparaConexion());
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Int32 EjecutarComando(String entidad
                                           , String claveprocedimiento
                                           , Hashtable parametros
                                           , String cadenaconexion)
        {
            SqlConnection osqlcon;

            if (String.IsNullOrEmpty(cadenaconexion))
            {
                osqlcon = PreparaConexion();
            }
            else
            {
                osqlcon = new SqlConnection(cadenaconexion);
            }

            return EjecutarComando
                (entidad
                 , claveprocedimiento
                 , parametros
                 , osqlcon);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Int32 EjecutarComando(String entidad
                                           , String claveprocedimiento
                                           , Hashtable parametros
                                           , SqlConnection cn)
        {
            Int32 _intvalor;
            SqlCommand _objcomandotmp;
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            _objcomandotmp = ObtenerObjetoSQLCommand(entidad, claveprocedimiento, parametros, cn);

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if(!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif
                _objcomandotmp.Connection.Open();
            }

            _intvalor = _objcomandotmp.ExecuteNonQuery();

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                //--Desactivar la impersonalización
                if (impersonationContext != null)
                { impersonationContext.Undo(); }
            }

            if (!RecuperarValoresParametrosSalida(_objcomandotmp, parametros)) { return -1; }

            return _intvalor;
        }


        // Ejecutar comando pasando como parametro el ID de Conexión Activa (J) 20060901.
        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Int32 EjecutarComando
            (SqlTransaction transaccion
            , String entidad
            , String claveprocedimiento
            , Hashtable parametros)
        {
            Int32 _intvalor;
            SqlCommand _objcomandotmp;
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            _objcomandotmp = ObtenerObjetoSQLCommand(entidad, claveprocedimiento, parametros, transaccion);
            _objcomandotmp.Transaction = transaccion;
            _objcomandotmp.Connection = transaccion.Connection;

            if (_objcomandotmp == null) { return -1; }

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if (!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif
                _objcomandotmp.Connection.Open();
            }

            _intvalor = _objcomandotmp.ExecuteNonQuery();

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                //--Desactivar la impersonalización
                //#if (!DEBUG)
                if (impersonationContext != null)
                { impersonationContext.Undo(); }
                //#endif
            }

            if (!RecuperarValoresParametrosSalida(_objcomandotmp, parametros)) { return -1; }

            return _intvalor;
        }

        /// <summary>
        /// EjecutarComando.
        /// </summary>
        /// <param name="comandotextosql"></param>
        /// <returns></returns>
        public virtual Int32 EjecutarComando(String comandotextosql)
        {
            return EjecutarComando(comandotextosql, String.Empty);
        }

        /// <summary>
        /// EjecutarComando
        /// </summary>
        /// <param name="comandotextosql"></param>
        /// <param name="cadenaconexion"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Int32 EjecutarComando
            (String comandotextosql
            , String cadenaconexion)
        {
            SqlConnection sqlcon = new SqlConnection();

            if (String.IsNullOrEmpty(cadenaconexion))
            {
                sqlcon = PreparaConexion();
            }
            else
                sqlcon = new SqlConnection(cadenaconexion);

            return EjecutarComando(comandotextosql, sqlcon);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Int32 EjecutarComando
            (String comandotextosql
            , SqlConnection sqlconn)
        {
            Int32 _intresultado;
            SqlCommand _objcomandotmp = new SqlCommand(comandotextosql);
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            _objcomandotmp.Connection = sqlconn;

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if (!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif

                _objcomandotmp.Connection.Open();
            }

            _intresultado = _objcomandotmp.ExecuteNonQuery();

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                //--Desactivar la impersonalización
                if (impersonationContext != null)
                { impersonationContext.Undo(); }
            }

            return _intresultado;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual Int32 EjecutarComando(SqlTransaction transaccion, String comandotextosql)
        {
            Int32 _intresultado;
            SqlCommand _objcomandotmp = new SqlCommand(comandotextosql);
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            //if (_objcomandotmp.Connection == null) 
            _objcomandotmp.Transaction = transaccion;
            _objcomandotmp.Connection = transaccion.Connection;

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if (!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif

                _objcomandotmp.Connection.Open();
            }

            _intresultado = _objcomandotmp.ExecuteNonQuery();

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                //--Desactivar la impersonalización
                if (impersonationContext != null) { impersonationContext.Undo(); }
            }

            return _intresultado;
        }

        #endregion

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual DataTableG EjecutarComandoEntidadG(String entidad, String claveprocedimiento, Hashtable parametros)
        {
            DataTableG _objtabladato;
            Fila fila;
            Object[] valores;
            SqlCommand _objcomandotmp;
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            _objcomandotmp = ObtenerObjetoSQLCommand(entidad, claveprocedimiento, parametros, (SqlConnection)null);

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                //--Activar la impersonalización
                //#if (!DEBUG)
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();
                //#endif
                _objcomandotmp.Connection.Open();
            }

            _objtabladato = new DataTableG();
            _objtabladato.Nombre = entidad.ToLower();

            using (SqlDataReader reader = _objcomandotmp.ExecuteReader())
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    _objtabladato.Columnas.Add(new Columna() { Nombre = reader.GetName(i), TipoDato = reader.GetFieldType(i) });
                }

                while (reader.Read())
                {
                    fila = new Fila();
                    valores = new Object[reader.FieldCount];

                    reader.GetValues(valores);

                    fila.Add(valores);
                    _objtabladato.Filas.Add(fila);
                }
            }

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                //--Desactivar la impersonalización
                if (impersonationContext != null)
                { impersonationContext.Undo(); }
            }

            RecuperarValoresParametrosSalida(_objcomandotmp, parametros);

            return _objtabladato;
        }

        #endregion

        #region Metodos para limpiar Caché

        /// <summary>
        /// Limpia el caché de todas las entidades.
        /// </summary>
        public void LimpiarCache()
        {
            _httablacomando = new Hashtable();
        }

        /// <summary>
        /// Limpia el caché para una entidad específica.
        /// </summary>
        /// <param name="entidad"></param>
        public void LimpiarCache(String entidad)
        {
            if (_httablacomando.Contains(entidad)) _httablacomando.Remove(entidad);
        }

        #endregion

        #region Metodos para la creación de un objeto SQLCOMMAND.

        /// <summary>
        /// Crea y devuelve un objeto SQLCOMMAND.
        /// </summary>
        /// <param name="claveSP"></param>
        /// <param name="parametros"></param>
        /// <param name="sqlcon"></param>
        /// <returns></returns>
        private SqlCommand ObtenerObjetoSQLCommand(String entidad, String clave, Hashtable parametros, SqlConnection sqlcon)
        {
            return ObtenerObjetoSQLCommand(entidad, clave, parametros, sqlcon, null);
        }

        /// <summary>
        /// Crea y devuelve un objeto SQLCOMMAND.
        /// </summary>
        /// <param name="entidad"></param>
        /// <param name="clave"></param>
        /// <param name="parametros"></param>
        /// <param name="sqltran"></param>
        /// <returns></returns>
        private SqlCommand ObtenerObjetoSQLCommand(String entidad, String clave, Hashtable parametros, SqlTransaction sqltran)
        {
            return ObtenerObjetoSQLCommand(entidad, clave, parametros, sqltran.Connection, sqltran);
        }

        /// <summary>
        /// Crea y devuelve un objeto SQLCOMMAND.
        /// </summary>
        /// <param name="entidad"></param>
        /// <param name="clave"></param>
        /// <param name="parametros"></param>
        /// <param name="sqlcon"></param>
        /// <param name="sqltran"></param>
        /// <returns></returns>
        private SqlCommand ObtenerObjetoSQLCommand(String entidad, String clave, Hashtable parametros, SqlConnection sqlcon, SqlTransaction sqltran)
        {
            SqlCommand osqlcommand = (SqlCommand)null;
            SqlConnection osqlconnection = sqlcon == null ? PreparaConexion() : sqlcon;
            String claveSP = entidad + "_" + clave + "_pa";

            if (_httablacomando.Contains(claveSP) == false)
            {
                // ----------------------------------------
                // Validar si existe el SP a ejecutar.
                // ----------------------------------------

                if (ExisteObjetoSQL(claveSP, osqlconnection, sqltran) == false)
                    throw new Exception("No existe objeto con entidad: " + entidad + "; y clave: " + clave + " a ejecutar...!");

                // ----------------------------------------
                // Definir el Objeto SQLCOMMAND.
                // ----------------------------------------
                osqlcommand = new SqlCommand();

                osqlcommand.CommandText = claveSP;
                osqlcommand.CommandType = CommandType.StoredProcedure;
                osqlcommand.Connection = osqlconnection;
                osqlcommand.Transaction = sqltran;
                osqlcommand.CommandTimeout = _inttiempofuerapredeterminado;

                // ----------------------------------------------
                // Establecer los parámetros.
                // ----------------------------------------------

                Boolean existeConexion = true;
                WindowsImpersonationContext impersonationContext = null;

                if (osqlcommand.Connection.State == ConnectionState.Closed)
                {
                    if (WindowsIdentity.GetCurrent(true) != null)
                        impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();

                    existeConexion = false;
                    osqlcommand.Connection.Open();
                }

                SqlCommandBuilder.DeriveParameters(osqlcommand);

                if (existeConexion == false)
                {
                    if (impersonationContext != null)
                        impersonationContext.Undo();

                    osqlcommand.Connection.Close();
                }

                // ----------------------------------------------
                // Agregar al Caché el Objeto SqlCommand creado.
                // ----------------------------------------------

                _httablacomando[claveSP] = osqlcommand;
            }

            osqlcommand = (SqlCommand)_httablacomando[claveSP];
            osqlcommand.Connection = osqlconnection;
            osqlcommand.Transaction = sqltran == null ? osqlcommand.Transaction : sqltran;

            if (osqlcommand.Parameters.Count > 0)
            {
                CargarValoresParametros(osqlcommand, parametros);
            }

            return osqlcommand;
        }

        /// <summary>
        /// Valida si existe el objeto a ejecutar en la DB.
        /// </summary>
        /// <param name="claveSP"></param>
        /// <returns></returns>
        /// 
        private Boolean ExisteObjetoSQL(String claveSP, SqlConnection osqlcon)
        {
            return ExisteObjetoSQL(claveSP, osqlcon, null);
        }

        /// <summary>
        /// Valida si existe el objeto a ejecutar en la DB.
        /// </summary>
        /// <param name="claveSP"></param>
        /// <param name="osqlcon"></param>
        /// <param name="osqltran"></param>
        /// <returns></returns>
        private Boolean ExisteObjetoSQL(String claveSP, SqlConnection osqlcon, SqlTransaction osqltran)
        {
            SqlCommand ocommand = new SqlCommand();
            Boolean existeConexion = true;

            WindowsImpersonationContext impersonationContext = null;

            ocommand.CommandText = "MetadatoTablaEsquema_ValidaExisteObjeto_pa";
            ocommand.CommandType = CommandType.StoredProcedure;
            ocommand.Connection = osqlcon == null ? PreparaConexion() : osqlcon;
            ocommand.Transaction = osqltran;
            ocommand.Parameters.AddWithValue("@p_nombreObjeto", claveSP);

            if (ocommand.Connection.State == ConnectionState.Closed)
            {
                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();

                existeConexion = false;
                ocommand.Connection.Open();
            }

            Int32 resultado = Convert.ToInt32(ocommand.ExecuteScalar());

            if (existeConexion == false)
            {
                //--Desactivar la impersonalización
                if (impersonationContext != null)
                { impersonationContext.Undo(); }

                ocommand.Connection.Close();
            }

            return (resultado == 1);
        }

        /// <summary>
        /// Valida si existe el objeto a ejecutar en la DB.
        /// </summary>
        /// <param name="claveSP"></param>
        /// <param name="osqltran"></param>
        /// <returns></returns>
        private Boolean ExisteObjetoSQL(String claveSP, SqlTransaction osqltran)
        {
            return ExisteObjetoSQL(claveSP, osqltran.Connection, osqltran);
        }

        public static string Encriptar(string _cadenaAencriptar)
        {
            string result = string.Empty;
            byte[] encryted = System.Text.Encoding.Unicode.GetBytes(_cadenaAencriptar);
            result = Convert.ToBase64String(encryted);
            return result;
        }

        public static string DesEncriptar(string _cadenaAdesencriptar)
        {
            string result = string.Empty;
            byte[] decryted = Convert.FromBase64String(_cadenaAdesencriptar);
            result = System.Text.Encoding.Unicode.GetString(decryted);
            return result;
        }

        #endregion
    }
}
