﻿using Capa_Entidad;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Capa_Datos
{
   public class clsGestionarMaestroDAO
    {

        #region Obtener Instancia

        private static readonly clsGestionarMaestroDAO _oGestorClase = new clsGestionarMaestroDAO();
        private static clsGestionDato _oGestorDAO;

        public static clsGestionarMaestroDAO ObtenerInstancia()
        {
            if (_oGestorDAO == null)
                _oGestorDAO = clsGestionDato.Instancia;
            return _oGestorClase;
        }

        public static clsGestionarMaestroDAO Instancia
        {
            get
            {
                if (_oGestorDAO == null)
                    _oGestorDAO = clsGestionDato.Instancia;
                return _oGestorClase;
            }
        }

        private clsGestionarMaestroDAO()
        { }

        #endregion Obtener Instancia

        public DataTable ObtenerAyudaLista(String nombretabla, String claveprocedimiento , Hashtable filtros)
        {
            clsGestionDato _objgestordato;
            DataTable _objentidad;

            if (nombretabla == null || nombretabla == "") { return (DataTable)null; }
            if (claveprocedimiento == null || claveprocedimiento == "") { return (DataTable)null; }

            _objgestordato = clsGestionDato.ObtenerInstancia();
            if (_objgestordato == null) { return (DataTable)null; }
            try
            {
                _objentidad = _objgestordato.EjecutarComandoEntidad(nombretabla.ToLower()
                                        , claveprocedimiento.ToLower()
                                        , filtros);

                _objentidad.TableName = nombretabla;

                return _objentidad;
            }
            catch (Exception ex)
            {
                throw (ex);
            }


        }

        public Boolean InsertarEmpresa(clsEmpresa objentidad)
        {
            Hashtable parametros = new Hashtable();
            Int32 _intresultado = 0;
            try
            {
                parametros.Add("@p_descripcion", objentidad.Descripcion);
                parametros.Add("@p_telefono", objentidad.Telefono);
                parametros.Add("@p_direccion", objentidad.Direccion);
                parametros.Add("@p_direccioncomplementaria", objentidad.DireccionComplementaria);
                parametros.Add("@p_nombrecomercial", objentidad.NombreComercial);
                parametros.Add("@p_nroidentidad", objentidad.NroIdentidad);
                parametros.Add("@p_idtipoidentidad", objentidad.IdTipoIdentidad);
                parametros.Add("@p_email", objentidad.Email);
                parametros.Add("@p_iddepartamento", objentidad.IdDepartamento);
                parametros.Add("@p_idprovincia", objentidad.IdProvincia);
                parametros.Add("@p_iddistrito", objentidad.IdDistrito);
                parametros.Add("@p_idestado", objentidad.IdEstado);
                parametros.Add("@p_imagen", objentidad.Imagen); 

                _intresultado = clsGestionDato.Instancia.EjecutarComando("Empresa", "Insertar", parametros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return (_intresultado > 0);
        }

        public Boolean ActualizarEmpresa(clsEmpresa objentidad)
        {
            Hashtable parametros = new Hashtable();
            Int32 _intresultado = 0;
            try
            {
                parametros.Add("@p_idempresa", objentidad.IdEmpresa);
                parametros.Add("@p_descripcion", objentidad.Descripcion);
                parametros.Add("@p_nombrecomercial", objentidad.NombreComercial);
                parametros.Add("@p_telefono", objentidad.Telefono);
                parametros.Add("@p_direccion", objentidad.Direccion);
                parametros.Add("@p_direccioncomplementaria", objentidad.DireccionComplementaria);
                parametros.Add("@p_nroidentidad", objentidad.NroIdentidad);
                parametros.Add("@p_idtipoidentidad", objentidad.IdTipoIdentidad);
                parametros.Add("@p_email", objentidad.Email);
                parametros.Add("@p_iddepartamento", objentidad.IdDepartamento);
                parametros.Add("@p_idprovincia", objentidad.IdProvincia);
                parametros.Add("@p_iddistrito", objentidad.IdDistrito);
                parametros.Add("@p_idestado", objentidad.IdEstado);
                parametros.Add("@p_imagen", objentidad.Imagen); 

                _intresultado = clsGestionDato.Instancia.EjecutarComando("Empresa", "Actualizar", parametros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return (_intresultado > 0);
        }

        public DataTableG ListarEmpresa(clsEmpresa objentidad)
        {
            //clsListaPaginacionResultadoBalanceSED olista;
            Hashtable parametros = new Hashtable();
            DataTableG dtResultado;

            try
            {
                parametros.Add("@p_idempresa", objentidad.IdEmpresa);
                parametros.Add("@p_idusuario", objentidad.IdUsuario );
                dtResultado = _oGestorDAO.EjecutarComandoEntidadG("Empresa", "Listar", parametros);

            }
            catch (Exception)
            {

                throw;
            }

            return dtResultado;
        }

        public Boolean EliminarEmpresa(clsEmpresa objentidad)
        {
            Hashtable parametros = new Hashtable();
            Int32 _intresultado = 0;
            try
            {
                parametros.Add("@p_idempresa", objentidad.IdEmpresa);

                _intresultado = clsGestionDato.Instancia.EjecutarComando("Empresa", "Eliminar", parametros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return (_intresultado > 0);
        }

        public DataTableG ListarReporte(clsReporte objentidad)
        {
            //clsListaPaginacionResultadoBalanceSED olista;
            Hashtable htParametros = new Hashtable();
            DataTableG dtResultado;

            try
            {
                dtResultado = _oGestorDAO.EjecutarComandoEntidadG("Reporte", "Listar", htParametros);

            }
            catch (Exception)
            {

                throw;
            }

            return dtResultado;
        }

        public DataTable ProcesarReporte(clsReporte objentidad)
        {
            Hashtable parametros = new Hashtable();
            DataTable dtResultado;

            string sp, prt1, prt2;
            sp = objentidad.SP;
            int p;
            p = sp.IndexOf("_");
            prt1 = sp.Substring(0, p);
            prt2 = sp.Substring(p + 1, sp.Length - p - 4);

            parametros.Add("@p_idempresa", objentidad.IdEmpresa);
            parametros.Add("@p_idsede", objentidad.IdSede);
            parametros.Add("@p_fechadesde", objentidad.FechaDesde);
            parametros.Add("@p_fechahasta", objentidad.FechaHasta);
            parametros.Add("@p_idrubro", objentidad.IdRubro);
            parametros.Add("@p_idusuario", objentidad.IdUsuario);

            try
            {
                dtResultado = _oGestorDAO.EjecutarComandoEntidad(prt1, prt2, parametros);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtResultado;
        }

        public Boolean InsertarPuntoAtencion(clsPuntoAtencion objentidad)
        {
            Hashtable parametros = new Hashtable();
            Int32 _intresultado = 0;
            try
            {
                parametros.Add("@p_idempresa", objentidad.IdEmpresa);
                parametros.Add("@p_idsede", objentidad.IdSede);
                parametros.Add("@p_idtipoatencion", objentidad.IdTipoAtencion);
                parametros.Add("@p_idusuario", objentidad.IdUsuario);
                parametros.Add("@p_descripcion", objentidad.Descripcion);
                parametros.Add("@p_idestado", objentidad.IdEstado);

                _intresultado = clsGestionDato.Instancia.EjecutarComando("PuntoAtencion", "Insertar", parametros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return (_intresultado > 0);
        }

        public Boolean ActualizarPuntoAtencion(clsPuntoAtencion objentidad)
        {
            Hashtable parametros = new Hashtable();
            Int32 _intresultado = 0;
            try
            {
                parametros.Add("@p_idempresa", objentidad.IdEmpresa);
                parametros.Add("@p_idsede", objentidad.IdSede);
                parametros.Add("@p_idpuntoatencion", objentidad.IdPuntoAtencion);
                parametros.Add("@p_idtipoatencion", objentidad.IdTipoAtencion);
                parametros.Add("@p_idusuario", objentidad.IdUsuario);
                parametros.Add("@p_descripcion", objentidad.Descripcion);
                parametros.Add("@p_idestado", objentidad.IdEstado);

                _intresultado = clsGestionDato.Instancia.EjecutarComando("PuntoAtencion", "Actualizar", parametros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return (_intresultado > 0);
        }

        public DataTableG ListarPuntoAtencion(clsPuntoAtencion objentidad)
        {
            //clsListaPaginacionResultadoBalanceSED olista;
            Hashtable parametros = new Hashtable();           
            DataTableG dtResultado;

            try
            {
                parametros.Add("@p_idempresa", objentidad.IdEmpresa);
                parametros.Add("@p_idsede", objentidad.IdSede);
                parametros.Add("@p_idusuario", objentidad.IdUsuario);
                parametros.Add("@p_idusuarioprincipal", objentidad.IdUsuarioPrincipal);
                dtResultado = _oGestorDAO.EjecutarComandoEntidadG("PuntoAtencion", "Listar", parametros);

            }
            catch (Exception)
            {

                throw;
            }

            return dtResultado;
        }

        public Boolean EliminarPuntoAtencion(clsPuntoAtencion objentidad)
        {
            Hashtable parametros = new Hashtable();
            Int32 _intresultado = 0;
            try
            {
                parametros.Add("@p_idpuntoatencion", objentidad.IdPuntoAtencion);

                _intresultado = clsGestionDato.Instancia.EjecutarComando("PuntoAtencion", "Eliminar", parametros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return (_intresultado > 0);
        }

        //public Boolean InsertarMonederoElectronico(clsMonederoElectronico objentidad)
        //{
        //    Hashtable parametros = new Hashtable();
        //    Int32 _intresultado = 0;
        //    try
        //    {
        //        parametros.Add("@p_cantidad", objentidad.Cantidad);
        //        parametros.Add("@p_cantidadpuntos", objentidad.CantidadPuntos);
        //        parametros.Add("@p_porcentajeventa", objentidad.PorcentajeVenta);
        //        parametros.Add("@p_activar", objentidad.Activar);
        //        parametros.Add("@p_idestado", objentidad.IdEstado);
        //        parametros.Add("@p_idempresa", objentidad.IdEmpresa);

        //        _intresultado = clsGestionDato.Instancia.EjecutarComando("MonederoElectronico", "Insertar", parametros);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    return (_intresultado > 0);
        //}

        //public Boolean ActualizarMonederoElectronico(clsMonederoElectronico objentidad)
        //{
        //    Hashtable parametros = new Hashtable();
        //    Int32 _intresultado = 0;
        //    try
        //    {
        //        parametros.Add("@p_idmonederoelectronico", objentidad.IdMonederoElectronico);
        //        parametros.Add("@p_cantidad", objentidad.Cantidad);
        //        parametros.Add("@p_cantidadpuntos", objentidad.CantidadPuntos);
        //        parametros.Add("@p_porcentajeventa", objentidad.PorcentajeVenta);
        //        parametros.Add("@p_activar", objentidad.Activar);
        //        parametros.Add("@p_idestado", objentidad.IdEstado);
        //        parametros.Add("@p_idempresa", objentidad.IdEmpresa);

        //        _intresultado = clsGestionDato.Instancia.EjecutarComando("MonederoElectronico", "Actualizar", parametros);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    return (_intresultado > 0);
        //}

        //public DataTableG ListarMonederoElectronico(clsMonederoElectronico objentidad)
        //{
        //    Hashtable parametros = new Hashtable();
        //    DataTableG dtResultado;

        //    try
        //    {
        //        parametros.Add("@p_idempresa", objentidad.IdEmpresa);
        //        parametros.Add("@p_idusuario", objentidad.IdUsuario);
        //        dtResultado = _oGestorDAO.EjecutarComandoEntidadG("MonederoElectronico", "Listar", parametros);

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //    return dtResultado;
        //}

        //public Boolean EliminarMonederoElectronico(clsMonederoElectronico objentidad)
        //{
        //    Hashtable parametros = new Hashtable();
        //    Int32 _intresultado = 0;
        //    try
        //    {
        //        parametros.Add("@p_idmonederoelectronico", objentidad.IdMonederoElectronico);

        //        _intresultado = clsGestionDato.Instancia.EjecutarComando("MonederoElectronico", "Eliminar", parametros);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    return (_intresultado > 0);
        //}
    }
}
