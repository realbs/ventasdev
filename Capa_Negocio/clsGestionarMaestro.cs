﻿using Capa_Datos;
using Capa_Entidad;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Capa_Negocio
{
    public class clsGestionarMaestro
    {
        #region Obtener Instancia

        private static readonly clsGestionarMaestro _objgestordao = new clsGestionarMaestro();

        public static clsGestionarMaestro ObtenerInstancia()
        {
            return _objgestordao;
        }

        public static clsGestionarMaestro Instancia
        {
            get { return _objgestordao; }
        }

        #endregion

        public DataTable ObtenerAyudaLista(String entidad, String clave, Hashtable parametros)
        {
            DataTable _objtabla = new DataTable();

            try
            {
                //Hashtable _htparametros = ConvertirArraytoHashtable(parametros);

                _objtabla = clsGestionarMaestroDAO.Instancia.ObtenerAyudaLista(entidad, clave, parametros);

            }

            catch (Exception ex)
            {
                //ExcepcionNG.Guardar(ex.Message, enumTipoExcepcion.Error);
            }

            return _objtabla; //.ConvertToByte();
        }

        public Hashtable ConvertirArraytoHashtable(Object[] lista)
        {
            Hashtable _htlista = new Hashtable();
            Int32 _intx = 0;

            for (_intx = 0; _intx < lista.Length; )
            {
                _htlista.Add(lista[_intx++], lista[_intx++]);
            }
            return _htlista;
        }

        public Boolean InsertarEmpresa(clsEmpresa objentidad)
        {
            try
            {
                return clsGestionarMaestroDAO.Instancia.InsertarEmpresa(objentidad);
            }
            catch (Exception ex)
            {
                //ExcepcionNG.Guardar(ex, enumTipoExcepcion.Error, true);
                return false;
            }
        }

        public Boolean ActualizarEmpresa(clsEmpresa objentidad)
        {
            try
            {
                return clsGestionarMaestroDAO.Instancia.ActualizarEmpresa(objentidad);
            }
            catch (Exception ex)
            {
                //ExcepcionNG.Guardar(ex, enumTipoExcepcion.Error, true);
                return false;
            }
        }

        public DataTableG ListarEmpresa(clsEmpresa objentidad)
        {
            DataTableG dtResultado = null;
            try
            {

                dtResultado = clsGestionarMaestroDAO.Instancia.ListarEmpresa(objentidad);

            }
            catch (Exception ex)
            {
                //ExcepcionNG.Guardar(ex.Message, enumTipoExcepcion.Error);
            }

            return dtResultado; //.ConvertToByte();
        }

        public Boolean EliminarEmpresa(clsEmpresa objentidad)
        {
            try
            {
                return clsGestionarMaestroDAO.Instancia.EliminarEmpresa(objentidad);
            }
            catch (Exception ex)
            {
                //ExcepcionNG.Guardar(ex, enumTipoExcepcion.Error, true);
                return false;
            }
        }

        public DataTableG ListarReporte(clsReporte objentidad)
        {
            DataTableG dtResultado = null;
            try
            {

                dtResultado = clsGestionarMaestroDAO.Instancia.ListarReporte(objentidad);

            }
            catch (Exception ex)
            {
                //ExcepcionNG.Guardar(ex.Message, enumTipoExcepcion.Error);
            }

            return dtResultado; //.ConvertToByte();
        }

        public DataTable ProcesarReporte(clsReporte objentidad)
        {
            DataTable dtResultado = null;
            try
            {

                dtResultado = clsGestionarMaestroDAO.Instancia.ProcesarReporte(objentidad);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtResultado; //.ConvertToByte();
        }

        public Boolean InsertarPuntoAtencion(clsPuntoAtencion objentidad)
        {
            try
            {
                return clsGestionarMaestroDAO.Instancia.InsertarPuntoAtencion(objentidad);
            }
            catch (Exception ex)
            {
                //ExcepcionNG.Guardar(ex, enumTipoExcepcion.Error, true);
                return false;
            }
        }

        public Boolean ActualizarPuntoAtencion(clsPuntoAtencion objentidad)
        {
            try
            {
                return clsGestionarMaestroDAO.Instancia.ActualizarPuntoAtencion(objentidad);
            }
            catch (Exception ex)
            {
                //ExcepcionNG.Guardar(ex, enumTipoExcepcion.Error, true);
                return false;
            }
        }

        public DataTableG ListarPuntoAtencion(clsPuntoAtencion objentidad)
        {
            DataTableG dtResultado = null;
            try
            {

                dtResultado = clsGestionarMaestroDAO.Instancia.ListarPuntoAtencion(objentidad);

            }
            catch (Exception ex)
            {
                //ExcepcionNG.Guardar(ex.Message, enumTipoExcepcion.Error);
            }

            return dtResultado; //.ConvertToByte();
        }

        public Boolean EliminarPuntoAtencion(clsPuntoAtencion objentidad)
        {
            try
            {
                return clsGestionarMaestroDAO.Instancia.EliminarPuntoAtencion(objentidad);
            }
            catch (Exception ex)
            {
                //ExcepcionNG.Guardar(ex, enumTipoExcepcion.Error, true);
                return false;
            }
        }

        //public Boolean InsertarMonederoElectronico(clsMonederoElectronico objentidad)
        //{
        //    try
        //    {
        //        return clsGestionarMaestroDAO.Instancia.InsertarMonederoElectronico(objentidad);
        //    }
        //    catch (Exception ex)
        //    {
        //        //ExcepcionNG.Guardar(ex, enumTipoExcepcion.Error, true);
        //        return false;
        //    }
        //}

        //public Boolean ActualizarMonederoElectronico(clsMonederoElectronico objentidad)
        //{
        //    try
        //    {
        //        return clsGestionarMaestroDAO.Instancia.ActualizarMonederoElectronico(objentidad);
        //    }
        //    catch (Exception ex)
        //    {
        //        //ExcepcionNG.Guardar(ex, enumTipoExcepcion.Error, true);
        //        return false;
        //    }
        //}

        //public DataTableG ListarMonederoElectronico(clsMonederoElectronico objentidad)
        //{
        //    DataTableG dtResultado = null;
        //    try
        //    {

        //        dtResultado = clsGestionarMaestroDAO.Instancia.ListarMonederoElectronico(objentidad);

        //    }
        //    catch (Exception ex)
        //    {
        //        //ExcepcionNG.Guardar(ex.Message, enumTipoExcepcion.Error);
        //    }

        //    return dtResultado; //.ConvertToByte();
        //}

        //public Boolean EliminarMonederoElectronico(clsMonederoElectronico objentidad)
        //{
        //    try
        //    {
        //        return clsGestionarMaestroDAO.Instancia.EliminarMonederoElectronico(objentidad);
        //    }
        //    catch (Exception ex)
        //    {
        //        //ExcepcionNG.Guardar(ex, enumTipoExcepcion.Error, true);
        //        return false;
        //    }
        //}

    }
}
